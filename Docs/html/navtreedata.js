/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Choco OS", "index.html", [
    [ "Choco OS - Chocolate Operating System", "index.html", null ],
    [ "Trainings", "d6/dce/_trainings.html", [
      [ "Step 1. Let's run the system!", "d6/dce/_trainings.html#Step-1", [
        [ "Windows", "d6/dce/_trainings.html#Windows", null ],
        [ "Linux", "d6/dce/_trainings.html#Linux", null ],
        [ "UART converter pins connection", "d6/dce/_trainings.html#ConverterPinsConecting", null ],
        [ "PuTTY configuration", "d6/dce/_trainings.html#PuttyConfiguration", null ]
      ] ],
      [ "Step 2. First glance at terminal", "d6/dce/_trainings.html#Step-2", [
        [ "Program Memory", "d6/dce/_trainings.html#MemoryProgram", null ],
        [ "Program System", "d6/dce/_trainings.html#SystemProgram", null ]
      ] ],
      [ "Step 3. Write your first \"Hello World!\"", "d6/dce/_trainings.html#Step-3", [
        [ "Generate a template", "d6/dce/_trainings.html#GenerateTemplate", null ],
        [ "Edit source file", "d6/dce/_trainings.html#EditSource", null ],
        [ "Adding Hello World to programs list", "d6/dce/_trainings.html#AddingToList", null ]
      ] ],
      [ "Step 4. Working with Eclipse", "d6/dce/_trainings.html#Step-4", [
        [ "Importing project", "d6/dce/_trainings.html#ProjectImport", null ],
        [ "Building", "d6/dce/_trainings.html#Building", null ],
        [ "Debugging", "d6/dce/_trainings.html#Debugging", null ]
      ] ],
      [ "Step 5. Placing any file inside the target flash memory", "d6/dce/_trainings.html#Step-5", null ],
      [ "Step 6. Usage of stdio and TGUI for generating terminal GUI", "d6/dce/_trainings.html#Step-6", null ],
      [ "Step 7. Making GUI application with real widgets", "d6/dce/_trainings.html#Step-7", null ],
      [ "Step.8 Changing configuration of the project", "d6/dce/_trainings.html#Step-8", [
        [ "List of drivers", "d6/dce/_trainings.html#ListOfDrivers", null ],
        [ "List of streams", "d6/dce/_trainings.html#ListOfStreams", null ],
        [ "List of services", "d6/dce/_trainings.html#ListOfServices", null ],
        [ "Auto-Configurations", "d6/dce/_trainings.html#AutoConfigurations", null ],
        [ "List of file systems", "d6/dce/_trainings.html#ListOfFileSystems", null ],
        [ "List of screens", "d6/dce/_trainings.html#ListOfScreens", null ],
        [ "Network configuration", "d6/dce/_trainings.html#NetworkConfiguration", null ],
        [ "System configuration", "d6/dce/_trainings.html#SystemConfiguration", null ],
        [ "Pins definition", "d6/dce/_trainings.html#PinsDefinition", null ]
      ] ],
      [ "Step.9 Write your first driver", "d6/dce/_trainings.html#Step-9", null ],
      [ "Step.10 Write your first file system", "d6/dce/_trainings.html#Step-10", null ],
      [ "Step.11 Write your service", "d6/dce/_trainings.html#Step-11", null ]
    ] ],
    [ "Coding Standard", "dd/db3/_coding_standard.html", [
      [ "Introduction", "dd/db3/_coding_standard.html#Introduction", null ],
      [ "Rules", "dd/db3/_coding_standard.html#Rules", null ]
    ] ],
    [ "Architecture", "db/daf/_architecture.html", "db/daf/_architecture" ],
    [ "Deprecated List", "da/d58/deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/d76/oc__service_8c.html#a74b4239a9a7ef31751d29178b39a98d3",
"d1/d08/ti_2lm4f_2lld_2gpio_2oc__gpio__lld_8c.html#a95a91817d96c4df2330ab7f87a15ec8d",
"d1/d1b/group___udp.html#ggaeafe5e0e03c7fdbb3d6c49962013e751a8e18ab9560290848e54736615da46c27",
"d1/d60/group___c_f_g.html#ga5564057778e27c29418ffbd4a20806c3",
"d1/dd2/group___m_c_s.html#ggaa8f1a29a1bf274f39c55d4e0fad50ffea4d1c545b4e1351418b3b9c23407bdd13",
"d2/d87/oc__struct_8h.html",
"d3/d07/ti_2lm4f_2lld_2sys_2oc__sys__lld_8c.html#a01385a44a9ac3364e3b9a0ef8cb61d1d",
"d3/ded/oc__stdio_8c.html#a89ef106643e54925e01e214c1bdb9423",
"d4/d1b/group___s_d_m_m_c.html#ggad2df5ccc98409a9e85765e5d9a7c1927ae5c32f692e775674de832852070dbc61",
"d4/d37/st_2stm32f7_2lld_2timer_2oc__timer__lld_8c.html#ae8e3aa3770db99adeee5c6747fe5faab",
"d4/ddc/_cortex___m7_2oc__mcs_8c.html#a251615287107359fb7c90a34f8ae427c",
"d5/d29/group___predefined_fonts.html#gae709e6936eadf332fb90b88c0b53e048",
"d5/dde/oc__driverman_8c.html",
"d6/d55/group___dhcp.html#ggabd459f45a662da9e141075b9a95c14f2a22dbbb86cdef22642e731b42cc0f967c",
"d6/dc7/group___elf.html#ga024ced97323844dca3b41c7d357fae68",
"d7/d20/group___http.html#gab589104a4daf087cc7242949d16bc7f2",
"d7/d2f/struct_screen__t.html#aa1e3ff0b3aca52db45270d789386999d",
"d7/d83/struct_context__t.html#a0c476a9d3fee62a169aacc7864e83a23",
"d7/db3/struct_semaphore__t.html",
"d8/d1b/oc__libraries_8h.html#a0406b536d4760657a3a087c50cdf6240a5aff12abdb817a30c2c27a5e4f8620ef",
"d8/d1b/oc__libraries_8h.html#a0406b536d4760657a3a087c50cdf6240ad89abaf00cf93e15e3c39bd6f442ba44",
"d8/d57/structo_c___font___character_info__t.html",
"d8/df6/group___c_l_o_c_k-_l_l_d.html#gaeb7c47238b786785485cf8178da91d15",
"d9/d13/group___m_defs.html#gaf16e6915d400bd393670b05a6a5d7393",
"d9/d1f/group___machine.html#ga8c573fa34e7af34c7c46df440e0beb20",
"d9/d71/group___thread.html#ga8bc0e7f7317a8edae1dbeec915127845",
"d9/d83/oc__lsf_8h.html#a7e94bec7e379f08a5cfeaddf97608d90",
"d9/d9b/group___tcp.html#ggab4b1f48f2cd50c3fc249b9929bd43697a779dc0b6a0fa89517e19df5153fe0b25",
"d9/de0/group___s_d_m_m_c-_l_l_d.html#ga6569bf9d5084d356279e09b676fd52e4",
"da/d90/group___netif_man.html#ga490d0f830fdd2553c91bdf3ff21f738f",
"db/d47/group___object.html#ga4bac271ea96b5c27df8f40c7b8bdbd8a",
"db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ab0b49f0613adb5653e4dbdb7fa3a51a8",
"dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8ada6cf47d547ecd235f0b1b8362c73068",
"dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t.html#ad296a2684a1375527319b91e8daa06e9",
"dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9a920aaab8d88a27b573aa2f35dde20d91",
"de/dec/oc__compiler_8h.html",
"df/d6b/struct_p_a_c_k_e_d.html#a129ab1a098ddc920967f86c50b18912a",
"df/d6b/struct_p_a_c_k_e_d.html#acd519506a39fce684d2905e99d6db625",
"df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856a2f198f9ab7a8685e4ddd5bf94e40438b",
"df/dcc/group___g_p_i_o-_l_l_d.html#gaf26d9deff1317c8e3329b5983806ad06",
"dir_af0699d91c794ca41264335f4c628cbd.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';