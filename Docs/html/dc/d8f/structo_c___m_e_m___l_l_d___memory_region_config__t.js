var structo_c___m_e_m___l_l_d___memory_region_config__t =
[
    [ "BaseAddress", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#a5b0cb702919e4a1f5fadb97dfd8489cc", null ],
    [ "Bufforable", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#ab23eb30ddb607446b3dfba6ed873925c", null ],
    [ "Cachable", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#a0d44fcdd136fe33cfe4be5f30a2f082d", null ],
    [ "FindFreeRegion", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#ab8a25222ffd326ce44f8d53ff6d05f8b", null ],
    [ "Power", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#abc56c5af1051778e42061aea39286c42", null ],
    [ "PrivilegedAccess", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#aa5a1e6195edea1208134600f27b5d3da", null ],
    [ "RegionNumber", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#a1884d03382d8b20fe3f87096906a1b06", null ],
    [ "Size", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#afe1e2b17eb8df8ef6ee0ace34cb21759", null ],
    [ "UserAccess", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html#a51886d09683cafd88264153a5409b795", null ]
];