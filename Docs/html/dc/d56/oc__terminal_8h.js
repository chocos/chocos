var oc__terminal_8h =
[
    [ "oC_Terminal_Color_t", "d5/dbf/group___terminal.html#gaf2ae38bc63f3a9b9984953170de4d684", [
      [ "oC_Terminal_Color_Black", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684ae3e286e63aeea7d2e91f8103dcd208dc", null ],
      [ "oC_Terminal_Color_Red", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a475a2b3c3a143c4cc9b87d9b79209095", null ],
      [ "oC_Terminal_Color_Green", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a81858f7789fa09b00adb9dec45a35329", null ],
      [ "oC_Terminal_Color_Yellow", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a28127b9750cecae01036b10d65a96fc3", null ],
      [ "oC_Terminal_Color_Blue", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a0d3c32a075b7b51837c0e11f115c1be8", null ],
      [ "oC_Terminal_Color_Magenta", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a9a90c48bdd25faeda1bb3b93b0c07b5c", null ],
      [ "oC_Terminal_Color_Cyan", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a68a3a6802de9730b6873d5f48ab963af", null ],
      [ "oC_Terminal_Color_White", "d5/dbf/group___terminal.html#ggaf2ae38bc63f3a9b9984953170de4d684a696fc51273d0a786b11b2a54e902dd95", null ]
    ] ]
];