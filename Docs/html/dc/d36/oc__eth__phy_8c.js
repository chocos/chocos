var oc__eth__phy_8c =
[
    [ "oC_ETH_PhyReset", "dc/d36/oc__eth__phy_8c.html#ad16a9ac317cf9399c61c339cd2f6f46b", null ],
    [ "oC_ETH_ReadLinkStatus", "dc/d36/oc__eth__phy_8c.html#a604ec6adafb24c370e05cc2bc4cd55ec", null ],
    [ "oC_ETH_ReadPhyRegister", "dc/d36/oc__eth__phy_8c.html#aad7a030ed8cf11a4ddac045ac42e6b06", null ],
    [ "oC_ETH_SetAutoNegotiation", "dc/d36/oc__eth__phy_8c.html#a9bdd5ec43f2f7e4c4fd600ca195fe1bb", null ],
    [ "oC_ETH_SetPhyLoopback", "dc/d36/oc__eth__phy_8c.html#a710f48198f9387f6209bba8405cf3b0e", null ],
    [ "oC_ETH_WritePhyRegister", "dc/d36/oc__eth__phy_8c.html#a23cc66245af8e169c68a28a4e4a9f1ef", null ],
    [ "ReadRegister", "dc/d36/oc__eth__phy_8c.html#a10a9a32333c01f2b1c919d89f008c78f", null ],
    [ "WriteRegister", "dc/d36/oc__eth__phy_8c.html#a06d2df3d6d68361430b9e8e62ff09262", null ]
];