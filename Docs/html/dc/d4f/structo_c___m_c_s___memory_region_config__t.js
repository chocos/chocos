var structo_c___m_c_s___memory_region_config__t =
[
    [ "AlignSize", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#ac0ee746befc74c6f24e39b14aa465c5b", null ],
    [ "BaseAddress", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#a5b0cb702919e4a1f5fadb97dfd8489cc", null ],
    [ "Bufforable", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#ab23eb30ddb607446b3dfba6ed873925c", null ],
    [ "Cacheable", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#a9051bea635c6c3013894e51bdd1beaa1", null ],
    [ "Power", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#abc56c5af1051778e42061aea39286c42", null ],
    [ "PrivilegedAccess", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#aa5a1e6195edea1208134600f27b5d3da", null ],
    [ "RegionNumber", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#a1884d03382d8b20fe3f87096906a1b06", null ],
    [ "Shareable", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#ad65a5c3dcaf70594973f618fa222c278", null ],
    [ "Size", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#afe1e2b17eb8df8ef6ee0ace34cb21759", null ],
    [ "UserAccess", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html#a51886d09683cafd88264153a5409b795", null ]
];