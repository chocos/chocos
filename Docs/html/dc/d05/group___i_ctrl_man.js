var group___i_ctrl_man =
[
    [ "oC_ICtrlMan_Activity_t", "d9/dc7/structo_c___i_ctrl_man___activity__t.html", [
      [ "EventsMask", "d9/dc7/structo_c___i_ctrl_man___activity__t.html#ac1755de9f52aef0d839d3a9b25afd5af", null ],
      [ "Height", "d9/dc7/structo_c___i_ctrl_man___activity__t.html#a184d49528cffcfc8ec211807d1870537", null ],
      [ "Position", "d9/dc7/structo_c___i_ctrl_man___activity__t.html#a1c09e5e9db86c5d2cb89fc1c5df1aadf", null ],
      [ "Screen", "d9/dc7/structo_c___i_ctrl_man___activity__t.html#af99480247458fe509a069639426f3469", null ],
      [ "Width", "d9/dc7/structo_c___i_ctrl_man___activity__t.html#a4ee55a170448d60b1f6fe43fca496673", null ]
    ] ],
    [ "oC_ICtrlMan_Activity_t", "dc/d05/group___i_ctrl_man.html#ga68aeb8756806832687870ab4bce0a5c7", null ],
    [ "oC_ICtrlMan_AddICtrl", "dc/d05/group___i_ctrl_man.html#ga9cbb8ae5ff2540941de3e463629c3d32", null ],
    [ "oC_ICtrlMan_ConfigureAllPossible", "dc/d05/group___i_ctrl_man.html#gaacb27ea074dfe39f65c19ec7c29de693", null ],
    [ "oC_ICtrlMan_GetICtrl", "dc/d05/group___i_ctrl_man.html#gad89a98e17fc03e74584e4cf2549dac14", null ],
    [ "oC_ICtrlMan_HasActivityOccurred", "dc/d05/group___i_ctrl_man.html#gad6943c18dd2ec0ca498b51aedb5f56e7", null ],
    [ "oC_ICtrlMan_ReadCursorPosition", "dc/d05/group___i_ctrl_man.html#ga6a686aee65f4661d0d13878eec234965", null ],
    [ "oC_ICtrlMan_RegisterActivity", "dc/d05/group___i_ctrl_man.html#ga5c4d4b06ff9d9be9b9f3992f4650e53a", null ],
    [ "oC_ICtrlMan_RemoveICtrl", "dc/d05/group___i_ctrl_man.html#ga3254cca98bdbdbf95d97e3d018600106", null ],
    [ "oC_ICtrlMan_TurnOffModule", "dc/d05/group___i_ctrl_man.html#ga7b100cdb703ac5ca5bd707c57e75d198", null ],
    [ "oC_ICtrlMan_UnconfigureAll", "dc/d05/group___i_ctrl_man.html#ga330008094314ce3dd97ecedb1cff8903", null ],
    [ "oC_ICtrlMan_UnregisterActivity", "dc/d05/group___i_ctrl_man.html#gaabeb4d0b870d406e23821bd0515c80cb", null ],
    [ "oC_ICtrlMan_WaitForActivity", "dc/d05/group___i_ctrl_man.html#ga03b1528efbfa3a2220ad20e866ecd1ff", null ],
    [ "oC_ICtrlMan_WaitForActivityFinish", "dc/d05/group___i_ctrl_man.html#ga72cb73d2b72d48f9d6436f4bb3ad8220", null ],
    [ "oC_ICtrlMan_WaitForAnyNewActivity", "dc/d05/group___i_ctrl_man.html#ga2f5cbc0e8b5f2046a6c98da8a6674e31", null ],
    [ "oC_ICtrlMan_WaitForRawEvent", "dc/d05/group___i_ctrl_man.html#ga09c253847f0f751e9c5c603de2bce4a8", null ],
    [ "oC_List", "dc/d05/group___i_ctrl_man.html#gac4785bcf988550e86fbdfe1e7301e737", null ]
];