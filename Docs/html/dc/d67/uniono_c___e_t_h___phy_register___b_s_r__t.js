var uniono_c___e_t_h___phy_register___b_s_r__t =
[
    [ "AutoNegotiateAbility", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#abc36efa6ac19f8afa142d43564b6d2f8", null ],
    [ "AutoNegotiateComplete", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a5405411835eea66a84c10fba775b7fa1", null ],
    [ "ExtendedCapabilities", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a60d8ecd5b2b46b67078460dce4fef536", null ],
    [ "ExtendedStatus", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#abcef7c340752fb44d9d9b82870d22620", null ],
    [ "FullDuplex100BASET2", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a6597803a1cd720c73c197bc073efc25f", null ],
    [ "FullDuplex100BASETX", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#ab09f27e42e4fe79315ef8c72fe73f296", null ],
    [ "FullDuplex10BASET", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#ac3e3e062f3b67c2374f56f9c88da88e1", null ],
    [ "HalfDuplex100BASET2", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a9bbc0bf98a5bbfed8161b1f0864ef295", null ],
    [ "HalfDuplex100BASETX", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a966331160aec4c6c0335c3a4162ebcc7", null ],
    [ "HalfDuplex10BASET", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a24548ddd5dbe06141d8d8fd19bf2e9ab", null ],
    [ "JabberDetect", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#aed19e009fdcaa3814b66641337dee942", null ],
    [ "LinkStatus", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#aad070cc8910a1bf8083ee8bb8eb07b43", null ],
    [ "RemoteFault", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#aca478eec4fdb4ca8aac75fa46b7e2672", null ],
    [ "Reserved16_31", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#aba5db68147cabbe25bbb3e00eac220d6", null ],
    [ "Reserved6_7", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#ad2216467ecf60af1280bac1cbef40bd9", null ],
    [ "T4100BASE", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#afb9b12ec3685bfed6f84ed7706c7a834", null ],
    [ "Value", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html#a8e0dcce3428a8051614e852b8836d0d1", null ]
];