var structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t =
[
    [ "HighCapacityCard", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t.html#a075f65cb8cfa1f68df71e34fc4da64ea", null ],
    [ "InitializationCompleted", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t.html#a984282c8fab990498f4c2b8c68fa8769", null ],
    [ "SwitchingToLowVoltageAccepted", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t.html#ad3d5ba96baadab3c13a9c5b4f2179849", null ],
    [ "VoltageSupported", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t.html#af404968d2c3c54e30126b19bc6ef23fb", null ]
];