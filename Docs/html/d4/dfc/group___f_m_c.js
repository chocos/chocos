var group___f_m_c =
[
    [ "oC_FMC_Config_t", "d1/d44/structo_c___f_m_c___config__t.html", [
      [ "ChipInfo", "d1/d44/structo_c___f_m_c___config__t.html#ab6f2ed4cc46b246dedf4126d1aa2b7be", null ],
      [ "HeapUsage", "d1/d44/structo_c___f_m_c___config__t.html#a83678a4157a8c85bd74dcf10273a0947", null ],
      [ "MaximumTimeForConfiguration", "d1/d44/structo_c___f_m_c___config__t.html#a39815b9f5f16774a05663c708022996f", null ],
      [ "NANDFlash", "d1/d44/structo_c___f_m_c___config__t.html#a61117bca11545206e33fa44f9cd7b309", null ],
      [ "NORFlash", "d1/d44/structo_c___f_m_c___config__t.html#ac601945669d137253a62b8b7936a10b3", null ],
      [ "PSRAM", "d1/d44/structo_c___f_m_c___config__t.html#a3677d6d9c49222169ecd03fd194f30f5", null ],
      [ "SDRAM", "d1/d44/structo_c___f_m_c___config__t.html#ada2d0dcd8069af4ea08b9901424d7ba9", null ]
    ] ],
    [ "oC_FMC_Config_t", "d4/dfc/group___f_m_c.html#gad21ab4658412ef155fd6754fe9579985", null ],
    [ "oC_FMC_Context_t", "d4/dfc/group___f_m_c.html#ga6826ca3ce5792bb6363d39d119f27372", null ],
    [ "oC_FMC_SDRAM_CommandData_t", "d4/dfc/group___f_m_c.html#ga471354bfb88a9c2e6ee9ca77da5f3fa8", null ],
    [ "oC_FMC_HeapUsage_t", "d4/dfc/group___f_m_c.html#ga5bff631a337ee130ba5943230fe17afa", [
      [ "oC_FMC_HeapUsage_UseAsHeapIfPossible", "d4/dfc/group___f_m_c.html#gga5bff631a337ee130ba5943230fe17afaa28c3e22dbda0a95db1c4eaf8609aae03", null ],
      [ "oC_FMC_HeapUsage_DontUseAsHeap", "d4/dfc/group___f_m_c.html#gga5bff631a337ee130ba5943230fe17afaa457170c7ad46593b9851b03182f414ab", null ]
    ] ],
    [ "oC_FMC_Protection_t", "d4/dfc/group___f_m_c.html#ga471cccb5eef221c995142108e883b841", [
      [ "oC_FMC_Protection_Default", "d4/dfc/group___f_m_c.html#gga471cccb5eef221c995142108e883b841aeeff0341a730dfaa3f00de1b6e23d607", null ],
      [ "oC_FMC_Protection_AllowWrite", "d4/dfc/group___f_m_c.html#gga471cccb5eef221c995142108e883b841a401f50c4c450000b554fdb1bc9389737", null ],
      [ "oC_FMC_Protection_AllowRead", "d4/dfc/group___f_m_c.html#gga471cccb5eef221c995142108e883b841a238626578db4f616c504ac8beaa1e70a", null ],
      [ "oC_FMC_Protection_AllowExecute", "d4/dfc/group___f_m_c.html#gga471cccb5eef221c995142108e883b841adb30e4dc6c5c87c7bda8320221164963", null ]
    ] ],
    [ "oC_FMC_SDRAM_Command_t", "d4/dfc/group___f_m_c.html#ga1b3160664167d83f9001a7fbf1c0db01", [
      [ "oC_FMC_SDRAM_Command_EnableClock", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01ae9e5e50d728c16d7228b1b425ab55ccb", null ],
      [ "oC_FMC_SDRAM_Command_Inhibit", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a80176decd7713d9963ca01f265625829", null ],
      [ "oC_FMC_SDRAM_Command_Nop", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01ab7ef7d4028547aed1a38e146e9b2b4af", null ],
      [ "oC_FMC_SDRAM_Command_LoadModeRegister", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a943e34442732cc6bf25ed1dacda77ad8", null ],
      [ "oC_FMC_SDRAM_Command_Active", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01adcc9088e4864bd4c7b5a0d52c7ae0316", null ],
      [ "oC_FMC_SDRAM_Command_Read", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a74626f282a00a11dcbb2c7414e2ee9bc", null ],
      [ "oC_FMC_SDRAM_Command_Write", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a8669f71b6ed28044d194c0e9b2bdbf03", null ],
      [ "oC_FMC_SDRAM_Command_Precharge", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a6216a949639006d58d203855ae23ada7", null ],
      [ "oC_FMC_SDRAM_Command_BurstTerminate", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a6aeaf4c0e7fafc46d8b7554e7da25111", null ],
      [ "oC_FMC_SDRAM_Command_AutoRefresh", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01ad448e48fadd37bc5b62f1d7855db65f9", null ],
      [ "oC_FMC_SDRAM_Command_SelfRefresh", "d4/dfc/group___f_m_c.html#gga1b3160664167d83f9001a7fbf1c0db01a8af4c8f154d2c45ff09553874399bd22", null ]
    ] ],
    [ "oC_FMC_Configure", "d4/dfc/group___f_m_c.html#ga5292091708066e50e33530e11582e305", null ],
    [ "oC_FMC_Ioctl", "d4/dfc/group___f_m_c.html#gacee725426283dcd531557dd0f3cd4c96", null ],
    [ "oC_FMC_Read", "d4/dfc/group___f_m_c.html#ga9c8850a3de766e95837a768ac7109b76", null ],
    [ "oC_FMC_SDRAM_SendCommand", "d4/dfc/group___f_m_c.html#ga639cfb959d1818b0cb6f720d962471ae", null ],
    [ "oC_FMC_TurnOff", "d4/dfc/group___f_m_c.html#ga978713e6b855bab2d57441ba4a01a409", null ],
    [ "oC_FMC_TurnOn", "d4/dfc/group___f_m_c.html#ga81c5463b804361cb4b2b5b3fda818d7d", null ],
    [ "oC_FMC_Unconfigure", "d4/dfc/group___f_m_c.html#ga6b9b46de8fefaeffb3d51a8785dfdd45", null ],
    [ "oC_FMC_Write", "d4/dfc/group___f_m_c.html#ga980a84088fc8fdfae312e180e029adf9", null ]
];