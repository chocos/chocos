var oc__serviceman_8c =
[
    [ "ServiceData_t", "dd/de8/struct_service_data__t.html", null ],
    [ "GetServiceData", "d4/d2c/oc__serviceman_8c.html#a8562f56afa6a29aa4ac8a41b51323702", null ],
    [ "oC_ServiceMan_GetService", "d4/d2c/oc__serviceman_8c.html#a9bda19c843735700f77300ea66eb8656", null ],
    [ "oC_ServiceMan_IsServiceActive", "d4/d2c/oc__serviceman_8c.html#a41e694fe17b35c70834476f35c757a82", null ],
    [ "oC_ServiceMan_StartAllPossible", "d4/d2c/oc__serviceman_8c.html#a74917ffc299308ae2aca2c15e50014a8", null ],
    [ "oC_ServiceMan_StartService", "d4/d2c/oc__serviceman_8c.html#a0c286a1aa74fcc40cb6124602eb9561c", null ],
    [ "oC_ServiceMan_StopAllPossible", "d4/d2c/oc__serviceman_8c.html#a6f6334711c38b166bcb81611d7d35295", null ],
    [ "oC_ServiceMan_StopService", "d4/d2c/oc__serviceman_8c.html#ab4ac886a7acb0b99ab6c66b04ae3babd", null ],
    [ "oC_ServiceMan_TurnOff", "d4/d2c/oc__serviceman_8c.html#a10f1ed2b2abb2d1345afaf8ae677837c", null ],
    [ "oC_ServiceMan_TurnOn", "d4/d2c/oc__serviceman_8c.html#affa2b0ee0bc5810576aba129c59566ce", null ],
    [ "ServiceData_Delete", "d4/d2c/oc__serviceman_8c.html#a5f3fcf24ccf0391ee22ba80d69674306", null ],
    [ "ServiceData_New", "d4/d2c/oc__serviceman_8c.html#a21e2b094fafc147e38d5e867888e2aa1", null ],
    [ "StartService", "d4/d2c/oc__serviceman_8c.html#a5b07994e3baa8b846fd3acccbbdbf52f", null ],
    [ "StopService", "d4/d2c/oc__serviceman_8c.html#af62025afb2834b328153baba1834e7df", null ]
];