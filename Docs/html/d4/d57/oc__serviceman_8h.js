var oc__serviceman_8h =
[
    [ "oC_ServiceMan_GetService", "d4/d57/oc__serviceman_8h.html#a9bda19c843735700f77300ea66eb8656", null ],
    [ "oC_ServiceMan_IsServiceActive", "d4/d57/oc__serviceman_8h.html#a41e694fe17b35c70834476f35c757a82", null ],
    [ "oC_ServiceMan_StartAllPossible", "d4/d57/oc__serviceman_8h.html#a74917ffc299308ae2aca2c15e50014a8", null ],
    [ "oC_ServiceMan_StartService", "d4/d57/oc__serviceman_8h.html#a0c286a1aa74fcc40cb6124602eb9561c", null ],
    [ "oC_ServiceMan_StopAllPossible", "d4/d57/oc__serviceman_8h.html#a6f6334711c38b166bcb81611d7d35295", null ],
    [ "oC_ServiceMan_StopService", "d4/d57/oc__serviceman_8h.html#ab4ac886a7acb0b99ab6c66b04ae3babd", null ],
    [ "oC_ServiceMan_TurnOff", "d4/d57/oc__serviceman_8h.html#a10f1ed2b2abb2d1345afaf8ae677837c", null ],
    [ "oC_ServiceMan_TurnOn", "d4/d57/oc__serviceman_8h.html#affa2b0ee0bc5810576aba129c59566ce", null ]
];