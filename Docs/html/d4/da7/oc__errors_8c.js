var oc__errors_8c =
[
    [ "SavedError_t", "dc/d84/struct_saved_error__t.html", null ],
    [ "oC_GetErrorString", "d4/da7/oc__errors_8c.html#ae514a54614e7b931a62a14cbd36554d3", null ],
    [ "oC_ReadLastError", "d4/da7/oc__errors_8c.html#a2c03dfc50caba7aceb68d04e75d4e8bc", null ],
    [ "oC_SaveErrorFunction", "d4/da7/oc__errors_8c.html#ae46f892f264f9675cafe383d90419626", null ],
    [ "oC_SetLockSavingErrors", "d4/da7/oc__errors_8c.html#a5835538793592b7852bec30faf02233a", null ],
    [ "oC_ErrorsStrings", "d4/da7/oc__errors_8c.html#a03b2cc69485de14608a1cc72221e49b3", null ]
];