var oc__ft5336_8h =
[
    [ "oC_FT5336_Config_t", "d8/d73/structo_c___f_t5336___config__t.html", "d8/d73/structo_c___f_t5336___config__t" ],
    [ "oC_FT5336_Context_t", "d4/d8d/oc__ft5336_8h.html#aaa9d6d5819159b7eb4b5149706e6cbe9", null ],
    [ "oC_FT5336_Mode_t", "d4/d8d/oc__ft5336_8h.html#a9b39f649a5cb2de00420fc77db68503c", [
      [ "oC_FT5336_Mode_InterruptMode", "d4/d8d/oc__ft5336_8h.html#a9b39f649a5cb2de00420fc77db68503ca5e2cc660b4a270f8303cddcc6f23d2d7", null ],
      [ "oC_FT5336_Mode_PollingMode", "d4/d8d/oc__ft5336_8h.html#a9b39f649a5cb2de00420fc77db68503cadcb5a244253b11bf9c27b0eea8880e1b", null ]
    ] ],
    [ "oC_FT5336_Configure", "d4/d8d/oc__ft5336_8h.html#a5df7e723fbba44d0839f3eb5adff66a2", null ],
    [ "oC_FT5336_IsEventSupported", "d4/d8d/oc__ft5336_8h.html#a2452669f5fede07c5a86bd0909b04ecb", null ],
    [ "oC_FT5336_IsTurnedOn", "d4/d8d/oc__ft5336_8h.html#afa6b1e7a7090cd3d6a06a12940d1eea7", null ],
    [ "oC_FT5336_TurnOff", "d4/d8d/oc__ft5336_8h.html#ab12d053ae91d0f6a59df0e2a2ac0d853", null ],
    [ "oC_FT5336_TurnOn", "d4/d8d/oc__ft5336_8h.html#a58f6daf0422a44821e5070cefa0b7b86", null ],
    [ "oC_FT5336_Unconfigure", "d4/d8d/oc__ft5336_8h.html#a4f292e7220dd76f082b296fab701a664", null ],
    [ "oC_FT5336_WaitForInput", "d4/d8d/oc__ft5336_8h.html#af1c5f5ec1a51fea9d52ac9222f20cd55", null ]
];