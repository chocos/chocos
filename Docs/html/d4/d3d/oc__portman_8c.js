var oc__portman_8c =
[
    [ "PortReservation_t", "d2/dd7/struct_port_reservation__t.html", "d2/dd7/struct_port_reservation__t" ],
    [ "ModuleRegistration_t", "d1/d24/struct_module_registration__t.html", "d1/d24/struct_module_registration__t" ],
    [ "GetFreePort", "d4/d3d/oc__portman_8c.html#acc0dd08ebe7a645db10d098b0d9cec45", null ],
    [ "GetModuleRegistration", "d4/d3d/oc__portman_8c.html#a9027f495bf7685e20f7b8dfcc04add58", null ],
    [ "GetNextProcessReservation", "d4/d3d/oc__portman_8c.html#a9b8100b7d394d641f9f9432ec18a01d4", null ],
    [ "GetPortReservation", "d4/d3d/oc__portman_8c.html#aea59b5091f70b55fc2fd063e14f13f9a", null ],
    [ "ModuleRegistration_Delete", "d4/d3d/oc__portman_8c.html#a0525e6e61860d4cbdcd9dd47af5e93cb", null ],
    [ "ModuleRegistration_New", "d4/d3d/oc__portman_8c.html#a563aa68818f67364183fe035124d6ce8", null ],
    [ "oC_PortMan_IsPortReserved", "d2/d9a/group___port_man.html#gad8af999b91d3b11fd57c33cc264c1ec2", null ],
    [ "oC_PortMan_IsPortReservedBy", "d2/d9a/group___port_man.html#ga552d3073c3fc1a444ae1555abad91b90", null ],
    [ "oC_PortMan_RegisterModule", "d2/d9a/group___port_man.html#ga93287dded812a729798ce8ae140697cc", null ],
    [ "oC_PortMan_ReleaseAllPortsOf", "d2/d9a/group___port_man.html#ga48fc59b265694b924c6094b3dbfc16bd", null ],
    [ "oC_PortMan_ReleasePort", "d2/d9a/group___port_man.html#ga0096c83cc638a2380db6c4d55096349b", null ],
    [ "oC_PortMan_ReservePort", "d2/d9a/group___port_man.html#gaf04e0e51a74be74761d65c744c50bf90", null ],
    [ "oC_PortMan_TurnOff", "d2/d9a/group___port_man.html#ga812e759434c7d9d2feb07f1e9b3fa95c", null ],
    [ "oC_PortMan_TurnOn", "d2/d9a/group___port_man.html#ga9f5785bbf7423e38cdd942430ae08d19", null ],
    [ "oC_PortMan_UnregisterModule", "d2/d9a/group___port_man.html#ga0f93e3b22ad52ab5fa609f732760b6ed", null ],
    [ "PortReservation_Delete", "d4/d3d/oc__portman_8c.html#a1812d9dfd74bafd3eadc563b7e7b7113", null ],
    [ "PortReservation_New", "d4/d3d/oc__portman_8c.html#ac4e39588f55f6e5a7e216881d0e54b36", null ],
    [ "WaitForFreePortRelease", "d4/d3d/oc__portman_8c.html#a5fd1928db65d06b0c7a0bb1c3525952f", null ],
    [ "WaitForPortRelease", "d4/d3d/oc__portman_8c.html#a98acb3c1491eac04a533017d0516c861", null ]
];