var oc__event_8c =
[
    [ "Event_t", "d8/d34/struct_event__t.html", null ],
    [ "oC_Event_ClearStateBits", "d0/def/group___event.html#gaabe9728ecb2ada74c2fade201e899368", null ],
    [ "oC_Event_Delete", "d0/def/group___event.html#ga4a796e93351123c4530d36883463e54e", null ],
    [ "oC_Event_GetState", "d0/def/group___event.html#ga30253a6fc9edae1f6fb16df95b1f1b25", null ],
    [ "oC_Event_IsCorrect", "d0/def/group___event.html#ga9f2e7d398b02e8086e0bf698c2ea9766", null ],
    [ "oC_Event_New", "d0/def/group___event.html#gaf4180593560905b0b08017b529765483", null ],
    [ "oC_Event_ProtectDelete", "d0/def/group___event.html#ga63b77b5744afd99e42a6d2a3c01b2165", null ],
    [ "oC_Event_ReadState", "d0/def/group___event.html#gac6a2874af2a4b928e37693db11ee9bde", null ],
    [ "oC_Event_SetState", "d0/def/group___event.html#gab2085317c76d85664d15436c603d5d83", null ],
    [ "oC_Event_SetStateBits", "d0/def/group___event.html#ga04649e40468d78a050db552af5397506", null ],
    [ "oC_Event_WaitForBitClear", "d0/def/group___event.html#gab73d0e9b737e9d6ca0757c67fe4db64a", null ],
    [ "oC_Event_WaitForBitSet", "d0/def/group___event.html#ga0cbb17338eb282552049d660c016cb28", null ],
    [ "oC_Event_WaitForState", "d0/def/group___event.html#ga4921358f46cbbb79d1437af76c8d30ed", null ],
    [ "oC_Event_WaitForValue", "d0/def/group___event.html#ga0b44728d87e6c39ba1a675f45df72fc5", null ]
];