var structo_c___s_d_m_m_c___card_info__t =
[
    [ "CardId", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a7fc937a9a21dac87383214645c552906", null ],
    [ "CardInterface", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a158133231c9d0e2aa671e890cc47f500", null ],
    [ "CardSize", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a80e821a3fbbc015472c56f2e99b3e50d", null ],
    [ "CardType", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a42214fcb59455d9fdf56ab752e485a62", null ],
    [ "DiskId", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a155f2b2400c63a183701f2ce39d45dec", null ],
    [ "FileFormat", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a64f0620bd61e6c383c8d535021e397b0", null ],
    [ "NumberOfSectors", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a97ad91aa426582a55c6d38e1480f1cb0", null ],
    [ "RelativeCardAddress", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a1c35c56cd79d76548a9915a42bd4ef9b", null ],
    [ "SectorSize", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html#a8fd1d60aa3e4d98c596e7476ab66a752", null ]
];