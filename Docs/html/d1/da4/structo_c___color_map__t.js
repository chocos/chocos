var structo_c___color_map__t =
[
    [ "ActiveLayer", "d1/da4/structo_c___color_map__t.html#affa001d126798ef8ec827c4ad831706b", null ],
    [ "ColorFormat", "d1/da4/structo_c___color_map__t.html#adb61686ba228c58e43d161e2348b7f44", null ],
    [ "FormatSize", "d1/da4/structo_c___color_map__t.html#aad29ca23ae97f6cd141ef92efffb0fb8", null ],
    [ "GenericWriteMap", "d1/da4/structo_c___color_map__t.html#a5c4f0c04939357f2c5a1adba1f6a4378", null ],
    [ "Height", "d1/da4/structo_c___color_map__t.html#a184d49528cffcfc8ec211807d1870537", null ],
    [ "Layers", "d1/da4/structo_c___color_map__t.html#a5eaed25bca927c8de5bb0fa985d9d7c8", null ],
    [ "MagicNumber", "d1/da4/structo_c___color_map__t.html#ac2d9a2b3478db6dc43ad665d6a6eda8c", null ],
    [ "Map", "d1/da4/structo_c___color_map__t.html#a904050ddb989fcb44cb632f2836f043d", null ],
    [ "NumberOfLayers", "d1/da4/structo_c___color_map__t.html#abad6a18c1d9b0ea876171aee8739c2a8", null ],
    [ "ReadMap", "d1/da4/structo_c___color_map__t.html#a06cac7deb550ce01f614766d7bd61dd5", null ],
    [ "Width", "d1/da4/structo_c___color_map__t.html#a4ee55a170448d60b1f6fe43fca496673", null ],
    [ "WriteMap", "d1/da4/structo_c___color_map__t.html#a87e0bff60808b211b10cbdd9fc448c03", null ]
];