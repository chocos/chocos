var structo_c___s_d_m_m_c___context__t =
[
    [ "oC_List", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#afb48aee4196f298bcd44dc00d89c0ebf", null ],
    [ "DetectionPeriod", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#af1d93245ee237eb1f1c83c5de6e7f5c4", null ],
    [ "DetectionPin", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#a474233490940a500ff80c1bf9c9308ad", null ],
    [ "Interface", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#ad2e1b17129731d1c71c2c4109b6810f0", null ],
    [ "ModeContext", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#a31764128e79201da68dea0a951362f88", null ],
    [ "ModuleBusy", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#aca7d8469aed2225489f883b545b91837", null ],
    [ "NumberOfRetries", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#ae9e9f5c17dd69960a71c24f442076fe4", null ],
    [ "NumberOfSectors", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#a97ad91aa426582a55c6d38e1480f1cb0", null ],
    [ "ObjectControl", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#af8aa619116d8a74934500e135a9dbb43", null ],
    [ "PowerMode", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#aec55b612ee80c154b760c2e45a0c46e1", null ],
    [ "SectorSize", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#a8fd1d60aa3e4d98c596e7476ab66a752", null ],
    [ "TransferMode", "d1/d2b/structo_c___s_d_m_m_c___context__t.html#aeeb77d21464922439f3ebe551f76c5a3", null ]
];