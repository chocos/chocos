var st_2stm32f7_2stm32f746ngh6_2oc__machine__defs_8h =
[
    [ "oC_MACHINE", "d9/d13/group___m_defs.html#ga17e714c24754fda8ad0c59a629d76bc9", null ],
    [ "oC_MACHINE_CORTEX", "d9/d13/group___m_defs.html#gaafeced6e97cb1d9e69950aabba04cad3", null ],
    [ "oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST", "d9/d13/group___m_defs.html#ga977d4506b3b77b04d75bfa356052afa9", null ],
    [ "oC_MACHINE_DMA_ENCODING_VALUE_WIDTH", "d9/d13/group___m_defs.html#gae359fc0409d0fec520cf04f640f6e741", null ],
    [ "oC_MACHINE_DMA_SIGNAL_TYPE_LIST", "d9/d13/group___m_defs.html#ga6421f3251eeb6049aeb9396122e4b7f3", null ],
    [ "oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH", "d9/d13/group___m_defs.html#ga1b59432461797d48c776dca57fb067de", null ],
    [ "oC_MACHINE_FAMILY", "d9/d13/group___m_defs.html#ga06fee27de7385ae0fc564761b29fa167", null ],
    [ "oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY", "d9/d13/group___m_defs.html#ga3b01abcfc28f0bba608896a3d9a01a0f", null ],
    [ "oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY", "d9/d13/group___m_defs.html#ga24d3e9a226bac13ef94a91a7f6cc4b9a", null ],
    [ "oC_MACHINE_MAXIMUM_FREQUENCY", "d9/d13/group___m_defs.html#gae183c3ac3fed3feec338cdd9eb3ae1bb", null ]
];