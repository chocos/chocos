var oc__diskman_8h =
[
    [ "oC_DiskList_t", "d8/daa/group___disk_man.html#gaf52af9e62b907246c4992f72a31cca92", null ],
    [ "oC_DiskMan_AddDisk", "d8/daa/group___disk_man.html#gad4bf8a9845228aaace3e3689bf5d4933", null ],
    [ "oC_DiskMan_GetDisk", "d8/daa/group___disk_man.html#ga3121fcefe695f06a2d73672b49a00d72", null ],
    [ "oC_DiskMan_GetList", "d8/daa/group___disk_man.html#ga9ac3177474521bb7435c97b4e4bbbab1", null ],
    [ "oC_DiskMan_RemoveDisk", "d8/daa/group___disk_man.html#ga0628171894ed995ce5ed2ed2ed6bf2d0", null ],
    [ "oC_DiskMan_TurnOff", "d8/daa/group___disk_man.html#ga078736994fe54187d4660a9765b171a9", null ],
    [ "oC_DiskMan_TurnOn", "d8/daa/group___disk_man.html#ga105b3b420ae370622aad967dff942c28", null ]
];