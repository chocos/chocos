var structo_c___f_m_c___config__t =
[
    [ "ChipInfo", "d1/d44/structo_c___f_m_c___config__t.html#ab6f2ed4cc46b246dedf4126d1aa2b7be", null ],
    [ "HeapUsage", "d1/d44/structo_c___f_m_c___config__t.html#a83678a4157a8c85bd74dcf10273a0947", null ],
    [ "MaximumTimeForConfiguration", "d1/d44/structo_c___f_m_c___config__t.html#a39815b9f5f16774a05663c708022996f", null ],
    [ "NANDFlash", "d1/d44/structo_c___f_m_c___config__t.html#a61117bca11545206e33fa44f9cd7b309", null ],
    [ "NORFlash", "d1/d44/structo_c___f_m_c___config__t.html#ac601945669d137253a62b8b7936a10b3", null ],
    [ "PSRAM", "d1/d44/structo_c___f_m_c___config__t.html#a3677d6d9c49222169ecd03fd194f30f5", null ],
    [ "SDRAM", "d1/d44/structo_c___f_m_c___config__t.html#ada2d0dcd8069af4ea08b9901424d7ba9", null ]
];