var dir_1272ef6bd664b31328989ba6475869a3 =
[
    [ "oc_event.c", "d4/dae/oc__event_8c.html", "d4/dae/oc__event_8c" ],
    [ "oc_exchan.c", "d7/d82/oc__exchan_8c.html", "d7/d82/oc__exchan_8c" ],
    [ "oc_intman.c", "d1/d95/oc__intman_8c.html", "d1/d95/oc__intman_8c" ],
    [ "oc_kprint.c", "df/d9d/oc__kprint_8c.html", "df/d9d/oc__kprint_8c" ],
    [ "oc_ktime.c", "db/dbd/oc__ktime_8c.html", "db/dbd/oc__ktime_8c" ],
    [ "oc_memman.c", "dd/d7c/oc__memman_8c.html", "dd/d7c/oc__memman_8c" ],
    [ "oc_moduleman.c", "d1/dc6/oc__moduleman_8c.html", "d1/dc6/oc__moduleman_8c" ],
    [ "oc_mutex.c", "d8/d3d/oc__mutex_8c.html", "d8/d3d/oc__mutex_8c" ],
    [ "oc_news.c", "d6/d8d/oc__news_8c.html", "d6/d8d/oc__news_8c" ],
    [ "oc_process.c", "d3/d9f/oc__process_8c.html", "d3/d9f/oc__process_8c" ],
    [ "oc_processman.c", "d5/d8e/oc__processman_8c.html", "d5/d8e/oc__processman_8c" ],
    [ "oc_program.c", "d9/d39/oc__program_8c.html", "d9/d39/oc__program_8c" ],
    [ "oc_programman.c", "de/d82/oc__programman_8c.html", null ],
    [ "oc_queue.c", "d9/d68/oc__queue_8c.html", "d9/d68/oc__queue_8c" ],
    [ "oc_semaphore.c", "db/d46/oc__semaphore_8c.html", "db/d46/oc__semaphore_8c" ],
    [ "oc_service.c", "d0/d76/oc__service_8c.html", "d0/d76/oc__service_8c" ],
    [ "oc_serviceman.c", "d4/d2c/oc__serviceman_8c.html", "d4/d2c/oc__serviceman_8c" ],
    [ "oc_signal.c", "d1/dd0/oc__signal_8c.html", [
      [ "Signal_t", "d1/d11/struct_signal__t.html", null ]
    ] ],
    [ "oc_stream.c", "d0/d7e/oc__stream_8c.html", "d0/d7e/oc__stream_8c" ],
    [ "oc_streamman.c", "dc/da8/oc__streamman_8c.html", null ],
    [ "oc_thread.c", "d9/daa/oc__thread_8c.html", "d9/daa/oc__thread_8c" ],
    [ "oc_threadman.c", "d4/d58/oc__threadman_8c.html", "d4/d58/oc__threadman_8c" ],
    [ "oc_user.c", "d5/dda/oc__user_8c.html", "d5/dda/oc__user_8c" ],
    [ "oc_userman.c", "d1/d5e/oc__userman_8c.html", null ]
];