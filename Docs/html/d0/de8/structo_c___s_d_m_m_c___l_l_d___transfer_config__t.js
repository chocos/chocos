var structo_c___s_d_m_m_c___l_l_d___transfer_config__t =
[
    [ "BlockSize", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a688d15af937b4b2f16b9da1fd8c2d375", null ],
    [ "DataSize", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#ad7d69b4601cad602752c6cd180642cfb", null ],
    [ "Timeout", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#ad43f6298d4ab7f5e730d7c4672b2a08f", null ],
    [ "TransferDirection", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a6802550f645df326068f2787486b30f7", null ],
    [ "TransferMode", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a4709d7049bd1e1549a8f906147195f53", null ],
    [ "UseDma", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a28b10aaf3cbac859c245b08db53ed69d", null ]
];