var ti_2lm4f_2lm4f120h5qr_2oc__machine__defs_8h =
[
    [ "oC_MACHINE", "d9/d13/group___m_defs.html#ga17e714c24754fda8ad0c59a629d76bc9", null ],
    [ "oC_MACHINE_CORTEX", "d9/d13/group___m_defs.html#gaafeced6e97cb1d9e69950aabba04cad3", null ],
    [ "oC_Machine_DefaultFrequency", "d9/d13/group___m_defs.html#gaf417b81a4d24e0f663caf8c002c873ad", null ],
    [ "oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST", "d9/d13/group___m_defs.html#ga977d4506b3b77b04d75bfa356052afa9", null ],
    [ "oC_MACHINE_DMA_ENCODING_VALUE_WIDTH", "d9/d13/group___m_defs.html#gae359fc0409d0fec520cf04f640f6e741", null ],
    [ "oC_MACHINE_DMA_SIGNAL_TYPE_LIST", "d9/d13/group___m_defs.html#ga6421f3251eeb6049aeb9396122e4b7f3", null ],
    [ "oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH", "d9/d13/group___m_defs.html#ga1b59432461797d48c776dca57fb067de", null ],
    [ "oC_MACHINE_FAMILY", "d9/d13/group___m_defs.html#ga06fee27de7385ae0fc564761b29fa167", null ],
    [ "oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY", "d9/d13/group___m_defs.html#ga3b01abcfc28f0bba608896a3d9a01a0f", null ],
    [ "oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY", "d9/d13/group___m_defs.html#ga24d3e9a226bac13ef94a91a7f6cc4b9a", null ],
    [ "oC_MACHINE_MAXIMUM_FREQUENCY", "d9/d13/group___m_defs.html#gae183c3ac3fed3feec338cdd9eb3ae1bb", null ],
    [ "oC_MACHINE_MEMORY_ALIGNMENT_BYTES", "d9/d13/group___m_defs.html#ga55b6729a7c26236fdc40a5e96d446b69", null ],
    [ "oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY", "d9/d13/group___m_defs.html#ga2ac881c9a89c7dd3112b13878bfba0f8", null ],
    [ "oC_MACHINE_PRIO_BITS", "d9/d13/group___m_defs.html#ga56417f2874a94d15b6bbb25571973476", null ],
    [ "oC_MACHINE_STACK_PUSH_IS_DECREMENTING_ADDRESS", "d9/d13/group___m_defs.html#gabe4b33d1c0509cf8b826154ea0841129", null ],
    [ "oC_MACHINE_STACK_TOP_ALIGNMENT_BYTES", "d9/d13/group___m_defs.html#gaf1d55cbbcff38110b7f4ba78753929ce", null ],
    [ "oC_MACHINE_xPSR_INITIAL_VALUE", "d9/d13/group___m_defs.html#gae676a29ee442c9d1dea8d90948746a60", null ]
];