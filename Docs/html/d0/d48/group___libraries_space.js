var group___libraries_space =
[
    [ "1Word", "da/d8a/group___word.html", null ],
    [ "Array", "db/de6/group___array.html", null ],
    [ "Assertions", "d0/d62/group___assert.html", null ],
    [ "(Bits) The library for the bits operation", "d6/d81/group___bits.html", "d6/d81/group___bits" ],
    [ "Color", "d9/d25/group___color.html", null ],
    [ "DataPackage - Data Package Library", "d5/d42/group___data_package.html", "d5/d42/group___data_package" ],
    [ "Endianess - Endianess Library", "dc/d28/group___endianess.html", null ],
    [ "Errors", "d6/d05/group___errors.html", null ],
    [ "ForIf - For loop with else condition", "d3/da0/group___for_if.html", null ],
    [ "Frequency", "dd/d36/group___frequency.html", "dd/d36/group___frequency" ],
    [ "IfNot - If not condition", "da/db1/group___if_not.html", null ],
    [ "Math - Basic math operations", "d5/d98/group___math.html", "d5/d98/group___math" ],
    [ "Md5 - The MD5 hash function", "d1/dca/group___md5.html", null ],
    [ "Module - The module library", "db/d49/group___module.html", null ],
    [ "Null - The null pointer", "d9/d77/group___null.html", null ],
    [ "Object - The object library", "db/d47/group___object.html", "db/d47/group___object" ],
    [ "Stdio", "de/d92/group__stdio.html", null ],
    [ "String", "d4/d6e/group___string.html", "d4/d6e/group___string" ],
    [ "StringList", "d3/d77/group___string_list.html", null ],
    [ "Time - The time library", "d0/d80/group___time.html", "d0/d80/group___time" ],
    [ "Version - The version library", "da/df7/group___version.html", null ]
];