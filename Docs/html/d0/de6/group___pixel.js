var group___pixel =
[
    [ "oC_Pixel_t", "da/d9f/structo_c___pixel__t.html", [
      [ "Alpha", "da/d9f/structo_c___pixel__t.html#a492687466b6cf95d27d013fe6ed10e1d", null ],
      [ "Color", "da/d9f/structo_c___pixel__t.html#a94ca1dbfa4290212e997ca1bb9a8f4d5", null ],
      [ "Usage", "da/d9f/structo_c___pixel__t.html#a134e82d52966f8da0f3725ff0a6b5cff", null ]
    ] ],
    [ "oC_Pixel_Position_t", "d9/d80/structo_c___pixel___position__t.html", [
      [ "X", "d9/d80/structo_c___pixel___position__t.html#a043b0422789ad067271d57d4198f45e7", null ],
      [ "Y", "d9/d80/structo_c___pixel___position__t.html#a6dee8d8b1c2623beabd3358b34c757ae", null ]
    ] ],
    [ "oC_Pixel_Alpha_t", "d0/de6/group___pixel.html#ga8c858a9b076cd228766ca1dba7f0d6f7", null ],
    [ "oC_Pixel_ResolutionInt_t", "d0/de6/group___pixel.html#gadde5ae26f64eab14747cc41d5296eaf6", null ],
    [ "oC_Pixel_ResolutionUInt_t", "d0/de6/group___pixel.html#ga1161d51783044cf3431233b8e324fa00", null ],
    [ "oC_Pixel_Usage_t", "d0/de6/group___pixel.html#gaec5b519286c98850111a866f99a5ad87", [
      [ "oC_Pixel_Usage_NotUsed", "d0/de6/group___pixel.html#ggaec5b519286c98850111a866f99a5ad87a77598ae79d71c88f24db6d1b6c92dcf6", null ],
      [ "oC_Pixel_Usage_Used", "d0/de6/group___pixel.html#ggaec5b519286c98850111a866f99a5ad87ad601dfe011df2258ff151fda109df5a7", null ]
    ] ],
    [ "oC_PixelFormat_t", "d0/de6/group___pixel.html#ga6a666f43f90fbf63cfef96b1d388724f", [
      [ "oC_PixelFormat_ARGB8888", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fad3ad04476d46efc245b0d8f7795c56b9", null ],
      [ "oC_PixelFormat_RGB888", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724faf0fd3c795be8c9752b4ac0147f3ea0d3", null ],
      [ "oC_PixelFormat_RGB565", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa665f5096e4c3728caad41b793db0a995", null ],
      [ "oC_PixelFormat_ARGB1555", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa34535234feab806f13aacdd45b1c7f29", null ],
      [ "oC_PixelFormat_ARGB4444", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa80067d00b7f2bc797aaa72c5d64c59a4", null ],
      [ "oC_PixelFormat_L8", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa1d45d2932ea63a618b383c6c3bc5b9e6", null ],
      [ "oC_PixelFormat_AL44", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa751483412bb3ee52b98916d481ff26e1", null ],
      [ "oC_PixelFormat_AL88", "d0/de6/group___pixel.html#gga6a666f43f90fbf63cfef96b1d388724fa8f5e0102d6e5e56c6f915d33c8f1362b", null ]
    ] ]
];