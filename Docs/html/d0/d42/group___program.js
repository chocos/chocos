var group___program =
[
    [ "oC_Program_t", "d0/d42/group___program.html#ga2b96fae9e1eb7e17b4da793a2eb08581", null ],
    [ "oC_Program_Priority_t", "d0/d42/group___program.html#gacffb36669725a7b4c452a52f8fda5851", [
      [ "oC_Program_Priority_IdleProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a7ca93ae5a9c5e8b70cf793b203df4384", null ],
      [ "oC_Program_Priority_UserSpaceProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851ae339327d70da5399db04a7950772d9ab", null ],
      [ "oC_Program_Priority_UserSpaceDaemon", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a5f8d45ebcf1dbb7e736d2961bc30e93f", null ],
      [ "oC_Program_Priority_CoreSpaceProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851ac1eb6ef27d9555231da4aa1f2e5a4d4d", null ],
      [ "oC_Program_Priority_CoreSpaceDaemon", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a753224ca7d0f2c85b6bc3d0e9a0b37c4", null ],
      [ "oC_Program_Priority_SystemHelperProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a51a7ab08cf3cfb79c3553aeda3f27a07", null ],
      [ "oC_Program_Priority_SystemHelperDeamon", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851ab73a805155301183dbf74174c9fdf408", null ],
      [ "oC_Program_Priority_SystemHandlerProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851ac9dba6764f464a7867bb881dee786100", null ],
      [ "oC_Program_Priority_SystemHandlerDeamon", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851abc946aeb97651cd2792fa3ad16cf3460", null ],
      [ "oC_Program_Priority_SystemSecurityProgram", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a9b9d79bcc0b851d7e18efb897fc63941", null ],
      [ "oC_Program_Priority_SystemSecurityDeamon", "d0/d42/group___program.html#ggacffb36669725a7b4c452a52f8fda5851a1d4ce0d4797e216cb47b38dccceba1a0", null ]
    ] ],
    [ "oC_Program_Delete", "d0/d42/group___program.html#gaeed40c1f5f8be79ada16f79d3e790652", null ],
    [ "oC_Program_Exec", "d0/d42/group___program.html#ga8c71300201590b4cd0704f6487b94dab", null ],
    [ "oC_Program_Exec2", "d0/d42/group___program.html#gadb644c9fa5c2992a06cf1b85ea1b59ad", null ],
    [ "oC_Program_GetHeapMapSize", "d0/d42/group___program.html#ga4aa880163838337801cb268f1b50d3c6", null ],
    [ "oC_Program_GetName", "d0/d42/group___program.html#ga2b22a1f2a9264a6988d24c1801cbf391", null ],
    [ "oC_Program_GetPriority", "d0/d42/group___program.html#ga7db05ed79751d41c7fb2d0eec085ee47", null ],
    [ "oC_Program_GetStdErrName", "d0/d42/group___program.html#ga7b7e5952b4b2a54cecd02362452aff82", null ],
    [ "oC_Program_GetStdInName", "d0/d42/group___program.html#ga767ffd5ccd8455b199494060cde77fab", null ],
    [ "oC_Program_GetStdOutName", "d0/d42/group___program.html#gabce832012530bdeb0b8bd1a19ec26685", null ],
    [ "oC_Program_IsCorrect", "d0/d42/group___program.html#ga21b45383d98893c6189f10299c52b9bd", null ],
    [ "oC_Program_New", "d0/d42/group___program.html#gadfda878e05a260f3c5f8dde3da1d1401", null ],
    [ "oC_Program_NewProcess", "d0/d42/group___program.html#ga0b5ed7605c7e6665460cae18d56f4280", null ],
    [ "oC_Program_SetGotAddress", "d0/d42/group___program.html#gabba4a14ec7c4c0ae89c501245b80a51c", null ],
    [ "oC_Program_WaitForFinish", "d0/d42/group___program.html#gafd42ab7ed01e219a27ee196e6ad3faf7", null ]
];