var oc__service_8c =
[
    [ "Service_t", "dd/db8/struct_service__t.html", null ],
    [ "AreRequiredModulesEnabled", "d0/d76/oc__service_8c.html#a09cdb5bae8e4184d6299716b64bca205", null ],
    [ "MainThreadFinished", "d0/d76/oc__service_8c.html#a74b4239a9a7ef31751d29178b39a98d3", null ],
    [ "oC_Service_Delete", "d0/d76/oc__service_8c.html#a47696a340dffe66e69e3e3269a27f1a8", null ],
    [ "oC_Service_GetName", "d0/d76/oc__service_8c.html#a2df78a38d5f3e759f8dacba1793c4634", null ],
    [ "oC_Service_GetProcess", "d0/d76/oc__service_8c.html#adc99c1e8690f5e9f49f030fb74278635", null ],
    [ "oC_Service_IsActive", "d0/d76/oc__service_8c.html#ad0c0af004956ae0990a207614305ce46", null ],
    [ "oC_Service_IsCorrect", "d0/d76/oc__service_8c.html#a57372c9c4b4df24f5810f798f740f350", null ],
    [ "oC_Service_New", "d0/d76/oc__service_8c.html#a108a5ff7042aaf2c805361ec27036180", null ],
    [ "oC_Service_Start", "d0/d76/oc__service_8c.html#a509d8aaef41f56d614cfa80712936504", null ],
    [ "oC_Service_Stop", "d0/d76/oc__service_8c.html#a402d59007227a5c42ecb54c5a4957835", null ],
    [ "ServiceThread", "d0/d76/oc__service_8c.html#ab33e6cbbe6a83a089cf569d5d00ec4cd", null ]
];