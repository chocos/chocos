var st_2stm32f7_2lld_2dma_2oc__dma__lld_8c =
[
    [ "oC_DMA_LLD_ConfigurePeripheralTrade", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#ac3eb2e432533ed07faa72a799b7e0a69", null ],
    [ "oC_DMA_LLD_ConfigureSoftwareTrade", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#ad16f4e73607de17523b99c7d7e03174d", null ],
    [ "oC_DMA_LLD_DoesDmaHasAccessToAddress", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#a989770ec3810d0f92f7ece45b9f5863b", null ],
    [ "oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#aa6ced0a6377dde0abfbd1b8ddfb3a5f6", null ],
    [ "oC_DMA_LLD_IsChannelAvailable", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#aac83a4351267ad9a08c11aa3404825ac", null ],
    [ "oC_DMA_LLD_IsChannelSupportedOnDmaChannel", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#a7de3d259b4e8d1babc138af46749e341", null ],
    [ "oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#ad2c8a6a4b7e21be634ed5108e4d54473", null ],
    [ "oC_DMA_LLD_IsTransferCompleteOnChannel", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#a8fe53ec33ff533826ae066900a80ee65", null ],
    [ "oC_DMA_LLD_ReadChannelUsedReference", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#ace70759c66f73be0e5b810eed28fa84f", null ],
    [ "oC_DMA_LLD_RestoreDefaultStateOnChannel", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#af2b595a4f9fbe1a5bb75e5b7aea69dd1", null ],
    [ "oC_DMA_LLD_TurnOffDriver", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#a618dea7c3c0041f416029c7ee0682c06", null ],
    [ "oC_DMA_LLD_TurnOnDriver", "d0/d30/st_2stm32f7_2lld_2dma_2oc__dma__lld_8c.html#af019a32c90986a43df1e8666f8baf749", null ]
];