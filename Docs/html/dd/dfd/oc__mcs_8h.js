var oc__mcs_8h =
[
    [ "oC_MCS_AlignPointer", "d1/dd2/group___m_c_s.html#gabb8fe6fb221436179815928b1886a42a", null ],
    [ "oC_MCS_AlignSize", "d1/dd2/group___m_c_s.html#ga2672b122a780bbec9c5087192f4bd53e", null ],
    [ "oC_MCS_AlignStackPointer", "d1/dd2/group___m_c_s.html#ga397d3306ba2b73302d4db26187fed51b", null ],
    [ "oC_MCS_IsPointerAligned", "d1/dd2/group___m_c_s.html#ga9b75bd933a14cc1fc0e8207e46c8fcdf", null ],
    [ "oC_MCS_IsStackPointerAligned", "d1/dd2/group___m_c_s.html#ga5a5b4286d50c7700b4caf5ca29cd37e7", null ],
    [ "oC_MCS_IsStackPushDecrementPointer", "d1/dd2/group___m_c_s.html#ga3e05665e3f7b82012bbc5fb54430c601", null ],
    [ "oC_ContextExitHandler_t", "d1/dd2/group___m_c_s.html#gaea7219297e59a8501d4d821f4b57ebee", null ],
    [ "oC_ContextHandler_t", "d1/dd2/group___m_c_s.html#ga4c30c0fbcbec215c050dbdd725db4960", null ],
    [ "oC_FindNextStackHandler_t", "d1/dd2/group___m_c_s.html#gad2b6a4863f242642a2f4ce68f2539b86", null ],
    [ "oC_Stack_t", "d1/dd2/group___m_c_s.html#ga6f39b35dd8a86ec26ff7b83708f8b000", null ],
    [ "IRQn_Type", "d1/dd2/group___m_c_s.html#ga7e1129cd8a196f4284d41db3e82ad5c8", null ],
    [ "oC_InterruptPriotity_t", "d1/dd2/group___m_c_s.html#gaa8f1a29a1bf274f39c55d4e0fad50ffe", [
      [ "oC_InterruptPriority_Error", "d1/dd2/group___m_c_s.html#ggaa8f1a29a1bf274f39c55d4e0fad50ffea2c025c917901fc4f6447e1bf66de269a", null ],
      [ "oC_InterruptPriority_Minimum", "d1/dd2/group___m_c_s.html#ggaa8f1a29a1bf274f39c55d4e0fad50ffea4d1c545b4e1351418b3b9c23407bdd13", null ],
      [ "oC_InterruptPriority_Maximum", "d1/dd2/group___m_c_s.html#ggaa8f1a29a1bf274f39c55d4e0fad50ffea18c0f333f51d107d7ffc81f42186e0f1", null ]
    ] ],
    [ "oC_MCS_MemoryAccessMode_t", "d1/dd2/group___m_c_s.html#ga2f207b9a4b44e2dfe470e720af4235ee", [
      [ "oC_MCS_MemoryAccessMode_User", "d1/dd2/group___m_c_s.html#gga2f207b9a4b44e2dfe470e720af4235eeaa2d2ba18a7c7c993cdedb12799a6ef5e", null ],
      [ "oC_MCS_MemoryAccessMode_Privileged", "d1/dd2/group___m_c_s.html#gga2f207b9a4b44e2dfe470e720af4235eeae2590cc6c3a79fb16114dacddd2f4298", null ]
    ] ],
    [ "oC_MCS_AreInterruptsEnabled", "d1/dd2/group___m_c_s.html#ga9ccc3bfb9219582aad3c79565d1dab9a", null ],
    [ "oC_MCS_ChangeCriticalSectionCounter", "dd/dfd/oc__mcs_8h.html#a957f04d4e6c1d5e8d0709b26a416553b", null ],
    [ "oC_MCS_ClearHardFaultStatus", "d1/dd2/group___m_c_s.html#gaa1e2e99688ad5da9ae32c3b2b99dd9f9", null ],
    [ "oC_MCS_ConfigureMemoryRegion", "d1/dd2/group___m_c_s.html#gaa7353a8655eb6172f3c858b2f14789d3", null ],
    [ "oC_MCS_ConfigureSystemTimer", "d1/dd2/group___m_c_s.html#ga2a97fea6d66b9ea0d02a0a9ab516dce9", null ],
    [ "oC_MCS_Delay", "d1/dd2/group___m_c_s.html#gabf75a8e9c619e0a9c8bca0151244140e", null ],
    [ "oC_MCS_DisableInterrupt", "d1/dd2/group___m_c_s.html#gaae38dd1afee5d0f05bff6962fd0118f7", null ],
    [ "oC_MCS_DisableInterrupts", "d1/dd2/group___m_c_s.html#gaf460d303e7063a8ca78b6072c17b425e", null ],
    [ "oC_MCS_EnableAllPossibleEvents", "d1/dd2/group___m_c_s.html#gade25f25334de3c6cb932f0c8c7061b2e", null ],
    [ "oC_MCS_EnableInterrupt", "d1/dd2/group___m_c_s.html#ga75ed06ea80ca31a9ade94635ed675f84", null ],
    [ "oC_MCS_EnableInterrupts", "d1/dd2/group___m_c_s.html#ga2e1a540208a4eeb442ecf14181bcbd30", null ],
    [ "oC_MCS_EnterCriticalSection", "dd/dfd/oc__mcs_8h.html#a0da2cb6e7bc1022b2075ec2487f615c6", null ],
    [ "oC_MCS_ExitCriticalSection", "dd/dfd/oc__mcs_8h.html#afde7feca5c6e857119b3695a5441b05e", null ],
    [ "oC_MCS_GetCurrentMainStackPointer", "d1/dd2/group___m_c_s.html#ga9ace472883ed5bb2000583f83b4b5233", null ],
    [ "oC_MCS_GetCurrentProcessStackPointer", "d1/dd2/group___m_c_s.html#ga32dcff292e426528fbd25ab850f1044a", null ],
    [ "oC_MCS_GetCurrentStack", "d1/dd2/group___m_c_s.html#gac209b97587b4aa991588f9a679589d8c", null ],
    [ "oC_MCS_GetFreeStackSize", "d1/dd2/group___m_c_s.html#gaf18bfdb06bcfafa6b57a8a203e7bbadc", null ],
    [ "oC_MCS_GetHardFaultReason", "d1/dd2/group___m_c_s.html#ga380e14818a9d62f259cf27b7d66f4c28", null ],
    [ "oC_MCS_GetHardFaultReasonDescription", "d1/dd2/group___m_c_s.html#ga0419b8b66f220398bb4d8344528cbe31", null ],
    [ "oC_MCS_GetInterruptPriority", "d1/dd2/group___m_c_s.html#gaed05d6d7434cd2b18101beea4bd6c225", null ],
    [ "oC_MCS_GetMaximumNumberOfRegions", "d1/dd2/group___m_c_s.html#ga65edbc9a600a22db8b85a8de606462a3", null ],
    [ "oC_MCS_GetMemoryAccessMode", "d1/dd2/group___m_c_s.html#gabaa242cec3a7fb31e61ae0cabb6cb29c", null ],
    [ "oC_MCS_GetMinimumStackBufferSize", "d1/dd2/group___m_c_s.html#gabd656b5ba8662fab340bfc6ea989fba0", null ],
    [ "oC_MCS_GetNumberOfRegions", "d1/dd2/group___m_c_s.html#gac523c7093a19a7af8603276f01991d5c", null ],
    [ "oC_MCS_GetStackSize", "d1/dd2/group___m_c_s.html#gaaa4154f81f12a1b73da0d629f0d2b895", null ],
    [ "oC_MCS_GetSystemStack", "d1/dd2/group___m_c_s.html#gaba4656d4260d6e67d8ef6b4aaf8c62df", null ],
    [ "oC_MCS_HALT", "d1/dd2/group___m_c_s.html#gaf673c0cc431bc38a7cdf6cc68d1cd421", null ],
    [ "oC_MCS_InitializeModule", "d1/dd2/group___m_c_s.html#gaa5152bfb620e2000f700bcfd0bd48f87", null ],
    [ "oC_MCS_InitializeStack", "d1/dd2/group___m_c_s.html#ga27d7068c2260e978ac1a5f3031d06513", null ],
    [ "oC_MCS_IsCriticalSectionActive", "dd/dfd/oc__mcs_8h.html#ade05dc8768ecb6713e49332d5092fd11", null ],
    [ "oC_MCS_IsDebuggerConnected", "d1/dd2/group___m_c_s.html#ga188d28f83823b4cb0a40d2e81d228650", null ],
    [ "oC_MCS_IsInterruptEnabled", "d1/dd2/group___m_c_s.html#gabe440176470766555619a8c52fc00efb", null ],
    [ "oC_MCS_PrintToDebugger", "d1/dd2/group___m_c_s.html#ga4571b44c3a53cc52b878281ee90f2041", null ],
    [ "oC_MCS_ReadFreeRegionNumber", "d1/dd2/group___m_c_s.html#ga75dbf44fbe400c82540e8d6fea9996f0", null ],
    [ "oC_MCS_Reboot", "d1/dd2/group___m_c_s.html#ga9740ad6b8ca8b3666de635e5b1f01774", null ],
    [ "oC_MCS_ReturnToSystemStack", "d1/dd2/group___m_c_s.html#gaf428cf1cdc7ea6e8d0a049be8e56e860", null ],
    [ "oC_MCS_SetInterruptPriority", "d1/dd2/group___m_c_s.html#gaea18339a7b772ff73d0267b42cf85772", null ],
    [ "oC_MCS_SetMemoryAccessMode", "d1/dd2/group___m_c_s.html#gac0880fae9017cfb29449f198d8b32292", null ],
    [ "oC_MCS_SetNextStack", "d1/dd2/group___m_c_s.html#gabf25926bfd3db7456a17db182c5bb5ff", null ],
    [ "oC_MCS_CriticalSectionCounter", "dd/dfd/oc__mcs_8h.html#ac14ca5d4db785f180afa9e435bbde966", null ]
];