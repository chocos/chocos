var oc__screenman_8h =
[
    [ "oC_List", "d9/d5a/group___screen_man.html#ga1f9fd59416efbe9db0f18c6d9dba2e9d", null ],
    [ "oC_ScreenMan_AddScreen", "d9/d5a/group___screen_man.html#ga2136f048d2741472cec98d054c224619", null ],
    [ "oC_ScreenMan_ConfigureAll", "d9/d5a/group___screen_man.html#ga0506209c9b6bb5ff43d42ab05e880d79", null ],
    [ "oC_ScreenMan_GetDefaultScreen", "d9/d5a/group___screen_man.html#gabfcd7bb82ecdeaad664628e9c94bb08f", null ],
    [ "oC_ScreenMan_GetScreen", "d9/d5a/group___screen_man.html#ga7cbd4d0713186955e473b7bca80bf74d", null ],
    [ "oC_ScreenMan_RemoveScreen", "d9/d5a/group___screen_man.html#ga827b0c512fcebb418a313e5ca76e0a69", null ],
    [ "oC_ScreenMan_TurnOff", "d9/d5a/group___screen_man.html#ga5cfaccb59490b19995b97b3f559aadc8", null ],
    [ "oC_ScreenMan_TurnOn", "d9/d5a/group___screen_man.html#gaa2127c096da175dbcc586ea441f2c30e", null ],
    [ "oC_ScreenMan_UnconfigureAll", "d9/d5a/group___screen_man.html#ga1fdb48285ea3194e0a483ff34b9df832", null ]
];