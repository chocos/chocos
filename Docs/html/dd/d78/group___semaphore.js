var group___semaphore =
[
    [ "oC_Semaphore_t", "dd/d78/group___semaphore.html#ga7a049427bc57deb6733e94fb5330900e", null ],
    [ "oC_Semaphore_InitialValue_t", "dd/d78/group___semaphore.html#ga3150e854e7697481378aeac7f325b274", [
      [ "oC_Semaphore_InitialValue_AllTaken", "dd/d78/group___semaphore.html#gga3150e854e7697481378aeac7f325b274aec75e1f5aea4ff8e4e8dcac345a58f2a", null ],
      [ "oC_Semaphore_InitialValue_GiveBinary", "dd/d78/group___semaphore.html#gga3150e854e7697481378aeac7f325b274a41b094cf6e3a6a8baf11e0c32c3987aa", null ],
      [ "oC_Semaphore_InitialValue_MaxValue", "dd/d78/group___semaphore.html#gga3150e854e7697481378aeac7f325b274ac558f2084b146ba6f1229b132a1a05e4", null ]
    ] ],
    [ "oC_Semaphore_Type_t", "dd/d78/group___semaphore.html#ga68e32989539196cfaabbd1f5579785de", [
      [ "oC_Semaphore_Type_Binary", "dd/d78/group___semaphore.html#gga68e32989539196cfaabbd1f5579785dea87e47ddca28ea5ec6906cdc6a9f9f77f", null ],
      [ "oC_Semaphore_Type_Counting", "dd/d78/group___semaphore.html#gga68e32989539196cfaabbd1f5579785dea8460c10a8d015233b9ad24fcc34f2b93", null ]
    ] ],
    [ "oC_Semaphore_Delete", "dd/d78/group___semaphore.html#gaef2cecd9f028ef680def073dc254ec5a", null ],
    [ "oC_Semaphore_ForceValue", "dd/d78/group___semaphore.html#gac6eb4a971b6ea81a613ec89d90923dae", null ],
    [ "oC_Semaphore_Give", "dd/d78/group___semaphore.html#ga4c4abcd5927ae6c425defa5961e6e955", null ],
    [ "oC_Semaphore_GiveCounting", "dd/d78/group___semaphore.html#ga3a74c818efb7ec79aef13c4c282cb31a", null ],
    [ "oC_Semaphore_IsCorrect", "dd/d78/group___semaphore.html#ga4181805f8273e371abfada4c46ac1171", null ],
    [ "oC_Semaphore_New", "dd/d78/group___semaphore.html#ga72cc1faa6bb99fd0ca3bcbd96a0182a5", null ],
    [ "oC_Semaphore_Take", "dd/d78/group___semaphore.html#ga36d72a586b69c4ace3c23576afe00263", null ],
    [ "oC_Semaphore_TakeCounting", "dd/d78/group___semaphore.html#ga6c981d0dce90187ad08385b19bd8d09e", null ],
    [ "oC_Semaphore_TakeCountingSoft", "dd/d78/group___semaphore.html#gaea2991c4d02f735b228ffdce8c30f889", null ],
    [ "oC_Semaphore_Value", "dd/d78/group___semaphore.html#gac9a0e0781fe318aba193a57c806be43b", null ]
];