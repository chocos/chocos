var oc__gpio_8h =
[
    [ "oC_GPIO_Config_t", "db/d2b/structo_c___g_p_i_o___config__t.html", "db/d2b/structo_c___g_p_i_o___config__t" ],
    [ "oC_GPIO_Context_t", "dd/de9/oc__gpio_8h.html#abb1baf0bf0a546a2b47b675df362cc8f", null ],
    [ "oC_GPIO_Current_t", "dd/de9/oc__gpio_8h.html#a6eeacb9ff02a6a41809d2de2a1291ccc", [
      [ "oC_GPIO_Current_Default", "dd/de9/oc__gpio_8h.html#a6eeacb9ff02a6a41809d2de2a1291cccad53f5888f206b20fc6866e01881c4903", null ],
      [ "oC_GPIO_Current_Minimum", "dd/de9/oc__gpio_8h.html#a6eeacb9ff02a6a41809d2de2a1291ccca9b8329aca6afb00c95be11ed3d50d956", null ],
      [ "oC_GPIO_Current_Medium", "dd/de9/oc__gpio_8h.html#a6eeacb9ff02a6a41809d2de2a1291ccca8e9208611d37dd2eeb54c77e85ad03a4", null ],
      [ "oC_GPIO_Current_Maximum", "dd/de9/oc__gpio_8h.html#a6eeacb9ff02a6a41809d2de2a1291cccafd3349ff0c0b5e7e1d38a7944a6d9fbc", null ]
    ] ],
    [ "oC_GPIO_IntTrigger_t", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6", [
      [ "oC_GPIO_IntTrigger_Default", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a1801d1c2b5072f612fb488d4defd8dad", null ],
      [ "oC_GPIO_IntTrigger_Off", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a3d4a6d57d545b31b15424cacad74bfe9", null ],
      [ "oC_GPIO_IntTrigger_RisingEdge", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a7ccb4076b89305b9fc92824768f3a1b3", null ],
      [ "oC_GPIO_IntTrigger_FallingEdge", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a0920ac74802101fed4741e4c17f9ab31", null ],
      [ "oC_GPIO_IntTrigger_BothEdges", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a5fda4c5b48750bf4aba7d6d7c5ac4c54", null ],
      [ "oC_GPIO_IntTrigger_HighLevel", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6af3c2f41f57a01c5f9b97e1fc8117e5ce", null ],
      [ "oC_GPIO_IntTrigger_LowLevel", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6af7dab210f31cc4f40713fd4ae5f1d05c", null ],
      [ "oC_GPIO_IntTrigger_BothLevels", "dd/de9/oc__gpio_8h.html#a37989344883b592a55fabaf943cfdaa6a9f2ccee92e987d017bf43aef67720b0e", null ]
    ] ],
    [ "oC_GPIO_Ioctl_Command_t", "dd/de9/oc__gpio_8h.html#a013821d5abf9b52ec26446a7564c2f34", [
      [ "oC_GPIO_Ioctl_Command_TogglePins", "dd/de9/oc__gpio_8h.html#a013821d5abf9b52ec26446a7564c2f34a6d783e0abd7770e3b6409d672495e66d", null ],
      [ "oC_GPIO_Ioctl_Command_SetPinsState", "dd/de9/oc__gpio_8h.html#a013821d5abf9b52ec26446a7564c2f34acca33f59c1b72d99f51104cc6d456b23", null ],
      [ "oC_GPIO_Ioctl_Command_GetHighPinsState", "dd/de9/oc__gpio_8h.html#a013821d5abf9b52ec26446a7564c2f34a8e780f67b9f784e9747af0805d69be53", null ],
      [ "oC_GPIO_Ioctl_Command_GetLowPinsState", "dd/de9/oc__gpio_8h.html#a013821d5abf9b52ec26446a7564c2f34a3e0a9d87b5efb7902182149a96f43879", null ]
    ] ],
    [ "oC_GPIO_Mode_t", "dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9", [
      [ "oC_GPIO_Mode_Default", "dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9a920aaab8d88a27b573aa2f35dde20d91", null ],
      [ "oC_GPIO_Mode_Input", "dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9a66d3b12e427b84879a0e88dc9ed932d0", null ],
      [ "oC_GPIO_Mode_Output", "dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9a2db2140d8092fa38fa720fc6b1c7affa", null ],
      [ "oC_GPIO_Mode_Alternate", "dd/de9/oc__gpio_8h.html#a735ca4791bb728df375411265f9d26b9a5fb574ef2d8e84d57eda04770017898e", null ]
    ] ],
    [ "oC_GPIO_OutputCircuit_t", "dd/de9/oc__gpio_8h.html#af252b2b3d0709d8900826d64de62d2c5", [
      [ "oC_GPIO_OutputCircuit_OpenDrain", "dd/de9/oc__gpio_8h.html#af252b2b3d0709d8900826d64de62d2c5a80a14378f80904a0144663de67f10aaf", null ],
      [ "oC_GPIO_OutputCircuit_PushPull", "dd/de9/oc__gpio_8h.html#af252b2b3d0709d8900826d64de62d2c5a24f79b1fe895eb4b50580a03157c2a47", null ]
    ] ],
    [ "oC_GPIO_PinsState_t", "dd/de9/oc__gpio_8h.html#acac9a57008f07b81fe7f149a3f953f8b", [
      [ "oC_GPIO_PinsState_AllLow", "dd/de9/oc__gpio_8h.html#acac9a57008f07b81fe7f149a3f953f8ba825f00d082875e971df15c1793239ae4", null ],
      [ "oC_GPIO_PinsState_AllHigh", "dd/de9/oc__gpio_8h.html#acac9a57008f07b81fe7f149a3f953f8baff02ab68991c61cd6897093896713c00", null ]
    ] ],
    [ "oC_GPIO_Protection_t", "dd/de9/oc__gpio_8h.html#aeb6b79f81a0ac99ec4da2a0c1683bf9b", [
      [ "oC_GPIO_Protection_DontUnlockProtectedPins", "dd/de9/oc__gpio_8h.html#aeb6b79f81a0ac99ec4da2a0c1683bf9bac252037aa2dc730a956587089c1d1092", null ],
      [ "oC_GPIO_Protection_UnlockProtectedPins", "dd/de9/oc__gpio_8h.html#aeb6b79f81a0ac99ec4da2a0c1683bf9ba3f20fc0ea4c2e83afb9f8c4afd84761e", null ]
    ] ],
    [ "oC_GPIO_Pull_t", "dd/de9/oc__gpio_8h.html#ab3222c7f1fa9b653679f0e97d48c9752", [
      [ "oC_GPIO_Pull_Default", "dd/de9/oc__gpio_8h.html#ab3222c7f1fa9b653679f0e97d48c9752af360105acb44f8812210c8fcbe06a485", null ],
      [ "oC_GPIO_Pull_Up", "dd/de9/oc__gpio_8h.html#ab3222c7f1fa9b653679f0e97d48c9752a6bc4a0f88c254adb1c61b8032258f083", null ],
      [ "oC_GPIO_Pull_Down", "dd/de9/oc__gpio_8h.html#ab3222c7f1fa9b653679f0e97d48c9752ae0378bc3a5298b1a0fd1fb28d3f9831b", null ]
    ] ],
    [ "oC_GPIO_Speed_t", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33", [
      [ "oC_GPIO_Speed_Default", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33a77e8ec9eb8980066bdf51fd38ef2ab78", null ],
      [ "oC_GPIO_Speed_Minimum", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33a7aa5a9005ed38d1ad55c3d1f5851db24", null ],
      [ "oC_GPIO_Speed_Medium", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33acfa387e0f39dba6a2b5f9f5099544ac0", null ],
      [ "oC_GPIO_Speed_Maximum", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33a2542bdca756d8bb14d32a78c316823f6", null ],
      [ "oC_GPIO_Speed_NumberOfElements", "dd/de9/oc__gpio_8h.html#aac9ee5b2faa583cba3d7f1a69be62a33a3afe6c7ff200d27917868df64bfdd741", null ]
    ] ],
    [ "oC_GPIO_ArePinsCorrect", "dd/de9/oc__gpio_8h.html#a0c1a6d1968006dc2bfd318c27d8fceba", null ],
    [ "oC_GPIO_ArePinsDefined", "dd/de9/oc__gpio_8h.html#a9ede549ecf69ce089ace3e242688c2c9", null ],
    [ "oC_GPIO_ArePinsUnused", "dd/de9/oc__gpio_8h.html#a2711000c9e0a637e74155a82fc771426", null ],
    [ "oC_GPIO_CheckIsPinUsed", "dd/de9/oc__gpio_8h.html#a8c29f3795e3c656c245bf125e311fed6", null ],
    [ "oC_GPIO_Configure", "dd/de9/oc__gpio_8h.html#aea4673a7d8298336a70f7862fe79a1a2", null ],
    [ "oC_GPIO_FindPinByName", "dd/de9/oc__gpio_8h.html#a3f5eb16d0a0273afc6e3b72f87078b02", null ],
    [ "oC_GPIO_GetHighStatePins", "dd/de9/oc__gpio_8h.html#a01c45e9dd82e089c2b70276f27faabb4", null ],
    [ "oC_GPIO_GetLowStatePins", "dd/de9/oc__gpio_8h.html#ad606142e5308e9f70bf99b97230f4ac3", null ],
    [ "oC_GPIO_GetPinName", "dd/de9/oc__gpio_8h.html#a97a87d16f9420b534d99725080307377", null ],
    [ "oC_GPIO_GetPinName", "dd/de9/oc__gpio_8h.html#a7e46c317763fe662b71ec8237413d607", null ],
    [ "oC_GPIO_GetPinsFor", "dd/de9/oc__gpio_8h.html#a1bd4474d6b176857720088d76e457466", null ],
    [ "oC_GPIO_GetPinsMaskOfPins", "dd/de9/oc__gpio_8h.html#aa97f79c7978820a3f6f8ab12a51276d6", null ],
    [ "oC_GPIO_GetPortName", "dd/de9/oc__gpio_8h.html#afe1519b8243c9d39a0fec6a84a8e7735", null ],
    [ "oC_GPIO_GetPortOfPins", "dd/de9/oc__gpio_8h.html#a46df7797791b38d0c5858b3b0c11eba9", null ],
    [ "oC_GPIO_IsPinDefined", "dd/de9/oc__gpio_8h.html#a98fff79d7c48894e1dc6f9611a6d47e2", null ],
    [ "oC_GPIO_IsPinsState", "dd/de9/oc__gpio_8h.html#abd1c1fe53a9f6482fa5b12fd47d1c6cf", null ],
    [ "oC_GPIO_IsPortCorrect", "dd/de9/oc__gpio_8h.html#aff2a62a5389cc03158194886235f6472", null ],
    [ "oC_GPIO_IsPortIndexCorrect", "dd/de9/oc__gpio_8h.html#a07154f723c3c8b738174f108509f8e4a", null ],
    [ "oC_GPIO_IsSinglePin", "dd/de9/oc__gpio_8h.html#a1d16764a3d68b6f5d7ec90059ea0c38c", null ],
    [ "oC_GPIO_IsTurnedOn", "dd/de9/oc__gpio_8h.html#a3a3e969518f5fdabce636b98057549b4", null ],
    [ "oC_GPIO_PortIndexToPort", "dd/de9/oc__gpio_8h.html#ac9481b3b3045d66b0752195f38e67623", null ],
    [ "oC_GPIO_PortToPortIndex", "dd/de9/oc__gpio_8h.html#ad911acb29c77db85816b24c05d1d16ef", null ],
    [ "oC_GPIO_ReadCurrent", "dd/de9/oc__gpio_8h.html#a0b02c7ab144b13bdfba9c06d759cc7f1", null ],
    [ "oC_GPIO_ReadData", "dd/de9/oc__gpio_8h.html#a2ca5754cb8f8920c2336166ec5e85ef6", null ],
    [ "oC_GPIO_ReadDataReference", "dd/de9/oc__gpio_8h.html#a487a3f119ff593063be169b80ebed7b4", null ],
    [ "oC_GPIO_ReadInterruptTrigger", "dd/de9/oc__gpio_8h.html#aad98a79dd0da8b861fe999a7950e59b7", null ],
    [ "oC_GPIO_ReadMode", "dd/de9/oc__gpio_8h.html#a772c19ac8c36e6a2e996d790083a01cc", null ],
    [ "oC_GPIO_ReadOutputCircuit", "dd/de9/oc__gpio_8h.html#a2e006f2b856974cbaecdd036e4a1a7e8", null ],
    [ "oC_GPIO_ReadPower", "dd/de9/oc__gpio_8h.html#a027dc892e5605db4dc7824bcce65e395", null ],
    [ "oC_GPIO_ReadPull", "dd/de9/oc__gpio_8h.html#a24e37d86119ed63948887eb31dd36e2d", null ],
    [ "oC_GPIO_ReadSpeed", "dd/de9/oc__gpio_8h.html#a93fcaa346e9f3f3d14db5090fb9ca74d", null ],
    [ "oC_GPIO_SetCurrent", "dd/de9/oc__gpio_8h.html#aa882bfb740cd97142cef0f13e8f1ade6", null ],
    [ "oC_GPIO_SetInterruptTrigger", "dd/de9/oc__gpio_8h.html#ab3356f544f3aea941d3e9ec9105fd64b", null ],
    [ "oC_GPIO_SetMode", "dd/de9/oc__gpio_8h.html#a38153c867246fa7ad19b734a25f8a985", null ],
    [ "oC_GPIO_SetOutputCircuit", "dd/de9/oc__gpio_8h.html#aadeab405963350656caff24d00abfa40", null ],
    [ "oC_GPIO_SetPinsState", "dd/de9/oc__gpio_8h.html#a5fcd854862debca1cffdaa0ee4b53a9e", null ],
    [ "oC_GPIO_SetPower", "dd/de9/oc__gpio_8h.html#a1e696f1bf30d82cbd734fcb7f962bbff", null ],
    [ "oC_GPIO_SetPull", "dd/de9/oc__gpio_8h.html#a6b284869a2a3e3e80b70a5d89c156c3f", null ],
    [ "oC_GPIO_SetSpeed", "dd/de9/oc__gpio_8h.html#a777d1ecfccfdea293f512e7b175ff988", null ],
    [ "oC_GPIO_TogglePinsState", "dd/de9/oc__gpio_8h.html#aaec0747e4d5e5154a985e3145161b95a", null ],
    [ "oC_GPIO_TurnOff", "dd/de9/oc__gpio_8h.html#aeb63e6b2efb2594903bb33f8a569a9fb", null ],
    [ "oC_GPIO_TurnOn", "dd/de9/oc__gpio_8h.html#ad5900b4a60b487879e0d85ec27cbadd6", null ],
    [ "oC_GPIO_Unconfigure", "dd/de9/oc__gpio_8h.html#aa1e992d8099d4647440a31a934367030", null ],
    [ "oC_GPIO_WaitForPins", "dd/de9/oc__gpio_8h.html#a42e9b267a7a59f40891e37cde5989b5e", null ],
    [ "oC_GPIO_WaitForPinsState", "dd/de9/oc__gpio_8h.html#a9f9c3103ca3ec2bf3a9416fc27a2a902", null ],
    [ "oC_GPIO_WriteData", "dd/de9/oc__gpio_8h.html#a177e90f92b8267c93bd15d92c359e616", null ]
];