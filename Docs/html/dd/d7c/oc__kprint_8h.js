var oc__kprint_8h =
[
    [ "oC_KPrint_EOF", "dd/d7c/oc__kprint_8h.html#a5bb618206b7130114db15e64c8109c5c", null ],
    [ "oC_KPrint_GetChar_t", "dc/da9/group___k_print.html#gadc18d5392bf5eab629d651834a82bd49", null ],
    [ "oC_KPrint_DriverPrintf", "dc/da9/group___k_print.html#gaa92e0e06e3f27707a155878670f0b89f", null ],
    [ "oC_KPrint_Format", "dc/da9/group___k_print.html#gaf79674d3775574cc3fe36d9c1c656314", null ],
    [ "oC_KPrint_FormatScanf", "dc/da9/group___k_print.html#gaedd28ab9d70ff7c2e0a54ac260d680b7", null ],
    [ "oC_KPrint_FormatScanfFromStream", "dc/da9/group___k_print.html#ga61042af150d2dab6641d699fc3f33d3d", null ],
    [ "oC_KPrint_GetCharFromString", "dc/da9/group___k_print.html#gae347b40127f87d1f6275a25fd4147afd", null ],
    [ "oC_KPrint_Printf", "dc/da9/group___k_print.html#ga48a4bcedb874a30f2328e9a0015580f0", null ],
    [ "oC_KPrint_ReadFromStdIn", "dc/da9/group___k_print.html#gad4c0f19d800aaf8a7695d6102d8e7fbf", null ],
    [ "oC_KPrint_Scanf", "dc/da9/group___k_print.html#gaff08470e128bad40de2e070bf8a103b9", null ],
    [ "oC_KPrint_TurnOff", "dc/da9/group___k_print.html#gad6c45d2d5f6544129dbd075190c7e4cf", null ],
    [ "oC_KPrint_TurnOn", "dc/da9/group___k_print.html#gaec9e14a0fad929b8935485233415fd3a", null ],
    [ "oC_KPrint_VPrintf", "dc/da9/group___k_print.html#ga930da67c45c10f996e9ef2120111997a", null ],
    [ "oC_KPrint_VScanf", "dc/da9/group___k_print.html#gadcd305c048b79198ae956ae7e6a69968", null ],
    [ "oC_KPrint_WriteToStdOut", "dc/da9/group___k_print.html#ga789b357579d822e9f2713122d22c56fc", null ]
];