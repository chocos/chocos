var structo_c___u_a_r_t___config__t =
[
    [ "BitOrder", "dd/d9d/structo_c___u_a_r_t___config__t.html#ae60dfa683070358a7411e96aa7ab6b37", null ],
    [ "BitRate", "dd/d9d/structo_c___u_a_r_t___config__t.html#aa181919652b0e07590666c2bff7f3181", null ],
    [ "Dma", "dd/d9d/structo_c___u_a_r_t___config__t.html#aa9855a57846ed92f09d63cc05144b6a6", null ],
    [ "Invert", "dd/d9d/structo_c___u_a_r_t___config__t.html#a974e1db445a4ddd2ef41bf3f1a03fe59", null ],
    [ "Loopback", "dd/d9d/structo_c___u_a_r_t___config__t.html#ad49df8a10bb500c80b0d2ec82fb704c0", null ],
    [ "Parity", "dd/d9d/structo_c___u_a_r_t___config__t.html#a65348f1a9b8cfb8c3eb861639c06c3f4", null ],
    [ "Rx", "dd/d9d/structo_c___u_a_r_t___config__t.html#a739681f4bc1920cd7186675c573aef93", null ],
    [ "StopBit", "dd/d9d/structo_c___u_a_r_t___config__t.html#ac93e7ff7fa68f7b1177d16a57cd18d40", null ],
    [ "Tx", "dd/d9d/structo_c___u_a_r_t___config__t.html#a9ca16ba5c1a4ea9c2ef8a5f9dcb7a84e", null ],
    [ "WordLength", "dd/d9d/structo_c___u_a_r_t___config__t.html#ac14e9951225349e3970e53f2d5234c7e", null ]
];