var ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h =
[
    [ "oC_REGISTER_MAP_", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#ad2139536a0d21165e7b4363d434c42f4", null ],
    [ "oC_REGISTER_MAP_ACOM", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a014142817a1f21bb75c0605539d7e91f", null ],
    [ "oC_REGISTER_MAP_ADC", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#aa5525eba1f1b96d62c03bfbc90b048b5", null ],
    [ "oC_REGISTER_MAP_CAN", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a5eb0a974a788fda296543ab3c05194d1", null ],
    [ "oC_REGISTER_MAP_DMA", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a55fdde236ed742e6cea174ac563a9c5c", null ],
    [ "oC_REGISTER_MAP_GPIO", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#abd73391b97f8f4696f388376657f13a8", null ],
    [ "oC_REGISTER_MAP_I2C", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a401074885073a8feb10aa82d6ffc073c", null ],
    [ "oC_REGISTER_MAP_LIST", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a30aea32e0e7f4fbb1ecadc61e3549153", null ],
    [ "oC_REGISTER_MAP_SPI", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a713ae80bbd42be2b8fc2af387196d8ad", null ],
    [ "oC_REGISTER_MAP_SYSTEM_CONTROL", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a89d02b68ebc92ab20f6bcdbaa933ddc9", null ],
    [ "oC_REGISTER_MAP_TIMER", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a49aa81b2e78cfc202fdb1c47a743d145", null ],
    [ "oC_REGISTER_MAP_UART", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#ac7a6b6bec2c6a2729dcb02c11bbb28b8", null ],
    [ "oC_REGISTER_MAP_USB", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#abacbd6f2a3fe75aa00bfd46b0c5299bc", null ],
    [ "oC_REGISTER_MAP_WDGTIMER", "dd/dab/ti_2lm4f_2lm4f120h5qr_2oc__rmaps__defs_8h.html#a0976893ec318889b592f67d88487d028", null ]
];