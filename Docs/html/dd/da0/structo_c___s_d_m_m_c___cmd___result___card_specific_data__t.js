var structo_c___s_d_m_m_c___cmd___result___card_specific_data__t =
[
    [ "BlockLength", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a27fc08359447cc2155132fd7a132d093", null ],
    [ "CardSize", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a80e821a3fbbc015472c56f2e99b3e50d", null ],
    [ "DsrImplemented", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a3efedfd08b62491eae3565a4a6dd0f2a", null ],
    [ "FileFormat", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a64f0620bd61e6c383c8d535021e397b0", null ],
    [ "PartialBlockReadAllowed", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a8da02563f6c94de9f82979a6fd9c8b7a", null ],
    [ "PartialBlockWriteAllowed", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#ae2e0ec319e535276894e420fc8877ffa", null ],
    [ "PermanentWriteProtection", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#ad66e01af757ebb357935beb944803f89", null ],
    [ "ReadBlockMisalign", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#af28f155373dc51109c2170548fd860a1", null ],
    [ "SectorSize", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a8fd1d60aa3e4d98c596e7476ab66a752", null ],
    [ "SupportedClasses", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#af5d421e957a3e2410a2419bfd60a7630", null ],
    [ "TemporaryWriteProtection", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a983429ead71903a58745951d5c773d65", null ],
    [ "TransferSpeed", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#ae6537efbfaa80869e2121e325a4ad7ec", null ],
    [ "WriteBlockMisalign", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html#a49394096ed5afd07d331574b9a01c230", null ]
];