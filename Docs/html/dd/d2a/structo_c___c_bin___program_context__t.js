var structo_c___c_bin___program_context__t =
[
    [ "Argc", "dd/d2a/structo_c___c_bin___program_context__t.html#a34296df812d7c5468ae201c7eeca5877", null ],
    [ "Argv", "dd/d2a/structo_c___c_bin___program_context__t.html#ab4fad31452ea2612bd37240026dd6e44", null ],
    [ "MainThread", "dd/d2a/structo_c___c_bin___program_context__t.html#a514e1e1805db22c4aa9a52742e2e3a64", null ],
    [ "Priority", "dd/d2a/structo_c___c_bin___program_context__t.html#a61429c7d77714ba749deb4010aad65d8", null ],
    [ "Process", "dd/d2a/structo_c___c_bin___program_context__t.html#a862c271da330044e3b398bbf95488ce2", null ],
    [ "Program", "dd/d2a/structo_c___c_bin___program_context__t.html#a17b1473c205a916becf53dac82703508", null ],
    [ "ProgramContext", "dd/d2a/structo_c___c_bin___program_context__t.html#af1c62aa361897123c363caa7f489a900", null ],
    [ "Sections", "dd/d2a/structo_c___c_bin___program_context__t.html#a90336245ba62ff67ccd3549cb431566c", null ],
    [ "SectionsBuffer", "dd/d2a/structo_c___c_bin___program_context__t.html#a64a1a42e8bcc2643e611aa067ca39261", null ],
    [ "StreamErr", "dd/d2a/structo_c___c_bin___program_context__t.html#a5f0a1d62d0b78fe33c50a1fe73fcae4d", null ],
    [ "StreamIn", "dd/d2a/structo_c___c_bin___program_context__t.html#a09e9476428fc7f008513b719be97f1f0", null ],
    [ "StreamOut", "dd/d2a/structo_c___c_bin___program_context__t.html#adcd72360f7d9fdb63f47411c3ce7828d", null ]
];