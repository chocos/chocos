var annotated_dup =
[
    [ "AccessPermissions_t", "df/d6f/struct_access_permissions__t.html", null ],
    [ "ActivitiesWaiter_t", "d8/d34/struct_activities_waiter__t.html", null ],
    [ "AddressResolutionEntry_t", "df/d23/struct_address_resolution_entry__t.html", null ],
    [ "BankData_t", "d1/d37/struct_bank_data__t.html", "d1/d37/struct_bank_data__t" ],
    [ "Cell_t", "d3/da0/struct_cell__t.html", "d3/da0/struct_cell__t" ],
    [ "ClockConfiguration_t", "d4/d53/struct_clock_configuration__t.html", null ],
    [ "ClockRange_t", "da/d12/struct_clock_range__t.html", null ],
    [ "Command_t", "d5/d16/struct_command__t.html", "d5/d16/struct_command__t" ],
    [ "CommandDefinition_t", "dd/dcb/struct_command_definition__t.html", "dd/dcb/struct_command_definition__t" ],
    [ "CommandName_t", "d3/dff/struct_command_name__t.html", "d3/dff/struct_command_name__t" ],
    [ "ConnectionContext_t", "dd/dae/struct_connection_context__t.html", null ],
    [ "ConnectionSlot_t", "d8/db7/struct_connection_slot__t.html", "d8/db7/struct_connection_slot__t" ],
    [ "Context_t", "d7/d83/struct_context__t.html", "d7/d83/struct_context__t" ],
    [ "ControlTableEntry_t", "d7/d9f/struct_control_table_entry__t.html", null ],
    [ "Descriptor_t", "d5/da8/struct_descriptor__t.html", null ],
    [ "Dir_t", "d0/d64/struct_dir__t.html", null ],
    [ "Disk_t", "dd/d42/struct_disk__t.html", null ],
    [ "DiskContext_t", "d2/dff/struct_disk_context__t.html", "d2/dff/struct_disk_context__t" ],
    [ "Divisor_t", "dd/d85/struct_divisor__t.html", null ],
    [ "DMACHCTL_t", "d9/d15/union_d_m_a_c_h_c_t_l__t.html", null ],
    [ "DmaStatus_t", "de/d0d/struct_dma_status__t.html", null ],
    [ "DriverInstanceContext_t", "d1/d21/struct_driver_instance_context__t.html", "d1/d21/struct_driver_instance_context__t" ],
    [ "Event_t", "d8/d34/struct_event__t.html", null ],
    [ "EventLog_t", "dc/d47/struct_event_log__t.html", null ],
    [ "EventSamples_t", "de/d40/struct_event_samples__t.html", null ],
    [ "File_t", "d8/d93/struct_file__t.html", "d8/d93/struct_file__t" ],
    [ "FreeArea_t", "df/d53/struct_free_area__t.html", null ],
    [ "GestDefinition_t", "de/d48/struct_gest_definition__t.html", null ],
    [ "GraphStyle_t", "d7/daa/struct_graph_style__t.html", null ],
    [ "ICtrl_t", "dc/d65/struct_i_ctrl__t.html", null ],
    [ "LineFrequencies_t", "dd/d5a/struct_line_frequencies__t.html", null ],
    [ "List_ElementHandle_t", "d9/d43/struct_list___element_handle__t.html", null ],
    [ "List_t", "d2/d22/struct_list__t.html", null ],
    [ "ModeContext_t", "d6/dbb/struct_mode_context__t.html", "d6/dbb/struct_mode_context__t" ],
    [ "ModuleRegistration_t", "d1/d24/struct_module_registration__t.html", "d1/d24/struct_module_registration__t" ],
    [ "Move_t", "dd/dd2/struct_move__t.html", null ],
    [ "Mutex_t", "da/dda/struct_mutex__t.html", null ],
    [ "Netif_t", "d3/dcd/struct_netif__t.html", null ],
    [ "News_t", "d0/d1b/struct_news__t.html", null ],
    [ "oC_Allocator_t", "da/d59/structo_c___allocator__t.html", "da/d59/structo_c___allocator__t" ],
    [ "oC_AutoConfiguration_t", "dc/d3f/structo_c___auto_configuration__t.html", null ],
    [ "oC_CBin_File_t", "df/d22/structo_c___c_bin___file__t.html", "df/d22/structo_c___c_bin___file__t" ],
    [ "oC_CBin_ProgramContext_t", "dd/d2a/structo_c___c_bin___program_context__t.html", "dd/d2a/structo_c___c_bin___program_context__t" ],
    [ "oC_ChannelData_t", "d7/dea/structo_c___channel_data__t.html", "d7/dea/structo_c___channel_data__t" ],
    [ "oC_ColorMap_t", "d1/da4/structo_c___color_map__t.html", "d1/da4/structo_c___color_map__t" ],
    [ "oC_DataPackage_t", "d8/de7/structo_c___data_package__t.html", "d8/de7/structo_c___data_package__t" ],
    [ "oC_Diag_SupportedDiagData_t", "d7/d94/structo_c___diag___supported_diag_data__t.html", null ],
    [ "oC_Diag_t", "d9/dc1/structo_c___diag__t.html", "d9/dc1/structo_c___diag__t" ],
    [ "oC_Disk_MasterBootRecord_t", "d8/de3/uniono_c___disk___master_boot_record__t.html", null ],
    [ "oC_DriverInstance_t", "db/d35/structo_c___driver_instance__t.html", null ],
    [ "oC_DynamicConfig_VariableDetails_t", "d8/d5f/structo_c___dynamic_config___variable_details__t.html", "d8/d5f/structo_c___dynamic_config___variable_details__t" ],
    [ "oC_Elf_FileHeader_t", "d0/d86/structo_c___elf___file_header__t.html", "d0/d86/structo_c___elf___file_header__t" ],
    [ "oC_Elf_ProgramHeader_t", "db/df9/structo_c___elf___program_header__t.html", "db/df9/structo_c___elf___program_header__t" ],
    [ "oC_Elf_RelocationEntry_t", "d9/dc0/structo_c___elf___relocation_entry__t.html", "d9/dc0/structo_c___elf___relocation_entry__t" ],
    [ "oC_Elf_SectionHeader_t", "d3/d3b/structo_c___elf___section_header__t.html", "d3/d3b/structo_c___elf___section_header__t" ],
    [ "oC_Elf_SymbolTableEntry_t", "df/d33/structo_c___elf___symbol_table_entry__t.html", "df/d33/structo_c___elf___symbol_table_entry__t" ],
    [ "oC_ETH_Config_t", "de/d51/structo_c___e_t_h___config__t.html", "de/d51/structo_c___e_t_h___config__t" ],
    [ "oC_ETH_PhyChipInfo_t", "d7/d2c/structo_c___e_t_h___phy_chip_info__t.html", "d7/d2c/structo_c___e_t_h___phy_chip_info__t" ],
    [ "oC_ETH_PhyRegister_BCR_t", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t" ],
    [ "oC_ETH_PhyRegister_BSR_t", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t" ],
    [ "oC_FileInfo_t", "d3/d09/structo_c___file_info__t.html", null ],
    [ "oC_FileSystem_Registration_t", "d6/df6/structo_c___file_system___registration__t.html", null ],
    [ "oC_FlashFs_FileDefinition_t", "d0/d91/structo_c___flash_fs___file_definition__t.html", null ],
    [ "oC_FMC_ChipInfo_t", "d5/d8d/structo_c___f_m_c___chip_info__t.html", "d5/d8d/structo_c___f_m_c___chip_info__t" ],
    [ "oC_FMC_Config_t", "d1/d44/structo_c___f_m_c___config__t.html", "d1/d44/structo_c___f_m_c___config__t" ],
    [ "oC_Font_CharacterInfo_t", "d8/d57/structo_c___font___character_info__t.html", "d8/d57/structo_c___font___character_info__t" ],
    [ "oC_Font_CharacterMap_t", "d3/d38/structo_c___font___character_map__t.html", null ],
    [ "oC_FontInfo_t", "d3/d69/structo_c___font_info__t.html", "d3/d69/structo_c___font_info__t" ],
    [ "oC_FT5336_Config_t", "d8/d73/structo_c___f_t5336___config__t.html", "d8/d73/structo_c___f_t5336___config__t" ],
    [ "oC_GPIO_Config_t", "db/d2b/structo_c___g_p_i_o___config__t.html", "db/d2b/structo_c___g_p_i_o___config__t" ],
    [ "oC_GTD_ColorDefinition_t", "db/dfd/structo_c___g_t_d___color_definition__t.html", "db/dfd/structo_c___g_t_d___color_definition__t" ],
    [ "oC_GTD_Config_t", "df/db0/structo_c___g_t_d___config__t.html", "df/db0/structo_c___g_t_d___config__t" ],
    [ "oC_Http_Header_t", "de/d9c/structo_c___http___header__t.html", "de/d9c/structo_c___http___header__t" ],
    [ "oC_Http_Packet_t", "d3/d56/structo_c___http___packet__t.html", "d3/d56/structo_c___http___packet__t" ],
    [ "oC_Http_Request_t", "d8/d87/structo_c___http___request__t.html", "d8/d87/structo_c___http___request__t" ],
    [ "oC_Icmp_Datagram_t", "d0/def/structo_c___icmp___datagram__t.html", "d0/def/structo_c___icmp___datagram__t" ],
    [ "oC_Icmp_Header_t", "d1/d1a/structo_c___icmp___header__t.html", "d1/d1a/structo_c___icmp___header__t" ],
    [ "oC_Icmp_Message_AddressMask_t", "db/d7b/structo_c___icmp___message___address_mask__t.html", "db/d7b/structo_c___icmp___message___address_mask__t" ],
    [ "oC_Icmp_Message_DestinationUnreachable_t", "d9/d6a/structo_c___icmp___message___destination_unreachable__t.html", null ],
    [ "oC_Icmp_Message_Echo_t", "d7/d70/structo_c___icmp___message___echo__t.html", "d7/d70/structo_c___icmp___message___echo__t" ],
    [ "oC_Icmp_Message_Timestamp_t", "d2/dd4/structo_c___icmp___message___timestamp__t.html", "d2/dd4/structo_c___icmp___message___timestamp__t" ],
    [ "oC_Icmp_Packet_t", "d5/df1/uniono_c___icmp___packet__t.html", "d5/df1/uniono_c___icmp___packet__t" ],
    [ "oC_ICtrlMan_Activity_t", "d9/dc7/structo_c___i_ctrl_man___activity__t.html", "d9/dc7/structo_c___i_ctrl_man___activity__t" ],
    [ "oC_IDI_Event_t", "dc/d64/structo_c___i_d_i___event__t.html", null ],
    [ "oC_InterruptData_t", "d7/d1e/structo_c___interrupt_data__t.html", null ],
    [ "oC_LCDTFT_Config_t", "db/de4/structo_c___l_c_d_t_f_t___config__t.html", null ],
    [ "oC_LCDTFT_LLD_Pins_t", "d7/d56/uniono_c___l_c_d_t_f_t___l_l_d___pins__t.html", null ],
    [ "oC_LCDTFT_LLD_SyncParameters_t", "d2/d96/structo_c___l_c_d_t_f_t___l_l_d___sync_parameters__t.html", null ],
    [ "oC_LCDTFT_LLD_TimingParameters_t", "d1/dd4/structo_c___l_c_d_t_f_t___l_l_d___timing_parameters__t.html", null ],
    [ "oC_LED_Config_t", "d3/dee/structo_c___l_e_d___config__t.html", "d3/dee/structo_c___l_e_d___config__t" ],
    [ "oC_MCS_MemoryRegionConfig_t", "dc/d4f/structo_c___m_c_s___memory_region_config__t.html", "dc/d4f/structo_c___m_c_s___memory_region_config__t" ],
    [ "oC_MEM_LLD_MemoryRegionConfig_t", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t.html", "dc/d8f/structo_c___m_e_m___l_l_d___memory_region_config__t" ],
    [ "oC_MemMan_AllocationStats_t", "d2/d5d/structo_c___mem_man___allocation_stats__t.html", null ],
    [ "oC_MemMan_AllocatorsStats_t", "d0/d96/structo_c___mem_man___allocators_stats__t.html", null ],
    [ "oC_Module_Registration_t", "d3/d0a/structo_c___module___registration__t.html", null ],
    [ "oC_Net_Address_t", "d2/d5b/structo_c___net___address__t.html", "d2/d5b/structo_c___net___address__t" ],
    [ "oC_Net_Frame_t", "de/d2b/structo_c___net___frame__t.html", "de/d2b/structo_c___net___frame__t" ],
    [ "oC_Net_HardwareAddress_t", "db/d6a/structo_c___net___hardware_address__t.html", "db/d6a/structo_c___net___hardware_address__t" ],
    [ "oC_Net_Info_t", "df/d97/structo_c___net___info__t.html", "df/d97/structo_c___net___info__t" ],
    [ "oC_Net_Ipv4Info_t", "da/d29/structo_c___net___ipv4_info__t.html", "da/d29/structo_c___net___ipv4_info__t" ],
    [ "oC_Net_Ipv6Info_t", "d1/d19/structo_c___net___ipv6_info__t.html", "d1/d19/structo_c___net___ipv6_info__t" ],
    [ "oC_Net_Packet_t", "d7/d58/uniono_c___net___packet__t.html", "d7/d58/uniono_c___net___packet__t" ],
    [ "oC_Net_QueueStatus_t", "dc/ddf/structo_c___net___queue_status__t.html", "dc/ddf/structo_c___net___queue_status__t" ],
    [ "oC_NetifMan_RoutingTableEntry_t", "da/dbc/structo_c___netif_man___routing_table_entry__t.html", "da/dbc/structo_c___netif_man___routing_table_entry__t" ],
    [ "oC_NEUNET_Config_t", "d2/da8/structo_c___n_e_u_n_e_t___config__t.html", null ],
    [ "oC_Partition_Entry_t", "dc/d3e/uniono_c___partition___entry__t.html", null ],
    [ "oC_Partition_Location_t", "dc/d84/structo_c___partition___location__t.html", null ],
    [ "oC_Pixel_Position_t", "d9/d80/structo_c___pixel___position__t.html", "d9/d80/structo_c___pixel___position__t" ],
    [ "oC_Pixel_t", "da/d9f/structo_c___pixel__t.html", "da/d9f/structo_c___pixel__t" ],
    [ "oC_PortMan_Config_t", "d3/d2c/structo_c___port_man___config__t.html", "d3/d2c/structo_c___port_man___config__t" ],
    [ "oC_Program_Header_t", "db/dab/structo_c___program___header__t.html", null ],
    [ "oC_Program_Registration_t", "d0/ded/structo_c___program___registration__t.html", null ],
    [ "oC_Program_Sections_t", "d8/d59/structo_c___program___sections__t.html", null ],
    [ "oC_PWM_Config_t", "d3/df6/structo_c___p_w_m___config__t.html", null ],
    [ "oC_SDMMC_CardId_t", "d4/d66/structo_c___s_d_m_m_c___card_id__t.html", "d4/d66/structo_c___s_d_m_m_c___card_id__t" ],
    [ "oC_SDMMC_CardInfo_t", "d1/dfb/structo_c___s_d_m_m_c___card_info__t.html", "d1/dfb/structo_c___s_d_m_m_c___card_info__t" ],
    [ "oC_SDMMC_Cmd_CommandData_t", "d6/d84/structo_c___s_d_m_m_c___cmd___command_data__t.html", "d6/d84/structo_c___s_d_m_m_c___cmd___command_data__t" ],
    [ "oC_SDMMC_Cmd_Request_SdSendOpCond_t", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t" ],
    [ "oC_SDMMC_Cmd_Result_CardSpecificData_t", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t.html", "dd/da0/structo_c___s_d_m_m_c___cmd___result___card_specific_data__t" ],
    [ "oC_SDMMC_Cmd_Result_SdSendOpCond_t", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t.html", "d4/d74/structo_c___s_d_m_m_c___cmd___result___sd_send_op_cond__t" ],
    [ "oC_SDMMC_CommIf_Handlers_t", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t" ],
    [ "oC_SDMMC_Config_t", "db/dd0/structo_c___s_d_m_m_c___config__t.html", "db/dd0/structo_c___s_d_m_m_c___config__t" ],
    [ "oC_SDMMC_Context_t", "d1/d2b/structo_c___s_d_m_m_c___context__t.html", "d1/d2b/structo_c___s_d_m_m_c___context__t" ],
    [ "oC_SDMMC_LLD_Command_t", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t" ],
    [ "oC_SDMMC_LLD_CommandResponse_t", "dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t.html", "dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t" ],
    [ "oC_SDMMC_LLD_Config_t", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t.html", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t" ],
    [ "oC_SDMMC_LLD_TransferConfig_t", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t" ],
    [ "oC_SDMMC_Mode_Interface_t", "d6/d5b/structo_c___s_d_m_m_c___mode___interface__t.html", "d6/d5b/structo_c___s_d_m_m_c___mode___interface__t" ],
    [ "oC_SDMMC_Mode_RawCommand_t", "d6/dee/structo_c___s_d_m_m_c___mode___raw_command__t.html", null ],
    [ "oC_SDMMC_Response_t", "da/db7/structo_c___s_d_m_m_c___response__t.html", "da/db7/structo_c___s_d_m_m_c___response__t" ],
    [ "oC_SDMMC_Responses_CardSpecificData_t", "df/da6/uniono_c___s_d_m_m_c___responses___card_specific_data__t.html", "df/da6/uniono_c___s_d_m_m_c___responses___card_specific_data__t" ],
    [ "oC_SDMMC_Responses_TransferSpeed_t", "d8/dc1/uniono_c___s_d_m_m_c___responses___transfer_speed__t.html", "d8/dc1/uniono_c___s_d_m_m_c___responses___transfer_speed__t" ],
    [ "oC_Service_Registration_t", "d2/dbc/structo_c___service___registration__t.html", null ],
    [ "oC_StackData_t", "d6/d81/structo_c___stack_data__t.html", "d6/d81/structo_c___stack_data__t" ],
    [ "oC_Storage_DiskInterface_t", "d8/d7f/structo_c___storage___disk_interface__t.html", null ],
    [ "oC_Stream_Cfg_t", "d8/d4a/structo_c___stream___cfg__t.html", null ],
    [ "oC_SystemCall_t", "d3/dd2/structo_c___system_call__t.html", null ],
    [ "oC_Tcp_Connection_Callback_t", "d3/d4f/structo_c___tcp___connection___callback__t.html", "d3/d4f/structo_c___tcp___connection___callback__t" ],
    [ "oC_Tcp_Connection_Config_t", "df/da2/structo_c___tcp___connection___config__t.html", "df/da2/structo_c___tcp___connection___config__t" ],
    [ "oC_Tcp_Header_t", "d6/d1a/structo_c___tcp___header__t.html", "d6/d1a/structo_c___tcp___header__t" ],
    [ "oC_Tcp_Packet_t", "d9/d2d/uniono_c___tcp___packet__t.html", "d9/d2d/uniono_c___tcp___packet__t" ],
    [ "oC_Terminal_CursorPos_t", "d6/d47/structo_c___terminal___cursor_pos__t.html", "d6/d47/structo_c___terminal___cursor_pos__t" ],
    [ "oC_Terminal_IfCfg_t", "dc/d54/structo_c___terminal___if_cfg__t.html", "dc/d54/structo_c___terminal___if_cfg__t" ],
    [ "oC_TGUI_ActiveObject_t", "df/d21/structo_c___t_g_u_i___active_object__t.html", null ],
    [ "oC_TGUI_BoxStyle_t", "d5/d44/structo_c___t_g_u_i___box_style__t.html", "d5/d44/structo_c___t_g_u_i___box_style__t" ],
    [ "oC_TGUI_ColorData_t", "dc/df5/structo_c___t_g_u_i___color_data__t.html", null ],
    [ "oC_TGUI_DrawListMenuConfig_t", "d6/d70/structo_c___t_g_u_i___draw_list_menu_config__t.html", null ],
    [ "oC_TGUI_DrawPropertyConfig_t", "d6/dff/structo_c___t_g_u_i___draw_property_config__t.html", null ],
    [ "oC_TGUI_EditBox_t", "d7/de7/structo_c___t_g_u_i___edit_box__t.html", null ],
    [ "oC_TGUI_EditBoxStyle_t", "d0/d8b/structo_c___t_g_u_i___edit_box_style__t.html", null ],
    [ "oC_TGUI_MenuEntry_t", "d0/da0/structo_c___t_g_u_i___menu_entry__t.html", "d0/da0/structo_c___t_g_u_i___menu_entry__t" ],
    [ "oC_TGUI_MenuStyle_t", "df/dcf/structo_c___t_g_u_i___menu_style__t.html", "df/dcf/structo_c___t_g_u_i___menu_style__t" ],
    [ "oC_TGUI_Position_t", "d6/d48/structo_c___t_g_u_i___position__t.html", "d6/d48/structo_c___t_g_u_i___position__t" ],
    [ "oC_TGUI_ProgressBarStyle_t", "d4/dfb/structo_c___t_g_u_i___progress_bar_style__t.html", "d4/dfb/structo_c___t_g_u_i___progress_bar_style__t" ],
    [ "oC_TGUI_Property_t", "df/d3e/structo_c___t_g_u_i___property__t.html", null ],
    [ "oC_TGUI_PropertyStyle_t", "db/db1/structo_c___t_g_u_i___property_style__t.html", null ],
    [ "oC_TGUI_PushButton_t", "d9/d76/structo_c___t_g_u_i___push_button__t.html", null ],
    [ "oC_TGUI_PushButtonStyle_t", "db/d8f/structo_c___t_g_u_i___push_button_style__t.html", null ],
    [ "oC_TGUI_QuickEditBox_t", "d2/db2/structo_c___t_g_u_i___quick_edit_box__t.html", null ],
    [ "oC_TGUI_QuickEditBoxStyle_t", "d5/d1b/structo_c___t_g_u_i___quick_edit_box_style__t.html", null ],
    [ "oC_TGUI_SelectionBox_t", "d0/d1d/structo_c___t_g_u_i___selection_box__t.html", null ],
    [ "oC_TGUI_SelectionBoxStyle_t", "da/d43/structo_c___t_g_u_i___selection_box_style__t.html", null ],
    [ "oC_TGUI_Spiner_t", "d7/db9/structo_c___t_g_u_i___spiner__t.html", null ],
    [ "oC_TGUI_Style_t", "dd/d2a/structo_c___t_g_u_i___style__t.html", "dd/d2a/structo_c___t_g_u_i___style__t" ],
    [ "oC_TGUI_TextBoxStyle_t", "dc/d78/structo_c___t_g_u_i___text_box_style__t.html", "dc/d78/structo_c___t_g_u_i___text_box_style__t" ],
    [ "oC_Time_Divided_t", "d2/df5/structo_c___time___divided__t.html", null ],
    [ "oC_TIMER_Config_t", "db/d32/structo_c___t_i_m_e_r___config__t.html", null ],
    [ "oC_UART_Config_t", "dd/d9d/structo_c___u_a_r_t___config__t.html", "dd/d9d/structo_c___u_a_r_t___config__t" ],
    [ "oC_Udp_Packet_t", "d1/d40/uniono_c___udp___packet__t.html", "d1/d40/uniono_c___udp___packet__t" ],
    [ "oC_WidgetScreen_DrawStyle_t", "d7/d6a/structo_c___widget_screen___draw_style__t.html", "d7/d6a/structo_c___widget_screen___draw_style__t" ],
    [ "oC_WidgetScreen_Option_t", "df/d10/structo_c___widget_screen___option__t.html", "df/d10/structo_c___widget_screen___option__t" ],
    [ "oC_WidgetScreen_Palette_t", "d5/db2/structo_c___widget_screen___palette__t.html", "d5/db2/structo_c___widget_screen___palette__t" ],
    [ "oC_WidgetScreen_ScreenDefinition_t", "df/d04/structo_c___widget_screen___screen_definition__t.html", "df/d04/structo_c___widget_screen___screen_definition__t" ],
    [ "oC_WidgetScreen_WidgetDefinition_t", "db/d64/structo_c___widget_screen___widget_definition__t.html", "db/d64/structo_c___widget_screen___widget_definition__t" ],
    [ "Option_t", "dc/d71/struct_option__t.html", null ],
    [ "PACKED", "df/d6b/struct_p_a_c_k_e_d.html", "df/d6b/struct_p_a_c_k_e_d" ],
    [ "Packet_t", "d1/d64/struct_packet__t.html", null ],
    [ "PacketFilterData_t", "d3/dd7/struct_packet_filter_data__t.html", null ],
    [ "PacketFlags_t", "d3/dcb/struct_packet_flags__t.html", null ],
    [ "Partition_t", "d5/de3/struct_partition__t.html", null ],
    [ "PllConfig_t", "d3/dfc/struct_pll_config__t.html", null ],
    [ "PortReservation_t", "d2/dd7/struct_port_reservation__t.html", "d2/dd7/struct_port_reservation__t" ],
    [ "Process_t", "d2/d84/struct_process__t.html", null ],
    [ "ProgramContext_t", "db/df5/struct_program_context__t.html", null ],
    [ "ProgressBar_t", "d3/d57/struct_progress_bar__t.html", null ],
    [ "Queue_t", "d0/dc6/struct_queue__t.html", null ],
    [ "Range_t", "d8/dc7/struct_range__t.html", null ],
    [ "ReservationData_t", "d4/da4/struct_reservation_data__t.html", null ],
    [ "ResponseData_t", "d7/dad/struct_response_data__t.html", "d7/dad/struct_response_data__t" ],
    [ "RevertAction_t", "d8/dbb/struct_revert_action__t.html", "d8/dbb/struct_revert_action__t" ],
    [ "RoutingTableData_t", "d2/df2/struct_routing_table_data__t.html", null ],
    [ "Sample_t", "d0/dc9/struct_sample__t.html", null ],
    [ "SavedError_t", "dc/d84/struct_saved_error__t.html", null ],
    [ "SavedPacket_t", "d4/d57/struct_saved_packet__t.html", null ],
    [ "Screen_t", "d7/d2f/struct_screen__t.html", "d7/d2f/struct_screen__t" ],
    [ "SDRAMCommand_t", "de/d14/struct_s_d_r_a_m_command__t.html", "de/d14/struct_s_d_r_a_m_command__t" ],
    [ "SDRAMConfigVariables_t", "d9/dd2/struct_s_d_r_a_m_config_variables__t.html", null ],
    [ "Segment_t", "dd/dbc/struct_segment__t.html", null ],
    [ "Semaphore_t", "d7/db3/struct_semaphore__t.html", null ],
    [ "Server_t", "db/d95/struct_server__t.html", "db/d95/struct_server__t" ],
    [ "Service_t", "dd/db8/struct_service__t.html", null ],
    [ "ServiceContext_t", "d2/d2d/struct_service_context__t.html", null ],
    [ "ServiceData_t", "dd/de8/struct_service_data__t.html", null ],
    [ "Signal_t", "d1/d11/struct_signal__t.html", null ],
    [ "SoftwareRing_t", "d9/d4d/struct_software_ring__t.html", null ],
    [ "Storage_t", "de/d18/struct_storage__t.html", null ],
    [ "Stream_t", "de/d8a/struct_stream__t.html", null ],
    [ "Tcp_Connection_t", "d8/d06/struct_tcp___connection__t.html", null ],
    [ "TelnetOptionData_t", "de/d53/union_telnet_option_data__t.html", null ],
    [ "TelnetOptionParser_t", "dc/da0/struct_telnet_option_parser__t.html", null ],
    [ "Thread_t", "d3/d09/struct_thread__t.html", "d3/d09/struct_thread__t" ],
    [ "User_t", "d9/d31/struct_user__t.html", null ],
    [ "VirtualDriverConfig_t", "d8/df2/struct_virtual_driver_config__t.html", null ],
    [ "Widget_t", "d1/d8e/struct_widget__t.html", null ],
    [ "WidgetHandlersDefinition_t", "d2/d6b/struct_widget_handlers_definition__t.html", null ],
    [ "WidgetScreen_t", "dd/d1c/struct_widget_screen__t.html", null ],
    [ "Window_t", "dc/d92/struct_window__t.html", "dc/d92/struct_window__t" ],
    [ "WindowSize_t", "d2/d2d/struct_window_size__t.html", null ]
];