var oc__system__cfg_8h =
[
    [ "CFG_BYTES_DEFAULT_PROCESS_HEAP_MAP_SIZE", "de/d5f/oc__system__cfg_8h.html#a3a7b45347dcf742f5f81ba1882034a8e", null ],
    [ "CFG_BYTES_DEFAULT_RED_ZONE_SIZE", "de/d5f/oc__system__cfg_8h.html#a8426834492d161bd718e6ee9b1d4551c", null ],
    [ "CFG_BYTES_DEFAULT_THREAD_STACK_SIZE", "de/d5f/oc__system__cfg_8h.html#a106fb55a201006f06bd12c7c63d15088", null ],
    [ "CFG_FREQUENCY_DEFAULT_FORCE_CHANGE_THREAD", "de/d5f/oc__system__cfg_8h.html#ae92c52573fabbcdf5cd672407b3be8ed", null ],
    [ "CFG_FREQUENCY_DEFAULT_SYSTEM_TIMER_FREQUENCY", "de/d5f/oc__system__cfg_8h.html#a83b23685102dd9dd459c8e00fedd3758", null ],
    [ "CFG_FREQUENCY_TARGET_FREQUENCY", "de/d5f/oc__system__cfg_8h.html#a5108a3ac22b2029e25a15ac8e9863347", null ],
    [ "CFG_TIME_MAXIMUM_TIME_FOR_INITIALIZATION_OF_SYSTEM", "de/d5f/oc__system__cfg_8h.html#a0c7b3a37db4ab9d56bd86c59a094fc71", null ],
    [ "CFG_TIME_TIMESTAMP_INCREMENT_TIME", "de/d5f/oc__system__cfg_8h.html#a15908741d84b78a5b6ba001b47d97acf", null ],
    [ "CFG_UINT32_MAXIMUM_NUMBER_OF_THREADS", "de/d5f/oc__system__cfg_8h.html#a5f80787e1ce294dc30374c433836ecc9", null ],
    [ "CFG_UINT8_MAXIMUM_NUMBER_OF_SAVED_ERRORS", "de/d5f/oc__system__cfg_8h.html#a8ec2aae4c10a4cb1b47cf299497c6c42", null ]
];