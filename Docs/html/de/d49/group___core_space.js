var group___core_space =
[
    [ "Bootloader", "d0/d8d/group___boot.html", "d0/d8d/group___boot" ],
    [ "Kernel", "d6/d00/group___kernel.html", "d6/d00/group___kernel" ],
    [ "Threads' Synchronization", "db/dbd/group___core_threads_sync.html", "db/dbd/group___core_threads_sync" ],
    [ "System Drivers", "d3/d8d/group___drivers.html", "d3/d8d/group___drivers" ],
    [ "Graphical User Interface", "d5/d89/group___g_u_i.html", "d5/d89/group___g_u_i" ],
    [ "Network", "df/d1c/group___core_network.html", "df/d1c/group___core_network" ],
    [ "File System", "db/dc3/group___core_file_system.html", "db/dc3/group___core_file_system" ],
    [ "List", "de/d0d/group___list.html", null ],
    [ "Terminal", "d5/dbf/group___terminal.html", "d5/dbf/group___terminal" ],
    [ "System Status Printer", "d1/d45/group___s_y_s_s_p_r_i_n_t.html", null ]
];