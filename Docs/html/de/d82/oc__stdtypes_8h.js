var oc__stdtypes_8h =
[
    [ "oC_int16_MAX", "de/d82/oc__stdtypes_8h.html#aa84ec335929a3cc250f37fab6c3018a6", null ],
    [ "oC_int16_MIN", "de/d82/oc__stdtypes_8h.html#aeea266287b4c421dbea428a4730e1742", null ],
    [ "oC_int32_MAX", "de/d82/oc__stdtypes_8h.html#abe1ce91fc255dd103d55f56eb9194e33", null ],
    [ "oC_int32_MIN", "de/d82/oc__stdtypes_8h.html#af0f273eb575533c9099a991c3be7ed1c", null ],
    [ "oC_int64_MAX", "de/d82/oc__stdtypes_8h.html#a5de03c272298446ee7e2b787f3e28dd3", null ],
    [ "oC_int64_MIN", "de/d82/oc__stdtypes_8h.html#af2587a428f52e879580d8c8411091ef8", null ],
    [ "oC_int8_MAX", "de/d82/oc__stdtypes_8h.html#a81ab6d714c9d06e419dc327e8f47eeb9", null ],
    [ "oC_int8_MIN", "de/d82/oc__stdtypes_8h.html#a8a96c1a2e7cc94b45d8626c3385d86d9", null ],
    [ "oC_uint16_MAX", "de/d82/oc__stdtypes_8h.html#adaac200ab0cb924445a72f69bed6799f", null ],
    [ "oC_uint16_MIN", "de/d82/oc__stdtypes_8h.html#aaa08e7c48f9b9095d810a8d9fd17a51f", null ],
    [ "oC_uint32_MAX", "de/d82/oc__stdtypes_8h.html#ad1dfdde81ba197eca01998ee209be65b", null ],
    [ "oC_uint32_MIN", "de/d82/oc__stdtypes_8h.html#ac4e0e486945d78d3a5d41e7efdbbee15", null ],
    [ "oC_uint64_MAX", "de/d82/oc__stdtypes_8h.html#a5367cf6fff75c3a2cbe0656a33a44652", null ],
    [ "oC_uint64_MIN", "de/d82/oc__stdtypes_8h.html#ab4f72d481ff023a3483743146a2fae72", null ],
    [ "oC_uint8_MAX", "de/d82/oc__stdtypes_8h.html#a54cdfd4e6020d8cd1279c6a7d89a25e5", null ],
    [ "oC_uint8_MIN", "de/d82/oc__stdtypes_8h.html#a3fb741907dec960f1d1c471a1da32a55", null ],
    [ "oC_int16_t", "de/d82/oc__stdtypes_8h.html#a30b2bb7325582768267fa94c4b30aaaf", null ],
    [ "oC_int32_t", "de/d82/oc__stdtypes_8h.html#a34353f5d8510281a090660c46a43f748", null ],
    [ "oC_int64_t", "de/d82/oc__stdtypes_8h.html#aaeb37f1b53ebec5e5fdc3fb27d2d2923", null ],
    [ "oC_int8_t", "de/d82/oc__stdtypes_8h.html#a6aea8c4fab750f923e3e26898cba0675", null ],
    [ "oC_uint16_t", "de/d82/oc__stdtypes_8h.html#ac8f02d04bd887782dc93bca6d2abe3cc", null ],
    [ "oC_uint32_t", "de/d82/oc__stdtypes_8h.html#a6d5d930cea04fbbdf2948d5647e37f10", null ],
    [ "oC_uint64_t", "de/d82/oc__stdtypes_8h.html#ad7b816775fad7873c2bfd6a70f118a50", null ],
    [ "oC_uint8_t", "de/d82/oc__stdtypes_8h.html#a7792f6dbbf821cded6e7f47d7c226de7", null ],
    [ "oC_Power_t", "de/d82/oc__stdtypes_8h.html#a06a66df1806be2b89ddf084819180695", [
      [ "oC_Power_Off", "de/d82/oc__stdtypes_8h.html#a06a66df1806be2b89ddf084819180695a6349f7607f20e2a78062d058476780e9", null ],
      [ "oC_Power_On", "de/d82/oc__stdtypes_8h.html#a06a66df1806be2b89ddf084819180695a674bd1600226656b69e079bfa2e79be0", null ],
      [ "oC_Power_NotHandled", "de/d82/oc__stdtypes_8h.html#a06a66df1806be2b89ddf084819180695a63e8617ab96f781f77fbb7b0e89427a4", null ],
      [ "oC_Power_Invalid", "de/d82/oc__stdtypes_8h.html#a06a66df1806be2b89ddf084819180695a9c0db7109b34a432146c9002b7bafec7", null ]
    ] ]
];