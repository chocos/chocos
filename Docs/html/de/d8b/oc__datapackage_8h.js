var oc__datapackage_8h =
[
    [ "oC_DATA_PACKAGE_MAGIC_NUMBER", "d5/d42/group___data_package.html#ga5e2d20b70414b4861b8475c60ce7d6f0", null ],
    [ "oC_DataPackage_Clear", "d5/d42/group___data_package.html#ga044fcf84ee696a0cdcf6f791b2455294", null ],
    [ "oC_DataPackage_GetDataU16", "d5/d42/group___data_package.html#gab92213fdfc299d0e8f30d2c1f296aa6a", null ],
    [ "oC_DataPackage_GetDataU32", "d5/d42/group___data_package.html#gad22bde2fc46d40bfe067fa7bd46aa5c0", null ],
    [ "oC_DataPackage_GetDataU64", "d5/d42/group___data_package.html#ga08f6fd64092c4f29589401f768b9b143", null ],
    [ "oC_DataPackage_GetDataU8", "d5/d42/group___data_package.html#ga9250694f61b4199e1c00132d8b76262b", null ],
    [ "oC_DataPackage_GetLeftSize", "d5/d42/group___data_package.html#ga4e3c223e3d3361c517ea3646b3fdda4e", null ],
    [ "oC_DataPackage_Initialize", "d5/d42/group___data_package.html#ga37baf6f828107e0277aa5aea1ec750b6", null ],
    [ "oC_DataPackage_IsEmpty", "d5/d42/group___data_package.html#ga20195f7286f7158e4fa303caa7f8c710", null ],
    [ "oC_DataPackage_IsFull", "d5/d42/group___data_package.html#gadcb05a610120de527a7413dbbb0565e3", null ],
    [ "oC_DataPackage_PutDataU16", "d5/d42/group___data_package.html#gafae18fe2fce58cd8277ade95bceeaeb8", null ],
    [ "oC_DataPackage_PutDataU32", "d5/d42/group___data_package.html#gaa47a118bf5fe29da7fede6897352a2ac", null ],
    [ "oC_DataPackage_PutDataU64", "d5/d42/group___data_package.html#ga176e364e53ef7636b8f42d84148d1b4d", null ],
    [ "oC_DataPackage_PutDataU8", "d5/d42/group___data_package.html#gaf3bc72359d6a7204aad42be452956a01", null ]
];