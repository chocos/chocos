var oc__eth__phy_8h =
[
    [ "oC_ETH_PhyRegister_BCR_t", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t" ],
    [ "oC_ETH_PhyRegister_BSR_t", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t.html", "dc/d67/uniono_c___e_t_h___phy_register___b_s_r__t" ],
    [ "oC_ETH_PhyRegister_t", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56c", [
      [ "oC_ETH_PhyRegister_BasicControlRegister", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56cae4801228c4b9e21fd688aaea208e8d3b", null ],
      [ "oC_ETH_PhyRegister_BasicStatusRegister", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56cabf1b6bcf94049b4b70f01ecfd671ec35", null ],
      [ "oC_ETH_PhyRegister_BCR", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56ca233dbaca05438885674044007e3d2bdb", null ],
      [ "oC_ETH_PhyRegister_BSR", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56ca3250ccdd662a18634af5931d49837d6a", null ],
      [ "oC_ETH_PhyRegister_Max", "de/d56/oc__eth__phy_8h.html#a3fd1bfd9a41b167f5c6c1bcab148d56ca8de1628a5f4924039c2600d2dbb16cca", null ]
    ] ],
    [ "oC_ETH_PhyReset", "de/d56/oc__eth__phy_8h.html#ad16a9ac317cf9399c61c339cd2f6f46b", null ],
    [ "oC_ETH_ReadLinkStatus", "de/d56/oc__eth__phy_8h.html#a604ec6adafb24c370e05cc2bc4cd55ec", null ],
    [ "oC_ETH_ReadPhyRegister", "de/d56/oc__eth__phy_8h.html#aad7a030ed8cf11a4ddac045ac42e6b06", null ],
    [ "oC_ETH_SetAutoNegotiation", "de/d56/oc__eth__phy_8h.html#a9bdd5ec43f2f7e4c4fd600ca195fe1bb", null ],
    [ "oC_ETH_SetPhyLoopback", "de/d56/oc__eth__phy_8h.html#a710f48198f9387f6209bba8405cf3b0e", null ],
    [ "oC_ETH_WritePhyRegister", "de/d56/oc__eth__phy_8h.html#a23cc66245af8e169c68a28a4e4a9f1ef", null ]
];