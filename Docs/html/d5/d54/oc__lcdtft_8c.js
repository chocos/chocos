var oc__lcdtft_8c =
[
    [ "Context_t", "d7/d83/struct_context__t.html", "d7/d83/struct_context__t" ],
    [ "Context_Delete", "d5/d54/oc__lcdtft_8c.html#ad757eb13d0b15c1deeb9ee11c71d811e", null ],
    [ "Context_New", "d5/d54/oc__lcdtft_8c.html#a1a58e091c3ed38b4095433624b2ecfe5", null ],
    [ "IsContextCorrect", "d5/d54/oc__lcdtft_8c.html#a3217434c78c19a72b5bc0939ca7aec3a", null ],
    [ "oC_LCDTFT_Configure", "d2/dea/group___l_c_d_t_f_t.html#gacac3787891de605e00495978037294ff", null ],
    [ "oC_LCDTFT_Ioctl", "d2/dea/group___l_c_d_t_f_t.html#gaaa1409c5f563374b41b1646c68b5e4ee", null ],
    [ "oC_LCDTFT_IsTurnedOn", "d2/dea/group___l_c_d_t_f_t.html#ga5ef3b309ff65c1a31767813568cd413c", null ],
    [ "oC_LCDTFT_TurnOff", "d2/dea/group___l_c_d_t_f_t.html#ga0653553a250b412ae56f9df9bda6018b", null ],
    [ "oC_LCDTFT_TurnOn", "d2/dea/group___l_c_d_t_f_t.html#ga2c358bd78138abff3b1461830bc01c8a", null ],
    [ "oC_LCDTFT_Unconfigure", "d2/dea/group___l_c_d_t_f_t.html#gab79686c48981059d2a62f87cf41bb92d", null ],
    [ "Allocator", "d5/d54/oc__lcdtft_8c.html#a35e1e2c2e113243f4ff2437fcc071643", null ]
];