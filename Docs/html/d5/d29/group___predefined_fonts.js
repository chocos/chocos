var group___predefined_fonts =
[
    [ "oC_FontInfo_Algerian_28pt", "d5/d29/group___predefined_fonts.html#ga95d76223ae973c939730a991f2d54200", null ],
    [ "oC_FontInfo_Arial_10pt", "d5/d29/group___predefined_fonts.html#gafff87481f2f23d55d8699f648e7c5ae4", null ],
    [ "oC_FontInfo_BlackadderITC_20pt", "d5/d29/group___predefined_fonts.html#ga5c008f047ef11dd0e9e7e98571c3dcb1", null ],
    [ "oC_FontInfo_Castellar_28pt", "d5/d29/group___predefined_fonts.html#ga7712304bf4256b75433fd662a8b488dd", null ],
    [ "oC_FontInfo_Consolas", "d5/d29/group___predefined_fonts.html#ga389dc2d62422e5d059be50b6955e8104", null ],
    [ "oC_FontInfo_Consolas6pt", "d5/d29/group___predefined_fonts.html#gadc980c3cec85c19249dae42cd6bcd18d", null ],
    [ "oC_FontInfo_Consolas7pt", "d5/d29/group___predefined_fonts.html#ga0ee738d48e8ccc34dc1888a6f10ab2b9", null ],
    [ "oC_FontInfo_Consolas8pt", "d5/d29/group___predefined_fonts.html#ga40e1fe6032fc0caae6e12aca2dd39ee3", null ],
    [ "oC_FontInfo_Consolas_20pt", "d5/d29/group___predefined_fonts.html#ga81d9894b23bf53452e108c51db54673d", null ],
    [ "oC_FontInfo_CooperBlack_20pt", "d5/d29/group___predefined_fonts.html#ga57d75986780ceb2cfceb7c913c727075", null ],
    [ "oC_FontInfo_NiagaraEngraved_36pt", "d5/d29/group___predefined_fonts.html#gaab134147534e0feeed8c2d3d40bf494d", null ],
    [ "oC_FontInfo_OldEnglishTextMT_36pt", "d5/d29/group___predefined_fonts.html#ga5b920721a579af7db21733c67f9a4a72", null ],
    [ "oC_FontInfo_ShowcardGothic_36pt", "d5/d29/group___predefined_fonts.html#gae709e6936eadf332fb90b88c0b53e048", null ]
];