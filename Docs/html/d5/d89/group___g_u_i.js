var group___g_u_i =
[
    [ "ColorMap", "d8/d36/group___color_map.html", "d8/d36/group___color_map" ],
    [ "Font", "df/d04/group___font.html", "df/d04/group___font" ],
    [ "Input Controller", "d2/d9e/group___i_ctrl.html", "d2/d9e/group___i_ctrl" ],
    [ "ICtrlMan - Input Control Manager Module", "dc/d05/group___i_ctrl_man.html", "dc/d05/group___i_ctrl_man" ],
    [ "Screen", "df/d7b/group___screen.html", "df/d7b/group___screen" ],
    [ "Screens Manager", "d9/d5a/group___screen_man.html", "d9/d5a/group___screen_man" ],
    [ "TGUI - Terminal GUI Module", "dc/d3b/group___t_g_u_i.html", "dc/d3b/group___t_g_u_i" ],
    [ "Widget Screen", "db/d47/group___widget_screen.html", "db/d47/group___widget_screen" ],
    [ "Terminals", "d5/d26/group___terminals.html", null ],
    [ "Pixel - The pixel object", "d0/de6/group___pixel.html", "d0/de6/group___pixel" ]
];