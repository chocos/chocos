var oc__led_8h =
[
    [ "oC_LED_Config_t", "d3/dee/structo_c___l_e_d___config__t.html", "d3/dee/structo_c___l_e_d___config__t" ],
    [ "oC_LED_Context_t", "d5/d36/oc__led_8h.html#affa960aee9191994ea35a9326771e4e4", null ],
    [ "oC_LED_Index_t", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bb", [
      [ "oC_LED_Index_Red", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bba8e82aaeb30f1b4fab3da9d055d0c2e7a", null ],
      [ "oC_LED_Index_Single", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bba88c970acb1d6ff2c2235cb730065cc21", null ],
      [ "oC_LED_Index_Green", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bba1a06e87190cce48d967e896ddd7c5249", null ],
      [ "oC_LED_Index_Blue", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bba735456a2faf8af9c7197d1ebf7405667", null ],
      [ "oC_LED_Index_NumberOfElements", "d5/d36/oc__led_8h.html#a274a38337d628313ff4d44bac8f5a4bbaa75c0432b6f4f4df88ad9096f8d9ccf1", null ]
    ] ],
    [ "oC_LED_Mode_t", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9c", [
      [ "oC_LED_Mode_Red", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca799250bb2f421a02753001a1b5833344", null ],
      [ "oC_LED_Mode_Green", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9caa2435243f9d82685744913063cefb2b8", null ],
      [ "oC_LED_Mode_Blue", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca9e400a08511844cd3ea449a54aea2527", null ],
      [ "oC_LED_Mode_UsePWM", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9cad1b557b323f1c20c8e3d2a191de88ca2", null ],
      [ "oC_LED_Mode_RedActiveLow", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9caf05cc7a4830eead870cff0537f0fac13", null ],
      [ "oC_LED_Mode_GreenActiveLow", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9cad80ecb7351634f22f0e06b9c340aeb35", null ],
      [ "oC_LED_Mode_BlueActiveLow", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca9f435f2a092aa0d565e4f72eab3568bd", null ],
      [ "oC_LED_Mode_Single", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca05bdd28984418fc70c9389ef5035efe7", null ],
      [ "oC_LED_Mode_SingleActiveLow", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9caad5aee170eae06eb6e0baf5c78d4a30c", null ],
      [ "oC_LED_Mode_RGB", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca814d2f4579e732d6e0d3dc984ab99cba", null ],
      [ "oC_LED_Mode_RG", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9cad8496b9a36f0e07059ff788334d0d670", null ],
      [ "oC_LED_Mode_RB", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca2b71443b784dceedc12f5d7c0162315a", null ],
      [ "oC_LED_Mode_BG", "d5/d36/oc__led_8h.html#a4da1af0223b40bdf51c54bcff9e44a9ca46d4ab2c58815b085069de710aee8957", null ]
    ] ],
    [ "oC_LED_Configure", "d5/d36/oc__led_8h.html#aafbea8b805371b45cbaad4251bf2254c", null ],
    [ "oC_LED_Ioctl", "d5/d36/oc__led_8h.html#a8d8190231f9c178033858d4e2b549a29", null ],
    [ "oC_LED_SetColor", "d5/d36/oc__led_8h.html#a2515c73e2d2b63e2d35966ce2073f21a", null ],
    [ "oC_LED_SetLight", "d5/d36/oc__led_8h.html#aa51329a9234ce0f8d0abd1bf75821461", null ],
    [ "oC_LED_Unconfigure", "d5/d36/oc__led_8h.html#af79630ecf67797531ed604acf783b880", null ],
    [ "oC_LED_Write", "d5/d36/oc__led_8h.html#adcab1e54c317d0613f4fcbbc55dd25eb", null ]
];