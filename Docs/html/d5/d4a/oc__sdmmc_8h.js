var oc__sdmmc_8h =
[
    [ "oC_SDMMC_LONG_RESPONSE_LENGTH", "d5/d4a/oc__sdmmc_8h.html#a2e42f8d38181625994b0ad98b6d581eb", null ],
    [ "oC_SDMMC_Context_t", "d4/d1b/group___s_d_m_m_c.html#ga81bfc7e08aadbf1597c4f9ef7e57a9f6", null ],
    [ "oC_SDMMC_Pins_t", "d4/d1b/group___s_d_m_m_c.html#ga322ba2ddb236be86b588f5178e0609ef", null ],
    [ "oC_SDMMC_RelativeCardAddress_t", "d4/d1b/group___s_d_m_m_c.html#gadd4649752df5ea0ab3ca6ce7cf2261ae", null ],
    [ "oC_SDMMC_CardType_t", "d4/d1b/group___s_d_m_m_c.html#ga4cb4c4e16d7d734dc3c60633896f2808", [
      [ "oC_SDMMC_CardType_Unknown", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808abb436250153f95a71cbdea0917834c5b", null ],
      [ "oC_SDMMC_CardType_SDSC", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808a05065a688d3a9a6b522c2d6c1e96501f", null ],
      [ "oC_SDMMC_CardType_SDHC", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808a8eca9b63621371f1c855ba0332b2a24c", null ],
      [ "oC_SDMMC_CardType_SDXC", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808aafc4bcaf5aeec44733b262adee5b0859", null ],
      [ "oC_SDMMC_CardType_SDHC_or_SDXC", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808a7642f5177718854c41a62ec236141432", null ],
      [ "oC_SDMMC_CardType_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#gga4cb4c4e16d7d734dc3c60633896f2808ac878f6e74a8752f67da5b31f39902925", null ]
    ] ],
    [ "oC_SDMMC_CommunicationInterfaceId_t", "d4/d1b/group___s_d_m_m_c.html#gaa9966d73a3a4ad2b7b6ffa3f6935188f", [
      [ "oC_SDMMC_CommunicationInterfaceId_Unknown", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188faa93a6ad43222db594b882b2f4856c6e8", null ],
      [ "oC_SDMMC_CommunicationInterfaceId_SDCv2", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188fa7e5c63794bf31b0242d5afe312689826", null ],
      [ "oC_SDMMC_CommunicationInterfaceId_SDCv1", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188fa06e59344e7e733ea61c6f19263f2e2a7", null ],
      [ "oC_SDMMC_CommunicationInterfaceId_MMCPlus", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188fa6eb4edec5d2c8c87de3544e280cfc7fb", null ],
      [ "oC_SDMMC_CommunicationInterfaceId_MMC", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188fa1005b382dd440e6d59c6ffaaacb67d33", null ],
      [ "oC_SDMMC_CommunicationInterfaceId_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#ggaa9966d73a3a4ad2b7b6ffa3f6935188fae11d0e70c2ac11cd5ff80b05a3b05eeb", null ]
    ] ],
    [ "oC_SDMMC_DetectionMode_t", "d4/d1b/group___s_d_m_m_c.html#ga84a4ec3f11ebd4ccac254f187d6a3106", [
      [ "oC_SDMMC_DetectionMode_Interrupt", "d4/d1b/group___s_d_m_m_c.html#gga84a4ec3f11ebd4ccac254f187d6a3106aa8133cfba829d4d6dd8c206a764a5472", null ],
      [ "oC_SDMMC_DetectionMode_Polling", "d4/d1b/group___s_d_m_m_c.html#gga84a4ec3f11ebd4ccac254f187d6a3106a1ca3c055ee456396a88a53662792aff1", null ],
      [ "oC_SDMMC_DetectionMode_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#gga84a4ec3f11ebd4ccac254f187d6a3106aba3c344861e3ecf550d332e7d262984a", null ]
    ] ],
    [ "oC_SDMMC_FileFormat_t", "d4/d1b/group___s_d_m_m_c.html#ga7b621b82b0c9ca6310fa811cbbf099aa", [
      [ "oC_SDMMC_FileFormat_Unknown", "d4/d1b/group___s_d_m_m_c.html#gga7b621b82b0c9ca6310fa811cbbf099aaa969b2349bcbb60892b43cc635aac65ab", null ],
      [ "oC_SDMMC_FileFormat_PartitionTable", "d4/d1b/group___s_d_m_m_c.html#gga7b621b82b0c9ca6310fa811cbbf099aaaa02965bd557de803bc6cb9c6f8c02821", null ],
      [ "oC_SDMMC_FileFormat_Fat", "d4/d1b/group___s_d_m_m_c.html#gga7b621b82b0c9ca6310fa811cbbf099aaa5de660b8aa4feb382ffc92179f54e460", null ],
      [ "oC_SDMMC_FileFormat_UniversalFileFormat", "d4/d1b/group___s_d_m_m_c.html#gga7b621b82b0c9ca6310fa811cbbf099aaa916ab318005ba4e1205ba44cd6844fe3", null ],
      [ "oC_SDMMC_FileFormat_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#gga7b621b82b0c9ca6310fa811cbbf099aaa3d4f8c279654526ad24ace29c6b80ec5", null ]
    ] ],
    [ "oC_SDMMC_Mode_t", "d4/d1b/group___s_d_m_m_c.html#ga68903e2d640a5b501b91893c2422fe79", [
      [ "oC_SDMMC_Mode_Auto", "d4/d1b/group___s_d_m_m_c.html#gga68903e2d640a5b501b91893c2422fe79a4380ed7e1dd8413f3f471fdef557556a", null ],
      [ "oC_SDMMC_Mode_LLD", "d4/d1b/group___s_d_m_m_c.html#gga68903e2d640a5b501b91893c2422fe79a374153f9c1a71ac36f192cd3ccfc0a12", null ],
      [ "oC_SDMMC_Mode_SPI", "d4/d1b/group___s_d_m_m_c.html#gga68903e2d640a5b501b91893c2422fe79ab6b91908f841ec9024b69153cca08a44", null ],
      [ "oC_SDMMC_Mode_SW_1Bit", "d4/d1b/group___s_d_m_m_c.html#gga68903e2d640a5b501b91893c2422fe79a59efb416039d225f48986411c75ac5ee", null ],
      [ "oC_SDMMC_Mode_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#gga68903e2d640a5b501b91893c2422fe79a50858da0f96a0771a841591716763c21", null ]
    ] ],
    [ "oC_SDMMC_PinIndex_t", "d4/d1b/group___s_d_m_m_c.html#ga456cdaf430312fddcd014a7ddde1064d", [
      [ "oC_SDMMC_PinIndex_P1", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da8cb4e5209fc5062c6d7788f2a2cd4a53", null ],
      [ "oC_SDMMC_PinIndex_P2", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da3d9f8e40f092a384362c5934eafb4e84", null ],
      [ "oC_SDMMC_PinIndex_P3", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da84d0f08dd7fc1dab1bf127316c5c32f3", null ],
      [ "oC_SDMMC_PinIndex_P4", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dac12265508db86debf9628a6e137e2bbf", null ],
      [ "oC_SDMMC_PinIndex_P5", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da3eba0522103ee2e62256d7d669dc809d", null ],
      [ "oC_SDMMC_PinIndex_P6", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da7c962b84ccc690d7e81588c571f67d2e", null ],
      [ "oC_SDMMC_PinIndex_P7", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da1429e656f1232d56fb5761c826667bd3", null ],
      [ "oC_SDMMC_PinIndex_P8", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da5347ed5ee1f30e63f63c1d80ca5d13f3", null ],
      [ "oC_SDMMC_PinIndex_P9", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da0645f47b38b5360c7ab7f7085807cd90", null ],
      [ "oC_SDMMC_PinIndex_P10", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da6021e2a67b0927a350812bfd4c2edff9", null ],
      [ "oC_SDMMC_PinIndex_P11", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da86f12f308817fd2a8e83d7a85f63101f", null ],
      [ "oC_SDMMC_PinIndex_P12", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dae101a4b5617505e21c0e1e045d686c0f", null ],
      [ "oC_SDMMC_PinIndex_P13", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064daf7ec51574fc2048767cd22bb680b46c6", null ],
      [ "oC_SDMMC_PinIndex_NumberOfPins", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da1cf90308ff45d654d58f53ba5157c019", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_nCS", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dad1ef925b3cca8abdcd5534ed44ff3894", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_DI", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da9d3420f752468f9f0d024fae45ea86eb", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_MOSI", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da34999aef8ccee8a648cbaaa997ed8468", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_CLK", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da7150fb073af6f2d0969b2e525987e143", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_DO", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da3b656ff9a2fd2e5a2be3e53f45826c0e", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_MISO", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da7b09f310788f40e1e49a5fa9a1551374", null ],
      [ "oC_SDMMC_PinIndex_SpiMode_nIRQ", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da0e97157c782c4110163ebe7657deb194", null ],
      [ "oC_SDMMC_PinIndex_1BitMode_CD", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da73d3a17414c5d2d6b4f82cc14cd1337f", null ],
      [ "oC_SDMMC_PinIndex_1BitMode_CMD", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064daec1c72ad499522359d0eebcaa09d87a0", null ],
      [ "oC_SDMMC_PinIndex_1BitMode_CLK", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dae9ac66326469db5d208c575f4fc7aa2b", null ],
      [ "oC_SDMMC_PinIndex_1BitMode_DAT0", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da523a0cd484d7f572403ad44a13e495ed", null ],
      [ "oC_SDMMC_PinIndex_1BitMode_nIRQ", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da8c70376b68ef79c7b4fa7c7231b1492f", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_DAT3", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da319c99e7d09d5a568c98f56c04ee3c9a", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_CMD", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dad7d5e9ecde12dae3a73da771a119bce4", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_CLK", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064daa1c2fafb0ec3fdcb860efbc960558908", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_DAT0", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da8422093a7ebc049dd585a40fc0d76ef8", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_nIRQ", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da8bb9da22e288eb2e4d6438622c08de9d", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_DAT1", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dad4b22ee41a13167df295a2e1284caaf4", null ],
      [ "oC_SDMMC_PinIndex_4BitMode_DAT2", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dae7ccf836662b30d9fec59ed2bec8bf1f", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT3", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dad17cb0fcb9f3e2bc4b8ca6c092610722", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_CMD", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da1159f656d059d6f7ba1be28fedbe806e", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_CLK", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dada0c88359c0f77bec5b9435c1b20f3f3", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT0", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da401785074c3064f8b114e6ca0ad32584", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT1", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da0e5f74f505d589ae806451557728cd7f", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT2", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da9e51beca80538f2c5c955378a1a5886e", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT4", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da6e4839c66b0969148390f1ce36df6580", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT5", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064daa426e41c1525e8ab2b0e3ac212998395", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT6", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064da6d3c831b8d89965922baf7700d32d032", null ],
      [ "oC_SDMMC_PinIndex_8BitMode_DAT7", "d4/d1b/group___s_d_m_m_c.html#gga456cdaf430312fddcd014a7ddde1064dae4d1f393ccf521f875e23f33e1e40ea5", null ]
    ] ],
    [ "oC_SDMMC_PowerMode_t", "d4/d1b/group___s_d_m_m_c.html#gad2df5ccc98409a9e85765e5d9a7c1927", [
      [ "oC_SDMMC_PowerMode_MaxPerformance", "d4/d1b/group___s_d_m_m_c.html#ggad2df5ccc98409a9e85765e5d9a7c1927ae5c32f692e775674de832852070dbc61", null ],
      [ "oC_SDMMC_PowerMode_PowerSaving", "d4/d1b/group___s_d_m_m_c.html#ggad2df5ccc98409a9e85765e5d9a7c1927a831210c0330e606242b16b6327ac9853", null ],
      [ "oC_SDMMC_PowerMode_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#ggad2df5ccc98409a9e85765e5d9a7c1927ab41f93ed9485f51e711f8cf38dd2d7f4", null ]
    ] ],
    [ "oC_SDMMC_State_t", "d4/d1b/group___s_d_m_m_c.html#ga330882d123bbfca626d5d83b1e35eb49", [
      [ "oC_SDMMC_State_Idle", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49ab70f7929e9f768c53858c6c99f0c3f99", null ],
      [ "oC_SDMMC_State_Ready", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49afcfcf7a30a158f4c221b4d889ffa6c3c", null ],
      [ "oC_SDMMC_State_Identification", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49a9d6f1fe17e0bc1b81778cd64d3e570c3", null ],
      [ "oC_SDMMC_State_StandBy", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49a28a112ae0b8cd1d04ba21aaabea8e367", null ],
      [ "oC_SDMMC_State_Transfer", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49ae4816b3245dea88b23ead874f5ceb22c", null ],
      [ "oC_SDMMC_State_SendingData", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49aea5f4a361500a6b32a4ca2a4cfdc9523", null ],
      [ "oC_SDMMC_State_ReceiveData", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49aee5238576a71653022b478b4397a7e52", null ],
      [ "oC_SDMMC_State_Programming", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49ae144be6d63e92f084808a42f39ed6c78", null ],
      [ "oC_SDMMC_State_Disconnect", "d4/d1b/group___s_d_m_m_c.html#gga330882d123bbfca626d5d83b1e35eb49aafe38e420c105cc840a0355f48e63baf", null ]
    ] ],
    [ "oC_SDMMC_TransferMode_t", "d4/d1b/group___s_d_m_m_c.html#gacba4442040b67ed6bf28709851da7c33", [
      [ "oC_SDMMC_TransferMode_Auto", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33affa311426496b10719e3048c8a784bb0", null ],
      [ "oC_SDMMC_TransferMode_1Bit", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33a64ec6728f6a0ed8fe89f70f8acddfa3e", null ],
      [ "oC_SDMMC_TransferMode_4Bit", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33a9f6043d73e6970225c0cf7ae3248fa83", null ],
      [ "oC_SDMMC_TransferMode_8Bit", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33ae2fc9e50a08edee7f08f18dddeba7f46", null ],
      [ "oC_SDMMC_TransferMode_SPI", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33a6b3bd360ff7a294a9a03a9f34d5c8832", null ],
      [ "oC_SDMMC_TransferMode_NumberOfElements", "d4/d1b/group___s_d_m_m_c.html#ggacba4442040b67ed6bf28709851da7c33a8917a5195b9442e42e7dbf59887c4e3d", null ]
    ] ],
    [ "oC_SDMMC_Configure", "d5/d4a/oc__sdmmc_8h.html#a7157ff8beab7c74301ca7d993b6e9b8a", null ],
    [ "oC_SDMMC_EraseSectors", "d5/d4a/oc__sdmmc_8h.html#a1574c83bc4db2e11bce23c052dbfd424", null ],
    [ "oC_SDMMC_GetCardStateName", "d5/d4a/oc__sdmmc_8h.html#a3394db0d9b63f61ca740529849bab218", null ],
    [ "oC_SDMMC_Ioctl", "d5/d4a/oc__sdmmc_8h.html#a040edaf19ed79562407814eb216964c3", null ],
    [ "oC_SDMMC_IsTurnedOn", "d5/d4a/oc__sdmmc_8h.html#a3e76ff73b7a5260a51dc9777c867915e", null ],
    [ "oC_SDMMC_ReadCardInfo", "d5/d4a/oc__sdmmc_8h.html#a30970b57b5bda4ce06843d5fce162994", null ],
    [ "oC_SDMMC_ReadCardType", "d5/d4a/oc__sdmmc_8h.html#a6e9e4ab954d71fe1cb6350c56a0328a1", null ],
    [ "oC_SDMMC_ReadMode", "d5/d4a/oc__sdmmc_8h.html#a3ef3fd03ec987372ffb4159bfabb7c76", null ],
    [ "oC_SDMMC_ReadNumberOfSectors", "d5/d4a/oc__sdmmc_8h.html#aa6ca13230983ec93b3f7708b4638ea7d", null ],
    [ "oC_SDMMC_ReadSectors", "d5/d4a/oc__sdmmc_8h.html#a1284bba4aba6ef372795dc09682938c3", null ],
    [ "oC_SDMMC_ReadSectorSize", "d5/d4a/oc__sdmmc_8h.html#a7937e3ce049026a1c4e92e7320d5567e", null ],
    [ "oC_SDMMC_ReadTransferMode", "d5/d4a/oc__sdmmc_8h.html#a942d44cd467549ca11c5eea6da9733db", null ],
    [ "oC_SDMMC_ReadTransferModeByName", "d5/d4a/oc__sdmmc_8h.html#a513f64f70480082c82f0e2d5a49dd8cc", null ],
    [ "oC_SDMMC_ReadTransferModeName", "d5/d4a/oc__sdmmc_8h.html#ab299d93217913b4e02a0836745c72e1f", null ],
    [ "oC_SDMMC_SetTransferMode", "d5/d4a/oc__sdmmc_8h.html#a6548a45ea9e1de0d34cded48856e7b81", null ],
    [ "oC_SDMMC_TurnOff", "d5/d4a/oc__sdmmc_8h.html#ad9c65264ac16103eb2ab3c4e6063f973", null ],
    [ "oC_SDMMC_TurnOn", "d5/d4a/oc__sdmmc_8h.html#a5f968e2d40875db881d9d038dcc072a9", null ],
    [ "oC_SDMMC_Unconfigure", "d5/d4a/oc__sdmmc_8h.html#a7f1ff6ca9ea2b764c96978b95dab7c7e", null ],
    [ "oC_SDMMC_WaitForDiskEject", "d5/d4a/oc__sdmmc_8h.html#a0e9dab3033d4de1fc322b0e320ad7082", null ],
    [ "oC_SDMMC_WaitForNewDisk", "d5/d4a/oc__sdmmc_8h.html#a3349a937279e328354a2c9d01eb77e77", null ],
    [ "oC_SDMMC_WriteSectors", "d5/d4a/oc__sdmmc_8h.html#ab865739cae657769a3e06d7efd3ace6c", null ]
];