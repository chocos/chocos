var structo_c___net___ipv4_info__t =
[
    [ "BroadcastIP", "da/d29/structo_c___net___ipv4_info__t.html#a8dfba83140bd844128e72a629c2878ec", null ],
    [ "DefaultTTL", "da/d29/structo_c___net___ipv4_info__t.html#aa2d9e32d4f7b720615146afa5d0f4622", null ],
    [ "DhcpIP", "da/d29/structo_c___net___ipv4_info__t.html#a260da825cb2276c5d9fc7203d2afa9b9", null ],
    [ "DhcpServerName", "da/d29/structo_c___net___ipv4_info__t.html#a72abe88ae0e7b8218af80c89cfee540d", null ],
    [ "DnsIP", "da/d29/structo_c___net___ipv4_info__t.html#a51dfc6fe70f71a967507424262b105e7", null ],
    [ "GatewayIP", "da/d29/structo_c___net___ipv4_info__t.html#a543e6d53ebfc372d262be065f0879b90", null ],
    [ "HardwareRouterAddress", "da/d29/structo_c___net___ipv4_info__t.html#a2f96867540c80cdb207edb43ae88510d", null ],
    [ "IP", "da/d29/structo_c___net___ipv4_info__t.html#ae5eabd51fc3664045d5a0fc94f27450c", null ],
    [ "IpExpiredTimestamp", "da/d29/structo_c___net___ipv4_info__t.html#a172230aaed81032de67bc1e3ed2e8e98", null ],
    [ "LeaseTime", "da/d29/structo_c___net___ipv4_info__t.html#a41f3e3fdf9bec6a8b47133c4754c537d", null ],
    [ "MTU", "da/d29/structo_c___net___ipv4_info__t.html#aea846b4bffed20847e6cc9d3da0b6f40", null ],
    [ "NetIP", "da/d29/structo_c___net___ipv4_info__t.html#a095e23507119b6a78a0e627a65c7e6fc", null ],
    [ "Netmask", "da/d29/structo_c___net___ipv4_info__t.html#acccf4aee1c6f6dc1de0e89d2c0fa98c9", null ],
    [ "NtpIP", "da/d29/structo_c___net___ipv4_info__t.html#a48ca2d7a264aecebe91be8383f21973b", null ],
    [ "StaticIP", "da/d29/structo_c___net___ipv4_info__t.html#ae69aafb0fb1d7c6cebaea7ff44513e2d", null ]
];