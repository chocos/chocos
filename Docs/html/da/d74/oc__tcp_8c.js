var oc__tcp_8c =
[
    [ "CloseConnectionsCreatedBy", "da/d74/oc__tcp_8c.html#aab3eee7fc11384e36578edaaea9c7dfa", null ],
    [ "CloseServersCreatedBy", "da/d74/oc__tcp_8c.html#abacef263e752b7ed4d7b0c8c66253aa9", null ],
    [ "GetConnectionCreatedBy", "da/d74/oc__tcp_8c.html#ac7b5db64853e16dea7b7676ebcfe82fe", null ],
    [ "GetServerCreatedBy", "da/d74/oc__tcp_8c.html#ab229370b4e83c127889643d4bfc05590", null ],
    [ "GetServerWithConnection", "da/d74/oc__tcp_8c.html#ae33cf066a0f1ef8cc6706c04f0fba4f7", null ],
    [ "oC_List", "da/d74/oc__tcp_8c.html#a6b2bc40b59fe7a20ac6063d5d0f2b9c8", null ],
    [ "oC_Tcp_Accept", "d9/d9b/group___tcp.html#ga320979cf507974d798fbea3bf1fa4cf6", null ],
    [ "oC_Tcp_CloseProcess", "d9/d9b/group___tcp.html#gab011fa12c1613718f8fee2a8a0623706", null ],
    [ "oC_Tcp_Connect", "d9/d9b/group___tcp.html#gab0a54bf43c3866c750d63897ebd1aba9", null ],
    [ "oC_Tcp_Disconnect", "d9/d9b/group___tcp.html#ga446da2931f8af91efbd0788ac6a85ca2", null ],
    [ "oC_Tcp_IsPortReserved", "d9/d9b/group___tcp.html#gad9e99e218f9bda0bafbf55c3def961ba", null ],
    [ "oC_Tcp_IsPortReservedBy", "d9/d9b/group___tcp.html#gaa7537e8a57c84c5a3d1d7ad6fd45d002", null ],
    [ "oC_Tcp_Listen", "d9/d9b/group___tcp.html#gae87a0ab398ed8c959fb316a4aaf487b4", null ],
    [ "oC_Tcp_Receive", "d9/d9b/group___tcp.html#ga0241f2e0a7dc5fd1cc3f38b3d03781b9", null ],
    [ "oC_Tcp_ReleasePort", "d9/d9b/group___tcp.html#ga02f198e379759b1277c76750c53fcb02", null ],
    [ "oC_Tcp_ReservePort", "d9/d9b/group___tcp.html#gaf631aeaddfea2758eea2c1542aa9dc11", null ],
    [ "oC_Tcp_Send", "d9/d9b/group___tcp.html#ga4817c256e9a24936a62b0f876c1db14d", null ],
    [ "oC_Tcp_StopListen", "d9/d9b/group___tcp.html#ga55766d3595157548575af7ada6e8d1a0", null ],
    [ "oC_Tcp_TurnOff", "d9/d9b/group___tcp.html#ga6c8211487f9ecb5ce9b5983235a6aede", null ],
    [ "oC_Tcp_TurnOn", "d9/d9b/group___tcp.html#ga1580ffe548d8d02739fcaed87e60ac11", null ]
];