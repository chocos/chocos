var structo_c___s_d_m_m_c___response__t =
[
    [ "CardId", "da/db7/structo_c___s_d_m_m_c___response__t.html#add28e12295cb04579ba1320fe6b435c6", null ],
    [ "CardSpecificData", "da/db7/structo_c___s_d_m_m_c___response__t.html#aa372b1cf8320ee983476a87b35ea8454", null ],
    [ "LongResponse", "da/db7/structo_c___s_d_m_m_c___response__t.html#a14bdfa8836bf728be4a6ecce37f5ff77", null ],
    [ "R1", "da/db7/structo_c___s_d_m_m_c___response__t.html#a0de05052e9ef153cd6e77f3d5c7b9253", null ],
    [ "R3", "da/db7/structo_c___s_d_m_m_c___response__t.html#a493c72b57bd3a55e99a4b00d1889d812", null ],
    [ "R6", "da/db7/structo_c___s_d_m_m_c___response__t.html#a9ae64c8fa2d09c528a920140206c8980", null ],
    [ "R7", "da/db7/structo_c___s_d_m_m_c___response__t.html#a27cdec9cd28ae3d57751008c1849835d", null ],
    [ "ShortResponse", "da/db7/structo_c___s_d_m_m_c___response__t.html#a2022631204b25dfc03bb1f3d8d0df9f5", null ],
    [ "Type", "da/db7/structo_c___s_d_m_m_c___response__t.html#a3087668c7b9723d10551280709677205", null ]
];