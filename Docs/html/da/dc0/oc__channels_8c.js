var oc__channels_8c =
[
    [ "oC_Channel_DisableInterruptFunction", "da/dc0/oc__channels_8c.html#a7a0a0ca0c33842ae4e00e46cd978db50", null ],
    [ "oC_Channel_EnableInterruptFunction", "da/dc0/oc__channels_8c.html#abbb55659fdbcf141229071efa57e2565", null ],
    [ "oC_Channel_IsInterruptEnabledFunction", "da/dc0/oc__channels_8c.html#af4dcf4ec7416a37cd1ae428137fca0ed", null ],
    [ "oC_Channel_SetInterruptPriorityFunction", "da/dc0/oc__channels_8c.html#aa5cea080a9b23f53549ba995800c9684", null ],
    [ "oC_ChannelsData", "d9/d7a/group___channels.html#ga9d71952c02dea71283f04e3961e39c1c", null ]
];