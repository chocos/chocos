var ti_2lm4f_2lld_2clock_2oc__clock__lld_8c =
[
    [ "CallClockConfiguredEvent", "d9/d65/ti_2lm4f_2lld_2clock_2oc__clock__lld_8c.html#a2ad9c56014447ce9f9d1502fec23002b", null ],
    [ "ConfigureClock", "d9/d65/ti_2lm4f_2lld_2clock_2oc__clock__lld_8c.html#a8143e5ec68fe0c396b4d941e06fcda3f", null ],
    [ "FindDivisor", "d9/d65/ti_2lm4f_2lld_2clock_2oc__clock__lld_8c.html#aa7e70125f8db410f91e355246ffb2665", null ],
    [ "GetXtalForFrequency", "d9/d65/ti_2lm4f_2lld_2clock_2oc__clock__lld_8c.html#ab9c39d34d5e5396cefcc4516d59f878a", null ],
    [ "oC_CLOCK_LLD_ConfigureExternalClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga205c216ee32b12d623726c7d7262e74f", null ],
    [ "oC_CLOCK_LLD_ConfigureHibernationClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga58824f39f62213a0dd295d8ff9248b98", null ],
    [ "oC_CLOCK_LLD_ConfigureInternalClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gace5ac7235288066ca80dab2eb62d0a29", null ],
    [ "oC_CLOCK_LLD_DelayForMicroseconds", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga62c035cd799c19696a6672d462d82cd2", null ],
    [ "oC_CLOCK_LLD_GetClockFrequency", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaf5300693845bfacad20b288ab640c307", null ],
    [ "oC_CLOCK_LLD_GetClockSource", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga09920d987413a24dcdf802f2c8c3e8de", null ],
    [ "oC_CLOCK_LLD_GetMaximumClockFrequency", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga57f5fb7b7a15952525edbdc73dc07da6", null ],
    [ "oC_CLOCK_LLD_SetClockConfiguredInterrupt", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga3d1d77f14159fda01daadbb13c3c40f0", null ],
    [ "oC_CLOCK_LLD_TurnOffDriver", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaeb7c47238b786785485cf8178da91d15", null ],
    [ "oC_CLOCK_LLD_TurnOnDriver", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaae3eca0b66e8536a74abf5786b097d41", null ]
];