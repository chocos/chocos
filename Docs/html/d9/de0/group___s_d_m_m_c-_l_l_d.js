var group___s_d_m_m_c__l_l_d =
[
    [ "oC_SDMMC_LLD_Command_t", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html", [
      [ "Argument", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html#afa6635c2ff94756b33097676d91356a4", null ],
      [ "CommandIndex", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html#ad296a2684a1375527319b91e8daa06e9", null ],
      [ "ResponseType", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html#ac79d1de78d3758098f5c1b680fadf2c9", null ],
      [ "SuspendCommand", "d1/d3a/structo_c___s_d_m_m_c___l_l_d___command__t.html#a7102ee6c7cfbd08a83ffad3de56fb61c", null ]
    ] ],
    [ "oC_SDMMC_LLD_CommandResponse_t", "dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t.html", [
      [ "CommandIndex", "dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t.html#ad296a2684a1375527319b91e8daa06e9", null ],
      [ "Response", "dd/d09/structo_c___s_d_m_m_c___l_l_d___command_response__t.html#adfed6f4c382dd4a5ef190dd0806efa35", null ]
    ] ],
    [ "oC_SDMMC_LLD_Config_t", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t.html", [
      [ "Channel", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t.html#af40238f432cf7cca88f956542a453711", null ],
      [ "SectorSize", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t.html#a8fd1d60aa3e4d98c596e7476ab66a752", null ],
      [ "WideBus", "d7/da6/structo_c___s_d_m_m_c___l_l_d___config__t.html#a838c947e78092e7fe7c11817e3d8c6f6", null ]
    ] ],
    [ "oC_SDMMC_LLD_TransferConfig_t", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html", [
      [ "BlockSize", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a688d15af937b4b2f16b9da1fd8c2d375", null ],
      [ "DataSize", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#ad7d69b4601cad602752c6cd180642cfb", null ],
      [ "Timeout", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#ad43f6298d4ab7f5e730d7c4672b2a08f", null ],
      [ "TransferDirection", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a6802550f645df326068f2787486b30f7", null ],
      [ "TransferMode", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a4709d7049bd1e1549a8f906147195f53", null ],
      [ "UseDma", "d0/de8/structo_c___s_d_m_m_c___l_l_d___transfer_config__t.html#a28b10aaf3cbac859c245b08db53ed69d", null ]
    ] ],
    [ "oC_SDMMC_Channel_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gaef37a5af5c01184f3adbbfce1c3f5677", null ],
    [ "oC_SDMMC_LLD_ChannelIndex_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga0f61d857741a4396cb35013b7497083f", null ],
    [ "oC_SDMMC_LLD_CommandIndex_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gad5fc9b83a0feb8e327382a4d2b65921a", null ],
    [ "oC_SDMMC_LLD_InterruptHandler_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga6cd425de79c3f95a0c85072b9f1b4680", null ],
    [ "oC_SDMMC_LLD_PinFunction_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gaee3f98434afd23a80538b0efb2d252f3", null ],
    [ "oC_SDMMC_Pin_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga068ffe4d8861b6ca79af25013264139f", null ],
    [ "oC_SDMMC_LLD_InterruptSource_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gaba148400053e6969c0d2acfbd82590d8", [
      [ "oC_SDMMC_LLD_InterruptSource_None", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8a82080d33965387566464410293a25f46", null ],
      [ "oC_SDMMC_LLD_InterruptSource_CommandSent", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8ad59442492457fc648373f6ec7d931a3c", null ],
      [ "oC_SDMMC_LLD_InterruptSource_CommandResponseReceived", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8add1797543406d1ca57b7c7a80e1750a9", null ],
      [ "oC_SDMMC_LLD_InterruptSource_DataReadyToReceive", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8aa7c4239fcda38587f64beab4c2bb9eb8", null ],
      [ "oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8afbe234b8e9f6187624ba0c07d762e47e", null ],
      [ "oC_SDMMC_LLD_InterruptSource_TransmissionError", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8abe7763b26874e63d66b551cb62b8b915", null ],
      [ "oC_SDMMC_LLD_InterruptSource_CommandTransmissionError", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8ad2cf8cb5da64cfc60f89d8fec504b0ce", null ],
      [ "oC_SDMMC_LLD_InterruptSource_TransmissionRestarted", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8a494184cc2dadc18fe664315e189b4fef", null ],
      [ "oC_SDMMC_LLD_InterruptSource_TransmissionFinished", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggaba148400053e6969c0d2acfbd82590d8a3011f8b8ad97dc8983255996df3584ef", null ]
    ] ],
    [ "oC_SDMMC_LLD_ResponseType_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga55a4df0ce8d62b5214ef388afcb28bb0", [
      [ "oC_SDMMC_LLD_ResponseType_None", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga55a4df0ce8d62b5214ef388afcb28bb0ab2d108a4340b8ed210ff4ca862eca5c1", null ],
      [ "oC_SDMMC_LLD_ResponseType_ShortResponse", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga55a4df0ce8d62b5214ef388afcb28bb0ac685abc02ddd980dc4b9f638de24c9ad", null ],
      [ "oC_SDMMC_LLD_ResponseType_LongResponse", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga55a4df0ce8d62b5214ef388afcb28bb0a1ad59dc3f13d78536f293eab880f3d62", null ],
      [ "oC_SDMMC_LLD_ResponseType_NumberOfElements", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga55a4df0ce8d62b5214ef388afcb28bb0a81f96d45fd65b5f4d9e0c746a8e45803", null ]
    ] ],
    [ "oC_SDMMC_LLD_TransferDirection_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga7e0cd9e57b737b13d7fb21012e64238e", [
      [ "oC_SDMMC_LLD_TransferDirection_Read", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga7e0cd9e57b737b13d7fb21012e64238eaa4abeee4b76df9a41f0d9dbb4983505b", null ],
      [ "oC_SDMMC_LLD_TransferDirection_Write", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga7e0cd9e57b737b13d7fb21012e64238ea375f27dffb3d9e52d2d7775285a81792", null ]
    ] ],
    [ "oC_SDMMC_LLD_TransferMode_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga221103007635badc9b28e4e436a7836f", [
      [ "oC_SDMMC_LLD_TransferMode_BlockDataTransfer", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga221103007635badc9b28e4e436a7836fa3e005700ef34b80cb4fc694dc7f47940", null ],
      [ "oC_SDMMC_LLD_TransferMode_Stream", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga221103007635badc9b28e4e436a7836fa7a5655db4677d36b8cd96b478b203332", null ],
      [ "oC_SDMMC_LLD_TransferMode_NumberOfElements", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gga221103007635badc9b28e4e436a7836fa31ba18f11e85805a00046e752d410192", null ]
    ] ],
    [ "oC_SDMMC_LLD_WideBus_t", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gab18c326fc8ee3076ba72b571f9015a27", [
      [ "oC_SDMMC_LLD_WideBus_1Bit", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggab18c326fc8ee3076ba72b571f9015a27ac8b7e2cb0e47e2c91559afd07356b820", null ],
      [ "oC_SDMMC_LLD_WideBus_4Bit", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggab18c326fc8ee3076ba72b571f9015a27a99d8f52aecbb2efdb4bf53f2a77c7f15", null ],
      [ "oC_SDMMC_LLD_WideBus_8Bit", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggab18c326fc8ee3076ba72b571f9015a27a61990e34e24a495c4e2a0c82fdbb6a55", null ],
      [ "oC_SDMMC_LLD_WideBus_NumberOfElements", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ggab18c326fc8ee3076ba72b571f9015a27a8d2312b2b9e999dece1031a6d0565776", null ]
    ] ],
    [ "oC_SDMMC_LLD_ChannelIndexToChannel", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gac06a39ed5e3c4cf1e1e59aeee0f7bdd1", null ],
    [ "oC_SDMMC_LLD_ChannelToChannelIndex", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga8e5e84ff190356fe3bb39c0defc8c39a", null ],
    [ "oC_SDMMC_LLD_Configure", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga6569bf9d5084d356279e09b676fd52e4", null ],
    [ "oC_SDMMC_LLD_ConfigurePin", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga6f7b746d91379b5ddf8373d6adf1efe6", null ],
    [ "oC_SDMMC_LLD_GetChannelOfModulePin", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga52a0f7192fd253572a7939632022c1b5", null ],
    [ "oC_SDMMC_LLD_IsChannelCorrect", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga5e90ce7d9dda78a561d5a48137dbc92a", null ],
    [ "oC_SDMMC_LLD_IsChannelIndexCorrect", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga41d7cc4bcd1fe38a949330eb920f89fd", null ],
    [ "oC_SDMMC_LLD_IsPinAvailable", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gab7764f29ed27d49a3fbc5da877dd12ca", null ],
    [ "oC_SDMMC_LLD_PrepareForTransfer", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga7b60b2dc5a558da29c9baf2e2850b97d", null ],
    [ "oC_SDMMC_LLD_ReadCommandResponse", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga744e93c017099b7b569c835503382ce6", null ],
    [ "oC_SDMMC_LLD_ReadDataPackage", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gadd30836f53656c4ff79938b393794002", null ],
    [ "oC_SDMMC_LLD_ReadPower", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga0af67e4e5bd310774ced9d2a46cc4374", null ],
    [ "oC_SDMMC_LLD_SendCommand", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga6cc1328d50cd103638226217cae3ebaa", null ],
    [ "oC_SDMMC_LLD_SetInterruptHandler", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga2a7faedddfc581e6041bc0d5d675ed45", null ],
    [ "oC_SDMMC_LLD_SetPower", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gac0b98273b864b94983bab9d394cd96ab", null ],
    [ "oC_SDMMC_LLD_SetWideBus", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gaa47e3ce7b54ec885f76ca80acb2a73d4", null ],
    [ "oC_SDMMC_LLD_TurnOffDriver", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga5ae3da69b2b57d25b054a77c26dce880", null ],
    [ "oC_SDMMC_LLD_TurnOnDriver", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gac6577841fe9ab0469114e6a3f8a003f0", null ],
    [ "oC_SDMMC_LLD_Unconfigure", "d9/de0/group___s_d_m_m_c-_l_l_d.html#gaa0bb5c20fdd3acb9a398aa97db838c73", null ],
    [ "oC_SDMMC_LLD_UnconfigurePin", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga789904a3b1c96463d9d704eee238d84f", null ],
    [ "oC_SDMMC_LLD_WriteDataPackage", "d9/de0/group___s_d_m_m_c-_l_l_d.html#ga7069e487c82ce551fe38a1cd7219d768", null ]
];