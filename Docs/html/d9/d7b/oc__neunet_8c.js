var oc__neunet_8c =
[
    [ "Context_t", "d7/d83/struct_context__t.html", "d7/d83/struct_context__t" ],
    [ "IsContextCorrect", "d9/d7b/oc__neunet_8c.html#ad33bc5ec7011772381cd033513536f63", null ],
    [ "oC_NEUNET_Configure", "d9/d7b/oc__neunet_8c.html#ac5103de69a38de4216b0553c1b0e697a", null ],
    [ "oC_NEUNET_Ioctl", "d9/d7b/oc__neunet_8c.html#a29a5f8695c15f4e63193bdc3da8d438d", null ],
    [ "oC_NEUNET_IsTurnedOn", "d9/d7b/oc__neunet_8c.html#a3a91740f316b094acc85826e827a52b1", null ],
    [ "oC_NEUNET_Read", "d9/d7b/oc__neunet_8c.html#a210f9ecda4a98dd99ce1476e45c04b06", null ],
    [ "oC_NEUNET_TurnOff", "d9/d7b/oc__neunet_8c.html#a73cc74ba6c08fbd15ba0bec856b4a6b8", null ],
    [ "oC_NEUNET_TurnOn", "d9/d7b/oc__neunet_8c.html#a1098bba71c07c7f3e4671327bcd00e24", null ],
    [ "oC_NEUNET_Unconfigure", "d9/d7b/oc__neunet_8c.html#abd790227271560c795facfbe96db83a8", null ],
    [ "oC_NEUNET_Write", "d9/d7b/oc__neunet_8c.html#a6602d44c3250482736d737ed39523f3b", null ],
    [ "Allocator", "d9/d7b/oc__neunet_8c.html#a35e1e2c2e113243f4ff2437fcc071643", null ]
];