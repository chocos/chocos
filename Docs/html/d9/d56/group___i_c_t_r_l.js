var group___i_c_t_r_l =
[
    [ "oC_ICtrl_t", "d9/d56/group___i_c_t_r_l.html#ga8fd6f518b35da5a06ff0e68c5f43b57c", null ],
    [ "oC_ICtrl_Delete", "d9/d56/group___i_c_t_r_l.html#gaae940a4145431fd329be7739e62c0b96", null ],
    [ "oC_ICtrl_GetDriver", "d9/d56/group___i_c_t_r_l.html#ga18fbd3145fbb7fba7916960b870db372", null ],
    [ "oC_ICtrl_GetName", "d9/d56/group___i_c_t_r_l.html#ga2a73fc05c9fe5e58790dfc2e77e35a38", null ],
    [ "oC_ICtrl_GetScreen", "d9/d56/group___i_c_t_r_l.html#gafcf4a175e55bde932ae4c666044e1f91", null ],
    [ "oC_ICtrl_IsConfigured", "d9/d56/group___i_c_t_r_l.html#gab3313f0f8d36338a22d36f058bdf0f2f", null ],
    [ "oC_ICtrl_IsCorrect", "d9/d56/group___i_c_t_r_l.html#ga185a6e29074b4956c272afd6166cc1c2", null ],
    [ "oC_ICtrl_IsEventSupportedByDriver", "d9/d56/group___i_c_t_r_l.html#ga3dcb991acdb6d17707402ce6bb00c6a1", null ],
    [ "oC_ICtrl_New", "d9/d56/group___i_c_t_r_l.html#gab855d97ae17c2957be46786367fe186b", null ],
    [ "oC_ICtrl_RecordGesture", "d9/d56/group___i_c_t_r_l.html#gaa49f9a2d336ff7f854c12ee0edc82631", null ],
    [ "oC_ICtrl_SetScreen", "d9/d56/group___i_c_t_r_l.html#gaea6ee621ae038c749db91dfdfe14490a", null ],
    [ "oC_ICtrl_WaitForEvent", "d9/d56/group___i_c_t_r_l.html#ga28f1b3fa3999dac8cb053cc657d33856", null ]
];