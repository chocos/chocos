var oc__portman_8h =
[
    [ "oC_PortMan_Port_t", "d2/d9a/group___port_man.html#gab8d0a52cb535d6016098276d14aff601", null ],
    [ "oC_PortMan_IsPortReserved", "d2/d9a/group___port_man.html#gad8af999b91d3b11fd57c33cc264c1ec2", null ],
    [ "oC_PortMan_IsPortReservedBy", "d2/d9a/group___port_man.html#ga552d3073c3fc1a444ae1555abad91b90", null ],
    [ "oC_PortMan_RegisterModule", "d2/d9a/group___port_man.html#ga93287dded812a729798ce8ae140697cc", null ],
    [ "oC_PortMan_ReleaseAllPortsOf", "d2/d9a/group___port_man.html#ga48fc59b265694b924c6094b3dbfc16bd", null ],
    [ "oC_PortMan_ReleasePort", "d2/d9a/group___port_man.html#ga0096c83cc638a2380db6c4d55096349b", null ],
    [ "oC_PortMan_ReservePort", "d2/d9a/group___port_man.html#gaf04e0e51a74be74761d65c744c50bf90", null ],
    [ "oC_PortMan_TurnOff", "d2/d9a/group___port_man.html#ga812e759434c7d9d2feb07f1e9b3fa95c", null ],
    [ "oC_PortMan_TurnOn", "d2/d9a/group___port_man.html#ga9f5785bbf7423e38cdd942430ae08d19", null ],
    [ "oC_PortMan_UnregisterModule", "d2/d9a/group___port_man.html#ga0f93e3b22ad52ab5fa609f732760b6ed", null ]
];