var oc__eth_8h =
[
    [ "oC_ETH_CHIP_NAME_MAX_LENGTH", "d3/d01/group___e_t_h.html#gae02baa3a7e6a8c5dd5b186c8299087fd", null ],
    [ "oC_ETH_ChipName_t", "d3/d01/group___e_t_h.html#gaafcad64ecbf7803c6ef3c1a872f50a36", null ],
    [ "oC_ETH_Context_t", "d3/d01/group___e_t_h.html#ga477c747d251b0fe6b250a0ce86fd2f37", null ],
    [ "oC_ETH_PhyAddress_t", "d3/d01/group___e_t_h.html#ga43d16b969e22e0d82d0ea55133a84789", null ],
    [ "oC_ETH_Configure", "d3/d01/group___e_t_h.html#ga3d2ea7a5e5de1e267b71b53a226ebecf", null ],
    [ "oC_ETH_IsContextCorrect", "d3/d01/group___e_t_h.html#ga405cf6fc4d2ffe7fef0faa3892fdb37d", null ],
    [ "oC_ETH_IsTurnedOn", "d3/d01/group___e_t_h.html#ga8c30e6718ee79e932bf0d1ddc0dc79c9", null ],
    [ "oC_ETH_PerformDiagnostics", "d3/d01/group___e_t_h.html#ga815a2bef52a08324e75abf36e7421b31", null ],
    [ "oC_ETH_ReadNetInfo", "d3/d01/group___e_t_h.html#gafa1317808413d07ff7d44df4ae6ac097", null ],
    [ "oC_ETH_ReceiveFrame", "d3/d01/group___e_t_h.html#ga34778e13e315ddc849c6c550b55c2893", null ],
    [ "oC_ETH_SendFrame", "d3/d01/group___e_t_h.html#ga52c6923201e55ca6ce3c1ff3554dcf8c", null ],
    [ "oC_ETH_SetLoopback", "d3/d01/group___e_t_h.html#gaba683b00fbb4bfa507131f644c4d091c", null ],
    [ "oC_ETH_TurnOff", "d3/d01/group___e_t_h.html#gae1b60fcc4ad3f957c386b10a00e223cc", null ],
    [ "oC_ETH_TurnOn", "d3/d01/group___e_t_h.html#gabcbb43aa1a5dd47b5589a6e1ab907c43", null ],
    [ "oC_ETH_Unconfigure", "d3/d01/group___e_t_h.html#ga143882998e26d8468b82eb972f06b78e", null ]
];