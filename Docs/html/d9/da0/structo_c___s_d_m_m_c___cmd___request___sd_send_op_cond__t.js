var structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t =
[
    [ "HighCapacitySupported", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#ada3c5de331f28cc7c61bd8bb1ce72fd5", null ],
    [ "Max", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#aba003a72f79d270487506fde99033973", null ],
    [ "MaximumPerformanceEnabled", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#ac519ec742405d040eea11dacaeb5957d", null ],
    [ "Min", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#a74914587c1e6b3f395c7c97eaeea369c", null ],
    [ "SwitchToLowVoltage", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#a0facd937ff3c9844de007f15ea2f2830", null ],
    [ "VoltageRange", "d9/da0/structo_c___s_d_m_m_c___cmd___request___sd_send_op_cond__t.html#a7894b5c4dfce6e560caae0d7481e574e", null ]
];