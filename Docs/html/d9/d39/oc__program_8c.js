var oc__program_8c =
[
    [ "ProgramContext_t", "db/df5/struct_program_context__t.html", null ],
    [ "CloseProcess", "d9/d39/oc__program_8c.html#a6425cfe6a83927c47fae7356f9469f4e", null ],
    [ "oC_Program_Delete", "d0/d42/group___program.html#gaeed40c1f5f8be79ada16f79d3e790652", null ],
    [ "oC_Program_Exec", "d0/d42/group___program.html#ga8c71300201590b4cd0704f6487b94dab", null ],
    [ "oC_Program_Exec2", "d0/d42/group___program.html#gadb644c9fa5c2992a06cf1b85ea1b59ad", null ],
    [ "oC_Program_GetHeapMapSize", "d0/d42/group___program.html#ga4aa880163838337801cb268f1b50d3c6", null ],
    [ "oC_Program_GetName", "d0/d42/group___program.html#ga2b22a1f2a9264a6988d24c1801cbf391", null ],
    [ "oC_Program_GetPriority", "d0/d42/group___program.html#ga7db05ed79751d41c7fb2d0eec085ee47", null ],
    [ "oC_Program_GetStdErrName", "d0/d42/group___program.html#ga7b7e5952b4b2a54cecd02362452aff82", null ],
    [ "oC_Program_GetStdInName", "d0/d42/group___program.html#ga767ffd5ccd8455b199494060cde77fab", null ],
    [ "oC_Program_GetStdOutName", "d0/d42/group___program.html#gabce832012530bdeb0b8bd1a19ec26685", null ],
    [ "oC_Program_IsCorrect", "d0/d42/group___program.html#ga21b45383d98893c6189f10299c52b9bd", null ],
    [ "oC_Program_New", "d0/d42/group___program.html#gadfda878e05a260f3c5f8dde3da1d1401", null ],
    [ "oC_Program_NewProcess", "d0/d42/group___program.html#ga0b5ed7605c7e6665460cae18d56f4280", null ],
    [ "oC_Program_SetGotAddress", "d0/d42/group___program.html#gabba4a14ec7c4c0ae89c501245b80a51c", null ],
    [ "oC_Program_WaitForFinish", "d0/d42/group___program.html#gafd42ab7ed01e219a27ee196e6ad3faf7", null ],
    [ "ProgramThread", "d9/d39/oc__program_8c.html#ac6d8f768728f5c5b6eec3d14eb45744d", null ]
];