var oc__uart_8h =
[
    [ "oC_UART_Context_t", "d2/d48/group___u_a_r_t.html#ga5b79ba8f8187fc8d3ba82d6189b02a60", null ],
    [ "oC_UART_BitOrder_t", "d2/d48/group___u_a_r_t.html#ga2a596276954f81d735636035d89aa445", [
      [ "oC_UART_BitOrder_LSBFirst", "d2/d48/group___u_a_r_t.html#gga2a596276954f81d735636035d89aa445aacb5990843a57c8785c1b98fcd783275", null ],
      [ "oC_UART_BitOrder_MSBFirst", "d2/d48/group___u_a_r_t.html#gga2a596276954f81d735636035d89aa445ae692a96701532106c7d9bb59f304fd87", null ]
    ] ],
    [ "oC_UART_Dma_t", "d2/d48/group___u_a_r_t.html#ga3549c3168708c4c41cec3c91add66769", [
      [ "oC_UART_Dma_UseIfPossible", "d2/d48/group___u_a_r_t.html#gga3549c3168708c4c41cec3c91add66769a9ddfad2ff51b72c290cb69946b1cebb9", null ],
      [ "oC_UART_Dma_AlwaysUse", "d2/d48/group___u_a_r_t.html#gga3549c3168708c4c41cec3c91add66769a108be07d2bdebed626fa737dc0751e1d", null ],
      [ "oC_UART_Dma_DontUse", "d2/d48/group___u_a_r_t.html#gga3549c3168708c4c41cec3c91add66769af589f73cf6081b5da72bed8fc7857c89", null ]
    ] ],
    [ "oC_UART_Invert_t", "d2/d48/group___u_a_r_t.html#gad81973fc84cdda74fbd2307a25c09691", [
      [ "oC_UART_Invert_NotInverted", "d2/d48/group___u_a_r_t.html#ggad81973fc84cdda74fbd2307a25c09691a4cabdf436ed52c6975f61a79e03e5f83", null ],
      [ "oC_UART_Invert_Inverted", "d2/d48/group___u_a_r_t.html#ggad81973fc84cdda74fbd2307a25c09691a12918ed018497fd255538773c25637ad", null ]
    ] ],
    [ "oC_UART_Mode_t", "d2/d48/group___u_a_r_t.html#gad4ed5ab6187a44d675d4fb564d4163c6", [
      [ "oC_UART_Mode_Both", "d2/d48/group___u_a_r_t.html#ggad4ed5ab6187a44d675d4fb564d4163c6a85557b7c4fe89a6ab4959dd52bb01684", null ],
      [ "oC_UART_Mode_Output", "d2/d48/group___u_a_r_t.html#ggad4ed5ab6187a44d675d4fb564d4163c6ac7c586dc99cbaca0e6a5c49f9b47f7a4", null ],
      [ "oC_UART_Mode_Input", "d2/d48/group___u_a_r_t.html#ggad4ed5ab6187a44d675d4fb564d4163c6aeaa6dd11b54b3a712cfc36962a3bb13a", null ]
    ] ],
    [ "oC_UART_Parity_t", "d2/d48/group___u_a_r_t.html#gabdbd58c3d79fa4c53c5713aa2d5dc6c0", [
      [ "oC_UART_Parity_None", "d2/d48/group___u_a_r_t.html#ggabdbd58c3d79fa4c53c5713aa2d5dc6c0a014714d63112e398ab32fbf1074243d5", null ],
      [ "oC_UART_Parity_Odd", "d2/d48/group___u_a_r_t.html#ggabdbd58c3d79fa4c53c5713aa2d5dc6c0a37ccb05aeca9726e410513cc38916cc2", null ],
      [ "oC_UART_Parity_Even", "d2/d48/group___u_a_r_t.html#ggabdbd58c3d79fa4c53c5713aa2d5dc6c0a45df88334ebb5567072e826e4ea5aeeb", null ]
    ] ],
    [ "oC_UART_StopBit_t", "d2/d48/group___u_a_r_t.html#gabd0b161db3c467a195d68f16e427c63b", [
      [ "oC_UART_StopBit_1Bit", "d2/d48/group___u_a_r_t.html#ggabd0b161db3c467a195d68f16e427c63ba016d9b92177d6ce4eb7d63a0f55b832e", null ],
      [ "oC_UART_StopBit_1p5Bits", "d2/d48/group___u_a_r_t.html#ggabd0b161db3c467a195d68f16e427c63ba540a9e94d79783dfca6687509380204b", null ],
      [ "oC_UART_StopBit_2Bits", "d2/d48/group___u_a_r_t.html#ggabd0b161db3c467a195d68f16e427c63ba249e892109f2827bd9809cd11d118170", null ]
    ] ],
    [ "oC_UART_WordLength_t", "d2/d48/group___u_a_r_t.html#gadab6ad4741e9212ed57be7419afb5d75", [
      [ "oC_UART_WordLength_5Bits", "d2/d48/group___u_a_r_t.html#ggadab6ad4741e9212ed57be7419afb5d75a379c8e5b4cc64f65d26517757102564d", null ],
      [ "oC_UART_WordLength_6Bits", "d2/d48/group___u_a_r_t.html#ggadab6ad4741e9212ed57be7419afb5d75aa470b428ea47106a86c9c08a20aa04a3", null ],
      [ "oC_UART_WordLength_7Bits", "d2/d48/group___u_a_r_t.html#ggadab6ad4741e9212ed57be7419afb5d75a746b78877afadee83576d240b82845f4", null ],
      [ "oC_UART_WordLength_8Bits", "d2/d48/group___u_a_r_t.html#ggadab6ad4741e9212ed57be7419afb5d75a6d58ff36be27f28c59340f3103a3245f", null ]
    ] ],
    [ "oC_UART_Configure", "d2/d48/group___u_a_r_t.html#gae3630ee555f64b6f63cb726d3d0e55ae", null ],
    [ "oC_UART_Disable", "d2/d48/group___u_a_r_t.html#gadc4a0170684dac2c1c6d7aff5708eb24", null ],
    [ "oC_UART_Enable", "d2/d48/group___u_a_r_t.html#gaae92f84acdd16328f48bf52725995e13", null ],
    [ "oC_UART_Ioctl", "d2/d48/group___u_a_r_t.html#gaa2c8dd68140a94666e8949ae2223ed41", null ],
    [ "oC_UART_IsTurnedOn", "d2/d48/group___u_a_r_t.html#ga34a9a20197d5d1465eb109948a6b816d", null ],
    [ "oC_UART_Read", "d2/d48/group___u_a_r_t.html#ga2110eb0b700e73bd3c3abe578a388c6d", null ],
    [ "oC_UART_Receive", "d2/d48/group___u_a_r_t.html#ga69278629366cbd7dab5e037f9106402b", null ],
    [ "oC_UART_TurnOff", "d2/d48/group___u_a_r_t.html#ga5a6002e7278ad6b32f9c2bea40d9dfe1", null ],
    [ "oC_UART_TurnOn", "d2/d48/group___u_a_r_t.html#ga3e7fd6b89ff528216fe0638233d7c214", null ],
    [ "oC_UART_Unconfigure", "d2/d48/group___u_a_r_t.html#ga51540899b82d61ff372ada3716938976", null ],
    [ "oC_UART_Write", "d2/d48/group___u_a_r_t.html#gabefeced6608dd8ad937e9ccdb4afde28", null ]
];