var searchData=
[
  ['g0fontheight_8322',['G0FontHeight',['../d7/d83/struct_context__t.html#a40b9d9ba6da3d347e6d64ff15691e392',1,'Context_t']]],
  ['g0fontwidth_8323',['G0FontWidth',['../d7/d83/struct_context__t.html#a5f36e01b81ecdd2d36a12b907fae86f3',1,'Context_t']]],
  ['g1fontheight_8324',['G1FontHeight',['../d7/d83/struct_context__t.html#aec224cc35cca16d5411621065615fda0',1,'Context_t']]],
  ['g1fontwidth_8325',['G1FontWidth',['../d7/d83/struct_context__t.html#a1a7ce141b4132b4ca9b9c97979f1bc7a',1,'Context_t']]],
  ['gatewayip_8326',['GatewayIP',['../da/d29/structo_c___net___ipv4_info__t.html#a543e6d53ebfc372d262be065f0879b90',1,'oC_Net_Ipv4Info_t']]],
  ['generic_8327',['Generic',['../d6/d84/structo_c___s_d_m_m_c___cmd___command_data__t.html#ab853fbf1e9c0207b59009992a8fd080a',1,'oC_SDMMC_Cmd_CommandData_t']]],
  ['genericwritemap_8328',['GenericWriteMap',['../d1/da4/structo_c___color_map__t.html#a5c4f0c04939357f2c5a1adba1f6a4378',1,'oC_ColorMap_t']]],
  ['getcursorposition_8329',['GetCursorPosition',['../dc/d54/structo_c___terminal___if_cfg__t.html#af4b6a07095e2483053b896e839eb5fa4',1,'oC_Terminal_IfCfg_t']]],
  ['getoffset_8330',['GetOffset',['../d8/de7/structo_c___data_package__t.html#abcaa0d2b521f0ae4b480748bff77928c',1,'oC_DataPackage_t']]],
  ['giaddr_8331',['GIAddr',['../df/d6b/struct_p_a_c_k_e_d.html#a8fb9b675fde1bb44703cc3fa3ae8740e',1,'PACKED']]],
  ['globalfindnextstackhandler_8332',['GlobalFindNextStackHandler',['../dc/d7b/_cortex___m4_2oc__mcs_8c.html#a602d54ef4d8c4b69a088b1e3f5a3f80a',1,'GlobalFindNextStackHandler():&#160;oc_mcs.c'],['../d4/ddc/_cortex___m7_2oc__mcs_8c.html#a602d54ef4d8c4b69a088b1e3f5a3f80a',1,'GlobalFindNextStackHandler():&#160;oc_mcs.c']]]
];
