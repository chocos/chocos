var searchData=
[
  ['waitforacknowledge_8080',['WaitForAcknowledge',['../dc/d18/oc__tcp__connection_8c.html#a64eb33c0ef6d29cf00e9b1631b4b734f',1,'oc_tcp_connection.c']]],
  ['waitforcarddetection_8081',['WaitForCardDetection',['../d4/d94/oc__sdmmc_8c.html#a17a4312d42ee0e7e0b2cc6de756f8bae',1,'oc_sdmmc.c']]],
  ['waitfordiskejectthread_8082',['WaitForDiskEjectThread',['../d4/dae/oc__diskman_8c.html#ab70114fa6384984aa75d53f3a9dd446c',1,'oc_diskman.c']]],
  ['waitforfreeportrelease_8083',['WaitForFreePortRelease',['../d4/d3d/oc__portman_8c.html#a5fd1928db65d06b0c7a0bb1c3525952f',1,'oc_portman.c']]],
  ['waitformemory_8084',['WaitForMemory',['../dd/d7c/oc__memman_8c.html#a215dff35b0996e78f2362916cc764c41',1,'oc_memman.c']]],
  ['waitfornewdiskthread_8085',['WaitForNewDiskThread',['../d4/dae/oc__diskman_8c.html#a4a6d79f8871671a8d6da806311a4ec35',1,'oc_diskman.c']]],
  ['waitforportrelease_8086',['WaitForPortRelease',['../d4/d3d/oc__portman_8c.html#a98acb3c1491eac04a533017d0516c861',1,'oc_portman.c']]],
  ['waitforpredefinedpacket_8087',['WaitForPredefinedPacket',['../dc/d18/oc__tcp__connection_8c.html#ad26efede645f371a5d551d2bf9a64b0d',1,'oc_tcp_connection.c']]],
  ['waitfortouch_8088',['WaitForTouch',['../d6/d6a/oc__ft5336_8c.html#adbf5704b3f82a054eaa4049e47a28499',1,'oc_ft5336.c']]],
  ['write_8089',['Write',['../d2/da2/oc__telnet_8c.html#a24698ce3bd8aeb97771894125775c4ca',1,'oc_telnet.c']]],
  ['writedata_8090',['WriteData',['../d7/d32/oc__fmc_8c.html#adff75d6e94f3ea056bcb9f2c28bbf802',1,'WriteData(oC_FMC_Context_t Context, char Data):&#160;oc_fmc.c'],['../d5/d60/group___s_d_m_m_c-_l_l_d-_mode.html#gaa85e507b83ca41ae25a3f394001400cf',1,'WriteData(oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t *CardInfo, oC_SectorNumber_t StartSector, const char *Buffer, oC_MemorySize_t *Size, oC_Time_t Timeout):&#160;oc_sdmmc_mode_lld.c']]],
  ['writeregister_8091',['WriteRegister',['../dc/d36/oc__eth__phy_8c.html#a06d2df3d6d68361430b9e8e62ff09262',1,'WriteRegister(oC_ETH_PhyAddress_t PhyAddress, oC_ETH_PhyRegister_t Register, uint32_t Value):&#160;oc_eth_phy.c'],['../d6/d6a/oc__ft5336_8c.html#a8c5352435f1a9044489c26030caf1bd0',1,'WriteRegister(oC_FT5336_Context_t Context, oC_I2C_Register_t Offset, uint8_t Value, oC_Time_t Timeout):&#160;oc_ft5336.c']]],
  ['writesectorsbydriver_8092',['WriteSectorsByDriver',['../d4/dae/oc__diskman_8c.html#a82823f655d4aa220ddeee1ee47991874',1,'oc_diskman.c']]]
];
