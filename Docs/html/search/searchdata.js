var indexSectionsWithContent =
{
  0: "(1abcdefghijklmnopqrstuvwxyz",
  1: "abcdefgilmnopqrstuvw",
  2: "o",
  3: "abcdefghijlmnoprstuvw",
  4: "abcdefghijlmnopqrstuvwxyz",
  5: "bcdfhorst",
  6: "abcdhinot",
  7: "abcdhinot",
  8: "acdfglmnos",
  9: "(1abcdefghiklmnopqrstuvw",
  10: "acdlptu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

