var searchData=
[
  ['transfermode_5falternatememoryscattergather_11005',['TransferMode_AlternateMemoryScatterGather',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca0417125f81647142017908df55f52358',1,'oc_dma_lld.c']]],
  ['transfermode_5falternateperipheralscattergather_11006',['TransferMode_AlternatePeripheralScatterGather',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca6e9cb22607cdc0026708d9eb84a4ec38',1,'oc_dma_lld.c']]],
  ['transfermode_5fautorequest_11007',['TransferMode_AutoRequest',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca1aa317f06e7af3523acaf8362446a980',1,'oc_dma_lld.c']]],
  ['transfermode_5fbasic_11008',['TransferMode_Basic',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca1ec2e404758d4bcae54e725d37569645',1,'oc_dma_lld.c']]],
  ['transfermode_5fmemoryscattergather_11009',['TransferMode_MemoryScatterGather',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca05f3fdf17cd792145cc0bcb1f6e1e956',1,'oc_dma_lld.c']]],
  ['transfermode_5fperipheralscattergather_11010',['TransferMode_PeripheralScatterGather',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca6286053502807ec0be607ced2052945c',1,'oc_dma_lld.c']]],
  ['transfermode_5fpingpong_11011',['TransferMode_PingPong',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca0b80a731fb781d9d6319d88728f63c83',1,'oc_dma_lld.c']]],
  ['transfermode_5fstop_11012',['TransferMode_Stop',['../df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca87619834caebd9da61fc6659ce15ea33',1,'oc_dma_lld.c']]]
];
