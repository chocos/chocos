var structo_c___tcp___connection___config__t =
[
    [ "Callbacks", "df/da2/structo_c___tcp___connection___config__t.html#ad06ce5d107587cacd0ade033b9dbf75c", null ],
    [ "ConfirmationTimeout", "df/da2/structo_c___tcp___connection___config__t.html#ab1c2024065055bf34217352209ac2884", null ],
    [ "ExpirationTimeout", "df/da2/structo_c___tcp___connection___config__t.html#ab0e005a2803b0a3d1c9b97e4c1c33492", null ],
    [ "InitialAcknowledgeNumber", "df/da2/structo_c___tcp___connection___config__t.html#a0d1ee50ae9e0471e69f920da1adc2157", null ],
    [ "InitialSequenceNumber", "df/da2/structo_c___tcp___connection___config__t.html#aacf875a8a00500eaf0b74415c0f15d0c", null ],
    [ "LocalAddress", "df/da2/structo_c___tcp___connection___config__t.html#ac0bd6b38b297eda73334cf9952a7ff94", null ],
    [ "LocalWindowScale", "df/da2/structo_c___tcp___connection___config__t.html#a5f7d33eb6e1021f05dd8a8cdf1cdb162", null ],
    [ "LocalWindowSize", "df/da2/structo_c___tcp___connection___config__t.html#a94d19de96228b85e2d46930ce508a781", null ],
    [ "PacketSize", "df/da2/structo_c___tcp___connection___config__t.html#a507b4d396fba7e9f258fed9ed9d2d3a8", null ],
    [ "ReadSegmentTimeout", "df/da2/structo_c___tcp___connection___config__t.html#a06ce596001150fabbbb6397df26bb22e", null ],
    [ "ReceiveTimeout", "df/da2/structo_c___tcp___connection___config__t.html#a8752a0ea69dfb5124b620f7af2b1b69a", null ],
    [ "RemoteAddress", "df/da2/structo_c___tcp___connection___config__t.html#a9ad31da5e05a9a15c8b4d82242ec40ee", null ],
    [ "ResendSegmentTime", "df/da2/structo_c___tcp___connection___config__t.html#ab5288ed475d8155acc43176b084c9d6d", null ],
    [ "SaveSegmentTimeout", "df/da2/structo_c___tcp___connection___config__t.html#afb17ffe3bc9ca8fe127c3018f1baa166", null ],
    [ "SendingAcknowledgeTimeout", "df/da2/structo_c___tcp___connection___config__t.html#abddb1152cff791fb463bcf9e16178550", null ]
];