var group___font =
[
    [ "Predefined Fonts", "d5/d29/group___predefined_fonts.html", "d5/d29/group___predefined_fonts" ],
    [ "oC_Font_CharacterInfo_t", "d8/d57/structo_c___font___character_info__t.html", [
      [ "Offset", "d8/d57/structo_c___font___character_info__t.html#ae8ff923acec242f835dc4340649b18a8", null ],
      [ "WidthBits", "d8/d57/structo_c___font___character_info__t.html#a2e8fb00c67a391b97c57f7343e74774e", null ]
    ] ],
    [ "oC_FontInfo_t", "d3/d69/structo_c___font_info__t.html", [
      [ "CharInfo", "d3/d69/structo_c___font_info__t.html#af77b772db7b49b029f8a5ee000cd487b", null ],
      [ "Data", "d3/d69/structo_c___font_info__t.html#a0a73759b305e9e2ed8c26b4d2bd419db", null ],
      [ "EndChar", "d3/d69/structo_c___font_info__t.html#a08406dcba65642424d87dab7259c267d", null ],
      [ "FontName", "d3/d69/structo_c___font_info__t.html#a817654268417445ea73bbd4986e09531", null ],
      [ "HeightPages", "d3/d69/structo_c___font_info__t.html#ac98357d15a452d1150d50cb1a185aa88", null ],
      [ "StartChar", "d3/d69/structo_c___font_info__t.html#a1451d917c45bae74f4241cfed25e7703", null ]
    ] ],
    [ "oC_Font_CharacterMap_t", "d3/d38/structo_c___font___character_map__t.html", null ],
    [ "oC_Font_", "df/d04/group___font.html#ga0d20b74fb0162062ac6fdd6fd3807e0a", null ],
    [ "oC_Font_IsPrintableCharacter", "df/d04/group___font.html#ga2d10184665ce5a9310c85a0fb8bed024", null ],
    [ "oC_Font_t", "df/d04/group___font.html#ga7f5971c51fa4687a3442a373c67bcca9", null ],
    [ "oC_Font_GetMaximumWidth", "df/d04/group___font.html#gaa304428f854197f3cdb576e25a19ba9e", null ],
    [ "oC_Font_ReadCharacterMap", "df/d04/group___font.html#gaaee2262d4210a43b27c644e2bbe06d29", null ],
    [ "oC_Font_ReadStringSize", "df/d04/group___font.html#gaad672e1ffb1f61703c8fe7bd2bc0175c", null ]
];