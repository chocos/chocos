var structo_c___widget_screen___screen_definition__t =
[
    [ "BackgroundColor", "df/d04/structo_c___widget_screen___screen_definition__t.html#a415c9124d7c95981acffc1a090d2dcc1", null ],
    [ "ColorFormat", "df/d04/structo_c___widget_screen___screen_definition__t.html#adb61686ba228c58e43d161e2348b7f44", null ],
    [ "Handler", "df/d04/structo_c___widget_screen___screen_definition__t.html#ace5b5e413519985d1688b0c2669406fd", null ],
    [ "Height", "df/d04/structo_c___widget_screen___screen_definition__t.html#a184d49528cffcfc8ec211807d1870537", null ],
    [ "NumberOfWidgetsDefinitions", "df/d04/structo_c___widget_screen___screen_definition__t.html#ade48711fc578e006d7cfc11c2a153743", null ],
    [ "Position", "df/d04/structo_c___widget_screen___screen_definition__t.html#a1c09e5e9db86c5d2cb89fc1c5df1aadf", null ],
    [ "PrepareHandler", "df/d04/structo_c___widget_screen___screen_definition__t.html#a3d92981b09a76276bfa9a019641f7730", null ],
    [ "WidgetsDefinitions", "df/d04/structo_c___widget_screen___screen_definition__t.html#a7933ae269b05b298b91f560e7154a9eb", null ],
    [ "Width", "df/d04/structo_c___widget_screen___screen_definition__t.html#a4ee55a170448d60b1f6fe43fca496673", null ]
];