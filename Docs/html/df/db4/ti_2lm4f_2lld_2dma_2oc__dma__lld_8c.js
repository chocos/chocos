var ti_2lm4f_2lld_2dma_2oc__dma__lld_8c =
[
    [ "DMACHCTL_t", "d9/d15/union_d_m_a_c_h_c_t_l__t.html", null ],
    [ "ControlTableEntry_t", "d7/d9f/struct_control_table_entry__t.html", null ],
    [ "ArbitrationSize_t", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951", [
      [ "ArbitrationSize_1Transfer", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951aa11711d679ded24465b25b577382f630", null ],
      [ "ArbitrationSize_2Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951ac401455dc604b0d79d7b9635741c1121", null ],
      [ "ArbitrationSize_4Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951abf77cab7093daaa495b6a3e4a7741405", null ],
      [ "ArbitrationSize_8Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951abd5a1a97325d860f891fcbfa55e3fa1d", null ],
      [ "ArbitrationSize_16Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951ac8d0af275c58751964343d0df24582fc", null ],
      [ "ArbitrationSize_32Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951a8cfa79ed82c8dba218e1c4d320132ce5", null ],
      [ "ArbitrationSize_64Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951ac78a366818812c646c02f4ffa7325737", null ],
      [ "ArbitrationSize_128Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951aa31374f9897cc5b5a70449cfbfcc0cb5", null ],
      [ "ArbitrationSize_256Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951a4db8e5196b07a6aa075e619e042b33d0", null ],
      [ "ArbitrationSize_512Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951a042e531e6db0ba1550e33fcbd8e21a9a", null ],
      [ "ArbitrationSize_1024Transfers", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a04b8cc6d3f65aa32825b39f3a9eee951abd7ed1a735e7175b82b61a07f32adf7c", null ]
    ] ],
    [ "DataSize_t", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a87ac71b6838461fc0d4266e1c28e8c20", [
      [ "DataSize_Byte", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a87ac71b6838461fc0d4266e1c28e8c20a6655e92e28bd2ca122d6ba95a09bc65e", null ],
      [ "DataSize_HalfWord", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a87ac71b6838461fc0d4266e1c28e8c20a64277ce2d176ffcff92e87e195ceb201", null ],
      [ "DataSize_Word", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a87ac71b6838461fc0d4266e1c28e8c20a8d8210eda55ca86d7429029fb9ed3393", null ]
    ] ],
    [ "IncrementSize_t", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856", [
      [ "IncrementSize_Byte", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856acf493717d550eaa89cc3b3bc9a56a8ea", null ],
      [ "IncrementSize_HalfWord", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856a38d3e9cef1d28d13c85f3672efef9599", null ],
      [ "IncrementSize_Word", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856a02742a584d38b4cf4c63be4c44fa6fc3", null ],
      [ "IncrementSize_NoIcrement", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7c4c38943c6bcb82046e6448f6c95856a2f198f9ab7a8685e4ddd5bf94e40438b", null ]
    ] ],
    [ "NextUseBurst_t", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ac5eca32c8f33cef484fff225fe253023", [
      [ "NextUseBurst_DontUse", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ac5eca32c8f33cef484fff225fe253023a4e726bcb66ff9a6a3cc6fddb8ae8ddbd", null ],
      [ "NextUseBurst_Use", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ac5eca32c8f33cef484fff225fe253023a257481cc793f7786b6407ed0aa711cf8", null ]
    ] ],
    [ "TransferMode_t", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92c", [
      [ "TransferMode_Stop", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca87619834caebd9da61fc6659ce15ea33", null ],
      [ "TransferMode_Basic", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca1ec2e404758d4bcae54e725d37569645", null ],
      [ "TransferMode_AutoRequest", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca1aa317f06e7af3523acaf8362446a980", null ],
      [ "TransferMode_PingPong", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca0b80a731fb781d9d6319d88728f63c83", null ],
      [ "TransferMode_MemoryScatterGather", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca05f3fdf17cd792145cc0bcb1f6e1e956", null ],
      [ "TransferMode_AlternateMemoryScatterGather", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca0417125f81647142017908df55f52358", null ],
      [ "TransferMode_PeripheralScatterGather", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca6286053502807ec0be607ced2052945c", null ],
      [ "TransferMode_AlternatePeripheralScatterGather", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8298b35cca2c25ee53ba49997462f92ca6e9cb22607cdc0026708d9eb84a4ec38", null ]
    ] ],
    [ "ConfigureControlWord", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a6bfff0a6c5ef979df890778aaa650065", null ],
    [ "GetPeripheralChannelAssignment", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a672158dca5551dffb87e74b937a0eb01", null ],
    [ "GetSoftwareChannelAssignment", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#aeb07bf4410fd7348fd0ad02933e67282", null ],
    [ "IsNumberOfTransfersCorrect", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ae6fde859440ab14dc5675395196a2dfb", null ],
    [ "oC_DMA_LLD_ConfigurePeripheralTrade", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ac3eb2e432533ed07faa72a799b7e0a69", null ],
    [ "oC_DMA_LLD_ConfigureSoftwareTrade", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ad16f4e73607de17523b99c7d7e03174d", null ],
    [ "oC_DMA_LLD_DoesDmaHasAccessToAddress", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a989770ec3810d0f92f7ece45b9f5863b", null ],
    [ "oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#aa6ced0a6377dde0abfbd1b8ddfb3a5f6", null ],
    [ "oC_DMA_LLD_IsChannelAvailable", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#aac83a4351267ad9a08c11aa3404825ac", null ],
    [ "oC_DMA_LLD_IsChannelSupportedOnDmaChannel", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a7de3d259b4e8d1babc138af46749e341", null ],
    [ "oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#afc0cb7ffa8fd21addbe4f7a31e224758", null ],
    [ "oC_DMA_LLD_IsTransferCompleteOnChannel", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a8fe53ec33ff533826ae066900a80ee65", null ],
    [ "oC_DMA_LLD_ReadChannelUsedReference", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ace70759c66f73be0e5b810eed28fa84f", null ],
    [ "oC_DMA_LLD_RestoreDefaultStateOnChannel", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#af2b595a4f9fbe1a5bb75e5b7aea69dd1", null ],
    [ "oC_DMA_LLD_TurnOffDriver", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#a618dea7c3c0041f416029c7ee0682c06", null ],
    [ "oC_DMA_LLD_TurnOnDriver", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#af019a32c90986a43df1e8666f8baf749", null ],
    [ "oC_InterruptHandler", "df/db4/ti_2lm4f_2lld_2dma_2oc__dma__lld_8c.html#ab6678418256c7b3171b5f5c6f16660ed", null ]
];