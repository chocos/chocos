var group___core_network =
[
    [ "IP", "de/de2/group___i_p.html", null ],
    [ "Network", "d9/d17/group___n_e_t.html", "d9/d17/group___n_e_t" ],
    [ "Netif - Network Interface object", "d5/d04/group___netif.html", "d5/d04/group___netif" ],
    [ "NetifMan - Network interface manager", "da/d90/group___netif_man.html", "da/d90/group___netif_man" ],
    [ "PortMan - Port Manager", "d2/d9a/group___port_man.html", "d2/d9a/group___port_man" ],
    [ "DHCP - Dynamic Host Configuration Protocol", "d6/d55/group___dhcp.html", "d6/d55/group___dhcp" ],
    [ "Hypertext Transfer Protocol", "d7/d20/group___http.html", "d7/d20/group___http" ],
    [ "Internet Control Message Protocol", "d3/db1/group___icmp.html", "d3/db1/group___icmp" ],
    [ "Tcp - Transmission Control Protocol", "d9/d9b/group___tcp.html", "d9/d9b/group___tcp" ],
    [ "Udp - User Datagram Protocol", "d1/d1b/group___udp.html", "d1/d1b/group___udp" ]
];