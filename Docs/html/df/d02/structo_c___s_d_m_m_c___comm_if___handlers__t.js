var structo_c___s_d_m_m_c___comm_if___handlers__t =
[
    [ "ConfirmIdentification", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a70b6fd4faa46942dd03ad39eda5936a2", null ],
    [ "FinishReadingSectors", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a1cca47c817bf6abfd7c10ebe80493cd3", null ],
    [ "FinishWritingSectors", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#ab0a2b09cb3f31f9e53b337864051a88a", null ],
    [ "InitializeCard", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#ac31a8ec9d1b1d0ea53fc4491f273602e", null ],
    [ "InterfaceId", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a6e2c20ac0b67b46900b1f14d9f38d3de", null ],
    [ "Name", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a760b4e07b204e4852e7fb55e25567623", null ],
    [ "SetTransferMode", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a2842ab39dc09b8890b173786d96144e1", null ],
    [ "StartReadingSectors", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#a8da3766962676a9e480805ce7aceb034", null ],
    [ "StartWritingSectors", "df/d02/structo_c___s_d_m_m_c___comm_if___handlers__t.html#aa84f7cc01848263326d6f61e6097cb95", null ]
];