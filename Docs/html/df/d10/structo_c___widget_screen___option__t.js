var structo_c___widget_screen___option__t =
[
    [ "ChangeFillColor", "df/d10/structo_c___widget_screen___option__t.html#a4287b4b271c55fac3d2512bfa0127577", null ],
    [ "ConstUserPointer", "df/d10/structo_c___widget_screen___option__t.html#a85f852b56af89abc4230ccffd177e902", null ],
    [ "FillColor", "df/d10/structo_c___widget_screen___option__t.html#a06d2e36b64bbe4851d02214b70ce3cf8", null ],
    [ "Font", "df/d10/structo_c___widget_screen___option__t.html#ac0e3d2f0de62b32f90de1ed0ce621b19", null ],
    [ "Image", "df/d10/structo_c___widget_screen___option__t.html#ab140a0438da1c4210d5b475919aad5be", null ],
    [ "Label", "df/d10/structo_c___widget_screen___option__t.html#a2c28600dbc88e9535c6e44b111829cc2", null ],
    [ "Margin", "df/d10/structo_c___widget_screen___option__t.html#afe24533e4eaf868b47edc15a60dd2e93", null ],
    [ "TextAlign", "df/d10/structo_c___widget_screen___option__t.html#a0b8f7826af99fffa1474bf9bf3e4558c", null ],
    [ "UserPointer", "df/d10/structo_c___widget_screen___option__t.html#a496b65cb228543a03da39e12b1fc18a1", null ],
    [ "VerticalTextAlign", "df/d10/structo_c___widget_screen___option__t.html#a17f659429069cb8a0ffca487d6319112", null ]
];