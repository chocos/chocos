var oc__object_8h =
[
    [ "oC_ObjectId_", "db/d47/group___object.html#gaaafdf5a31ee97434787d2a7b8ffe0c0b", null ],
    [ "oC_OBJECTS_REGISTRATIONS_LIST", "db/d47/group___object.html#ga7ab7c48910dc4d1505712f1f84d5a691", null ],
    [ "oC_ObjectControl_t", "db/d47/group___object.html#ga4bac271ea96b5c27df8f40c7b8bdbd8a", null ],
    [ "oC_ObjectId_t", "db/d47/group___object.html#gaaf058d2f12d27cb174accce867cb3936", [
      [ "oC_OBJECTS_REGISTRATIONS_LIST", "db/d47/group___object.html#ggaaf058d2f12d27cb174accce867cb3936ae5b992a4bdf6ca9bd4d4893bb9115187", null ],
      [ "oC_ObjectId_Mask", "db/d47/group___object.html#ggaaf058d2f12d27cb174accce867cb3936a8e52f19f23bd49e95669e32eb562f5f6", null ]
    ] ],
    [ "oC_CheckObjectControl", "db/d47/group___object.html#gad83787aba3d9ff5f0e8a863ffb639c9e", null ],
    [ "oC_CountObjectControl", "db/d47/group___object.html#gaa1bfe148f00962eb98a52001a8ee50fe", null ],
    [ "oC_GetObjectId", "db/d47/group___object.html#gae27cfb3993385202f65cd9c2d14c84e8", null ]
];