var group___screen =
[
    [ "oC_Screen_t", "df/d7b/group___screen.html#gafc62df848f03e3d3d73dc82b9c8c208d", null ],
    [ "oC_Screen_Configure", "df/d7b/group___screen.html#gaa894be898252c0040d38bf92cc87f278", null ],
    [ "oC_Screen_Delete", "df/d7b/group___screen.html#gafa3042a323f53ff6ae8fdf1c21b6d8ae", null ],
    [ "oC_Screen_GetDriver", "df/d7b/group___screen.html#ga5fc4f477da331bb65f3b9d6ec5f8b13f", null ],
    [ "oC_Screen_GetName", "df/d7b/group___screen.html#gae83cdc971cc9e3379d463b40e3c71785", null ],
    [ "oC_Screen_IsConfigured", "df/d7b/group___screen.html#ga99d5dee78275a842d528eaaa70f5c8ee", null ],
    [ "oC_Screen_IsCorrect", "df/d7b/group___screen.html#ga90b65dae1f8e572165955210e6485d91", null ],
    [ "oC_Screen_New", "df/d7b/group___screen.html#ga92e0e0f49c7775ca50fffb30869864c9", null ],
    [ "oC_Screen_ReadColorMap", "df/d7b/group___screen.html#gada1059c135354a54d66d1e226dd23257", null ],
    [ "oC_Screen_ReadResolution", "df/d7b/group___screen.html#ga8ff3e3aed230a41ee6b59a24deac0931", null ],
    [ "oC_Screen_SwitchLayer", "df/d7b/group___screen.html#gafb8529684579809fcb3e5b8bd387ba1c", null ],
    [ "oC_Screen_Unconfigure", "df/d7b/group___screen.html#ga991d798172eb235bb9495e24903c2802", null ]
];