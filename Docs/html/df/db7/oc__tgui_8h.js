var oc__tgui_8h =
[
    [ "oC_TGUI_Property_t", "df/d3e/structo_c___t_g_u_i___property__t.html", null ],
    [ "oC_TGUI_PropertyStyle_t", "db/db1/structo_c___t_g_u_i___property_style__t.html", null ],
    [ "oC_TGUI_DrawPropertyConfig_t", "d6/dff/structo_c___t_g_u_i___draw_property_config__t.html", null ],
    [ "oC_TGUI_DrawListMenuConfig_t", "d6/d70/structo_c___t_g_u_i___draw_list_menu_config__t.html", null ],
    [ "oC_TGUI_EditBoxStyle_t", "d0/d8b/structo_c___t_g_u_i___edit_box_style__t.html", null ],
    [ "oC_TGUI_QuickEditBoxStyle_t", "d5/d1b/structo_c___t_g_u_i___quick_edit_box_style__t.html", null ],
    [ "oC_TGUI_EditBox_t", "d7/de7/structo_c___t_g_u_i___edit_box__t.html", null ],
    [ "oC_TGUI_QuickEditBox_t", "d2/db2/structo_c___t_g_u_i___quick_edit_box__t.html", null ],
    [ "oC_TGUI_PushButtonStyle_t", "db/d8f/structo_c___t_g_u_i___push_button_style__t.html", null ],
    [ "oC_TGUI_PushButton_t", "d9/d76/structo_c___t_g_u_i___push_button__t.html", null ],
    [ "oC_TGUI_SelectionBoxStyle_t", "da/d43/structo_c___t_g_u_i___selection_box_style__t.html", null ],
    [ "oC_TGUI_SelectionBox_t", "d0/d1d/structo_c___t_g_u_i___selection_box__t.html", null ],
    [ "oC_TGUI_ActiveObject_t", "df/d21/structo_c___t_g_u_i___active_object__t.html", null ],
    [ "oC_TGUI_Spiner_t", "d7/db9/structo_c___t_g_u_i___spiner__t.html", null ],
    [ "oC_TGUI_ColorData_t", "dc/df5/structo_c___t_g_u_i___color_data__t.html", null ],
    [ "oC_TGUI_Column_t", "dc/d3b/group___t_g_u_i.html#ga20015ece6dfebd94f02aef9de125c82f", null ],
    [ "oC_TGUI_DrawListHandler_t", "dc/d3b/group___t_g_u_i.html#ga2fbfde9ec51545ec3406a9296a0e1716", null ],
    [ "oC_TGUI_EditBoxSaveValueHandler_t", "dc/d3b/group___t_g_u_i.html#gaa7f16d673cdead8e424b9cdcc1d73138", null ],
    [ "oC_TGUI_EntryIndex_t", "dc/d3b/group___t_g_u_i.html#ga23fae3c43ac48b91f8c6680b307e085c", null ],
    [ "oC_TGUI_Line_t", "dc/d3b/group___t_g_u_i.html#ga45b36850322fe3b75b7219c322a2027d", null ],
    [ "oC_TGUI_MenuHandler_t", "dc/d3b/group___t_g_u_i.html#ga6f4f937f86d4d2a1d3e331b12e04d4d5", null ],
    [ "oC_TGUI_PushButtonHandler_t", "dc/d3b/group___t_g_u_i.html#ga5c3dc1ca5ca4d4eba0107988351220d6", null ],
    [ "oC_TGUI_QuickEditBoxSaveValueHandler_t", "dc/d3b/group___t_g_u_i.html#gac9a69bedf91113fc12445611ab1eca89", null ],
    [ "oC_TGUI_SelectionHandler_t", "dc/d3b/group___t_g_u_i.html#ga388e5fbd1eae96c842442b1d2d6b4464", null ],
    [ "oC_TGUI_SelectListHandler_t", "dc/d3b/group___t_g_u_i.html#ga0b6ac98728a7def34592ca8a7ea5e618", null ],
    [ "oC_TGUI_Color_t", "dc/d3b/group___t_g_u_i.html#gad6eae202ebb01ddbc49152f4ff5283b8", [
      [ "oC_TGUI_Color_Default", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a7f27fa0bfc7ac36fab14bf700e5c85e7", null ],
      [ "oC_TGUI_Color_Black", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a82b9aa02a3b8d26e6504278c10a0d992", null ],
      [ "oC_TGUI_Color_Red", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a111cd82e4d57ed7637bf94a795ae38e3", null ],
      [ "oC_TGUI_Color_Green", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a307b39cf97208e314f4ba9bff191a7a2", null ],
      [ "oC_TGUI_Color_Yellow", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a48a5a009898611193955348388d48ace", null ],
      [ "oC_TGUI_Color_Blue", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8aa433165b68ce528217bf0681a5aad60f", null ],
      [ "oC_TGUI_Color_Magenda", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8ab6f56aabd4d16a250397b373789996f9", null ],
      [ "oC_TGUI_Color_Cyan", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a766eecb3f5a673451dd55b705b5e9c65", null ],
      [ "oC_TGUI_Color_LightGray", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a4bb20181918ef86b3c284eec20dcfe02", null ],
      [ "oC_TGUI_Color_DarkGray", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a3b0322da3bcccc2213f35e465aa84a83", null ],
      [ "oC_TGUI_Color_LightRed", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a9a667bd6d37371373859213429b60c35", null ],
      [ "oC_TGUI_Color_LightGreen", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8ada6cf47d547ecd235f0b1b8362c73068", null ],
      [ "oC_TGUI_Color_LightYellow", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a2c53d4249711806db0d5b437946e3e8c", null ],
      [ "oC_TGUI_Color_LightBlue", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8adeee6ec04122649914c84f85614f99c5", null ],
      [ "oC_TGUI_Color_LightMagenda", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8abcad409ce70c89c2201cbcdeb25fdfc9", null ],
      [ "oC_TGUI_Color_LightCyan", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8ac1ce9fd8293d1753142e6caab4aece35", null ],
      [ "oC_TGUI_Color_White", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8ab0031de9a971e412d41c6f88c527eb64", null ],
      [ "oC_TGUI_Color_NumberOfColors", "dc/d3b/group___t_g_u_i.html#ggad6eae202ebb01ddbc49152f4ff5283b8a4c1a7e65b0cebce6366f42e1a6a1475e", null ]
    ] ],
    [ "oC_TGUI_Key_t", "dc/d3b/group___t_g_u_i.html#gae9ed2d2fee22acf71a044132e2bc47f1", [
      [ "oC_TGUI_Key_Enter", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1aab61c2300c4d56e705789c118dad1f89", null ],
      [ "oC_TGUI_Key_Backspace", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a94353841fa08dc954e3595328a0a09fa", null ],
      [ "oC_TGUI_Key_Tab", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a0e951eb64bb2f767fa89119c604d5bdc", null ],
      [ "oC_TGUI_Key_Control", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a37ce9962ccc4d72d306c5f57b0b9b6f9", null ],
      [ "oC_TGUI_Key_ControlC", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a2e046c405f8cf68751629f4ecdde96ca", null ],
      [ "oC_TGUI_Key_SpecialKeysId", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1afee7114e5d94965748c08fdcadc643c6", null ],
      [ "oC_TGUI_Key_ESC", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a462217747d063adce6efee5ccbd7e999", null ],
      [ "oC_TGUI_Key_ArrowUp", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a99d8a4215cc03c4b978b2c7aef92e19f", null ],
      [ "oC_TGUI_Key_ArrowDown", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1ace65b4b1130ecbb7495c199eb9a02b23", null ],
      [ "oC_TGUI_Key_ArrowRight", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a458ead7af8ab10283f55c6c0d8224f3d", null ],
      [ "oC_TGUI_Key_ArrowLeft", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a490b437b828ab69cd6fea13386cd71c5", null ],
      [ "oC_TGUI_Key_F1", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1af3491c34287b36515814928bde1ec4ac", null ],
      [ "oC_TGUI_Key_F2", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1af63b164148f300dc5d3adba168f812f6", null ],
      [ "oC_TGUI_Key_F3", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a549e098500e0b71fd74e7b1ab4d0a15f", null ],
      [ "oC_TGUI_Key_F4", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a674a0c61775b4084c3b6bfbe6b533881", null ],
      [ "oC_TGUI_Key_F5", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a1aa8a528d6d7dbeaf77bafcb01013e9d", null ],
      [ "oC_TGUI_Key_F6", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1aed07dbac45bbc8ecf6365f9ae9949392", null ],
      [ "oC_TGUI_Key_F7", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a95772a038d860c64f42e5817767f4524", null ],
      [ "oC_TGUI_Key_F8", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a59b7ec6ab4ea4704b7ca6682a71d20d5", null ],
      [ "oC_TGUI_Key_F9", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a4b8eeb4033623c092eba20371476a326", null ],
      [ "oC_TGUI_Key_F10", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a68dce888fe80648bd9a3adea7058b19f", null ],
      [ "oC_TGUI_Key_F11", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a541d6224a8c787cb7075538e8f0074cb", null ],
      [ "oC_TGUI_Key_F12", "dc/d3b/group___t_g_u_i.html#ggae9ed2d2fee22acf71a044132e2bc47f1a9f47216f9ddaa40070b5aed6df9fd389", null ]
    ] ],
    [ "oC_TGUI_TextStyle_t", "dc/d3b/group___t_g_u_i.html#ga67e95ed3d396b69ce588cb053edecd0e", [
      [ "oC_TGUI_TextStyle_Default", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea00cbdfad187d513a3715048febbad474", null ],
      [ "oC_TGUI_TextStyle_Bold", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea3bf18397aef48274ac3eb4ee14210e64", null ],
      [ "oC_TGUI_TextStyle_Dim", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea56f42e3e8a4ee7017f3b3949e6ceb2ea", null ],
      [ "oC_TGUI_TextStyle_Underline", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea56cd4ae38d9cf9fbdc7176e0ebdf0609", null ],
      [ "oC_TGUI_TextStyle_Blink", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0eabb78e6b5b195d043943548f0e681e394", null ],
      [ "oC_TGUI_TextStyle_Inverted", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea3717c3e3c28e6f6c4d3992d2d2abb2b5", null ],
      [ "oC_TGUI_TextStyle_Hidden", "dc/d3b/group___t_g_u_i.html#gga67e95ed3d396b69ce588cb053edecd0ea8f1a23e03d35d5f4c63a2acf8c7d0417", null ]
    ] ],
    [ "oC_TGUI_ValueType_t", "dc/d3b/group___t_g_u_i.html#ga5a7246af0c484c0ddbb445a1c4a5b979", [
      [ "oC_TGUI_ValueType_UINT", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a49dc566b43ff552597ee17e268370d99", null ],
      [ "oC_TGUI_ValueType_U32", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a5c287cfa8a4ac6d6fe1992ab0af4c6ce", null ],
      [ "oC_TGUI_ValueType_U64", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a303db85eafceb1337574fd8612a58fce", null ],
      [ "oC_TGUI_ValueType_I32", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a9de85d96d2ae05a72b13e896b031ab3d", null ],
      [ "oC_TGUI_ValueType_I64", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a775815c30a82407635aabc183042e757", null ],
      [ "oC_TGUI_ValueType_ValueFloat", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a302e69f6b5eace718bc482a2a7cea4cb", null ],
      [ "oC_TGUI_ValueType_ValueDouble", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a0caf33031339b343a9f5d0e7a6d1c9c5", null ],
      [ "oC_TGUI_ValueType_ValueChar", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a1934d1b3f5c96e1eacc4c23540805ed3", null ],
      [ "oC_TGUI_ValueType_ValueString", "dc/d3b/group___t_g_u_i.html#gga5a7246af0c484c0ddbb445a1c4a5b979a883e463823d83e9e6ccbb256275cfd89", null ]
    ] ],
    [ "oC_TGUI_ClearPartOfScreen", "df/db7/oc__tgui_8h.html#ab222a0af614cc5725c886ecdf773c637", null ],
    [ "oC_TGUI_DrawActiveObjects", "df/db7/oc__tgui_8h.html#a8eb01510e602f4d05ad6a01801ef647f", null ],
    [ "oC_TGUI_DrawBox", "df/db7/oc__tgui_8h.html#adadc29e82be948c5c5f9e779fb61141d", null ],
    [ "oC_TGUI_DrawMenu", "df/db7/oc__tgui_8h.html#a9615e93e0d47f04726c6988920b021b8", null ],
    [ "oC_TGUI_ResetDevice", "df/db7/oc__tgui_8h.html#adb978c494db9c40898d2ac43157abfa0", null ]
];