var oc__event_8h =
[
    [ "oC_Event_t", "d0/def/group___event.html#gab8c8c67d6bb295bb85b37156a8447dbd", null ],
    [ "oC_Event_CompareType_t", "d0/def/group___event.html#gac7de6a8fda2b8d3bf68e5e5199367f82", [
      [ "oC_Event_CompareType_Equal", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82adbcf2787bd9a8a738223177ed3e62329", null ],
      [ "oC_Event_CompareType_Less", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82a61248db327a7ee9ce1e738618db1f413", null ],
      [ "oC_Event_CompareType_LessOrEqual", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82a3d957060845f00efd69d130f79ce8c0d", null ],
      [ "oC_Event_CompareType_Greater", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82a4fdc1f820b33f6650edeefae4c5e8439", null ],
      [ "oC_Event_CompareType_GreaterOrEqual", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82a8288fcfb3d9c23713c196afb2750227f", null ],
      [ "oC_Event_CompareType_NumberOfElements", "d0/def/group___event.html#ggac7de6a8fda2b8d3bf68e5e5199367f82a40da26fc3e456d3cdab4e27b807fbcb8", null ]
    ] ],
    [ "oC_Event_Protect_t", "d0/def/group___event.html#ga224f001ba8c0480c1353c6af6505ca0f", [
      [ "oC_Event_Protect_NotProtected", "d0/def/group___event.html#gga224f001ba8c0480c1353c6af6505ca0fa4e500d8b4893618188837e6fe106d2e4", null ],
      [ "oC_Event_Protect_Protected", "d0/def/group___event.html#gga224f001ba8c0480c1353c6af6505ca0fa56afb76c7284b2a79961bbb8bfecf4c7", null ]
    ] ],
    [ "oC_Event_State_t", "d0/def/group___event.html#ga23d64bd1e2b509d45e0b41d4cee675fe", [
      [ "oC_Event_State_Active", "d0/def/group___event.html#gga23d64bd1e2b509d45e0b41d4cee675feaf65021de06111e060be31bfef7e0d745", null ],
      [ "oC_Event_State_Inactive", "d0/def/group___event.html#gga23d64bd1e2b509d45e0b41d4cee675feae4353e9eb63c80dba96ab84893ba8ec6", null ]
    ] ],
    [ "oC_Event_StateMask_t", "d0/def/group___event.html#ga7cdede638c8e74c81ae331f9b42eb72c", [
      [ "oC_Event_StateMask_Full", "d0/def/group___event.html#gga7cdede638c8e74c81ae331f9b42eb72cada57035819952e292f82d05a62941cd3", null ],
      [ "oC_Event_StateMask_DifferentThan", "d0/def/group___event.html#gga7cdede638c8e74c81ae331f9b42eb72ca264f81854413c3f384fe5da9d662e5a6", null ]
    ] ],
    [ "oC_Event_ClearStateBits", "d0/def/group___event.html#gaabe9728ecb2ada74c2fade201e899368", null ],
    [ "oC_Event_Delete", "d0/def/group___event.html#ga4a796e93351123c4530d36883463e54e", null ],
    [ "oC_Event_GetState", "d0/def/group___event.html#ga30253a6fc9edae1f6fb16df95b1f1b25", null ],
    [ "oC_Event_IsCorrect", "d0/def/group___event.html#ga9f2e7d398b02e8086e0bf698c2ea9766", null ],
    [ "oC_Event_New", "d0/def/group___event.html#gaf4180593560905b0b08017b529765483", null ],
    [ "oC_Event_ProtectDelete", "d0/def/group___event.html#ga63b77b5744afd99e42a6d2a3c01b2165", null ],
    [ "oC_Event_ReadState", "d0/def/group___event.html#gac6a2874af2a4b928e37693db11ee9bde", null ],
    [ "oC_Event_SetState", "d0/def/group___event.html#gab2085317c76d85664d15436c603d5d83", null ],
    [ "oC_Event_SetStateBits", "d0/def/group___event.html#ga04649e40468d78a050db552af5397506", null ],
    [ "oC_Event_WaitForBitClear", "d0/def/group___event.html#gab73d0e9b737e9d6ca0757c67fe4db64a", null ],
    [ "oC_Event_WaitForBitSet", "d0/def/group___event.html#ga0cbb17338eb282552049d660c016cb28", null ],
    [ "oC_Event_WaitForState", "d0/def/group___event.html#ga4921358f46cbbb79d1437af76c8d30ed", null ],
    [ "oC_Event_WaitForValue", "d0/def/group___event.html#ga0b44728d87e6c39ba1a675f45df72fc5", null ]
];