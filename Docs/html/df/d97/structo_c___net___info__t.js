var structo_c___net___info__t =
[
    [ "BaudRate", "df/d97/structo_c___net___info__t.html#a21f557ec09c1b4b4471715181a296ad0", null ],
    [ "DriverName", "df/d97/structo_c___net___info__t.html#a4750cde45b1484103e752e61f894f117", null ],
    [ "HardwareAddress", "df/d97/structo_c___net___info__t.html#a9d28e08ecdac9cfe93efcfb618aa56fd", null ],
    [ "HardwareAddressLength", "df/d97/structo_c___net___info__t.html#a02b1f00a57ff89fb014f29a2563844e1", null ],
    [ "HardwareBroadcastAddress", "df/d97/structo_c___net___info__t.html#a8ffa846d94ec1ebf7b0d10a2b8b104a8", null ],
    [ "HardwareType", "df/d97/structo_c___net___info__t.html#a9a33d16d0163d8b1c01c18cdaf37b580", null ],
    [ "InterfaceIndex", "df/d97/structo_c___net___info__t.html#a59892131e2a2a749530269eb4beefe7a", null ],
    [ "InterfaceName", "df/d97/structo_c___net___info__t.html#a0c476a9d3fee62a169aacc7864e83a23", null ],
    [ "IPv4", "df/d97/structo_c___net___info__t.html#ae4725753cf1810be4f216c32a28b868f", null ],
    [ "IPv6", "df/d97/structo_c___net___info__t.html#a4aecd6bf091252579f74eb1ddd37f495", null ],
    [ "LinkStatus", "df/d97/structo_c___net___info__t.html#a070ee446c9d8e53fc12e0cc49a658f54", null ],
    [ "NetworkLayer", "df/d97/structo_c___net___info__t.html#a12b60c80df0855326bc2764bd039d39d", null ],
    [ "QueueStatus", "df/d97/structo_c___net___info__t.html#a93ddd4e1685bca83f61ee86a225b0330", null ],
    [ "ReceivedBytes", "df/d97/structo_c___net___info__t.html#aed2b642d36e5f19c2c6b572a5d74c79f", null ],
    [ "TransmittedBytes", "df/d97/structo_c___net___info__t.html#aa80b79ca35bed029582c7a73570481a8", null ]
];