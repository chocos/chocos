var oc__memory_8h =
[
    [ "oC_Access_", "df/da6/oc__memory_8h.html#ab17ef82faf7ae48c7022bf5e1f3ed0c2", null ],
    [ "oC_Access_t", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08", [
      [ "oC_Access_None", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08a4b2bc429a6265b29973f188c3dd5911a", null ],
      [ "oC_Access_Read", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08ab2ba99707e5b8f00ca2c09020ea5d777", null ],
      [ "oC_Access_Write", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08abfe5020926a8e8c06ff461c77c134db6", null ],
      [ "oC_Access_Execute", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08ad57d434b66a5e841cd74b1daa3ce083f", null ],
      [ "oC_Access_R", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08af5807077b5f1db05b791cd0b09dbdc5e", null ],
      [ "oC_Access_W", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08a690c84bd4db2eaf32ee195cdb9bcf84b", null ],
      [ "oC_Access_X", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08a4f6ed89965bf278444ec049b689e14aa", null ],
      [ "oC_Access_RW", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08aa37f0087a178aaf13a34011c6b30fe0b", null ],
      [ "oC_Access_RWX", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08a049e35c6f5c2935a8ea92be6594e39ec", null ],
      [ "oC_Access_Full", "df/da6/oc__memory_8h.html#aaa1e75daee6fa0fe6f9d5ef70c84eb08ae0218d609cb62202cbdd7e7085101ca9", null ]
    ] ]
];