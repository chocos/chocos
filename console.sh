#!/bin/sh

#=====================================================================================================
# 
#                       Sets color of the foreground
#
# @param Color          String with color to set.
#                               List of supported:
#                                   "black"
#                                   "red"
#                                   "green"
#                                   "yellow"
#                                   "blue"
#                                   "magenda"
#                                   "cyan"
#                                   "white"
#
#=====================================================================================================
SetForegroundColor() 
{
    Color="$1";
    ColorNumber=0;
    
    if [ "$Color" = "black"   ]; then ColorNumber=30; fi
    if [ "$Color" = "red"     ]; then ColorNumber=31; fi 
    if [ "$Color" = "green"   ]; then ColorNumber=32; fi
    if [ "$Color" = "yellow"  ]; then ColorNumber=33; fi
    if [ "$Color" = "blue"    ]; then ColorNumber=34; fi
    if [ "$Color" = "magenda" ]; then ColorNumber=35; fi
    if [ "$Color" = "cyan"    ]; then ColorNumber=36; fi
    if [ "$Color" = "white"   ]; then ColorNumber=37; fi
    
    if [ "$1" = "" ]; then 
        printf "\033[31;40mError! Color is not given!\033[37;40m\n"
    elif [ $ColorNumber = 0 ]; then
        printf "\033[31;40mError! Color $Color is not recognized!\033[37;40m\n"
    else
        printf "\033[1;${ColorNumber}m"
    fi
}

#=====================================================================================================
# 
#                       Sets color of the background
#
# @param Color          String with color to set.
#                               List of supported:
#                                   "black"
#                                   "red"
#                                   "green"
#                                   "yellow"
#                                   "blue"
#                                   "magenda"
#                                   "cyan"
#                                   "white"
#
#=====================================================================================================
SetBackgroundColor() 
{
    Color="$1";
    ColorNumber=0;
    
    if [ "$Color" = "black"   ]; then ColorNumber=30; fi
    if [ "$Color" = "red"     ]; then ColorNumber=31; fi 
    if [ "$Color" = "green"   ]; then ColorNumber=32; fi
    if [ "$Color" = "yellow"  ]; then ColorNumber=33; fi
    if [ "$Color" = "blue"    ]; then ColorNumber=34; fi
    if [ "$Color" = "magenda" ]; then ColorNumber=35; fi
    if [ "$Color" = "cyan"    ]; then ColorNumber=36; fi
    if [ "$Color" = "white"   ]; then ColorNumber=37; fi
    
    if [ "$1" = "" ]; then 
        printf "\033[31;40mError! Color is not given!\033[37;40m\n"
    elif [ $ColorNumber = 0 ]; then
        printf "\033[31;40mError! Color $Color is not recognized!\033[37;40m\n"
    else
        printf "\033[${ColorNumber}m"
    fi
}

#=====================================================================================================
#
#                       Resets all console attributes
# 
#=====================================================================================================
ResetAllAttributes()
{
    printf "\033[0m"
}

#=====================================================================================================
#
#                       Saves cursor position
# 
#=====================================================================================================
SaveCursor()
{
    printf "\033[s"
}

#=====================================================================================================
#
#                       Restores cursor position
# 
#=====================================================================================================
RestoreCursor()
{
    printf "\0338"
}