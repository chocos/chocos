/** ****************************************************************************************************************************************
 *
 * @file       oc_[driver_name]_lld.c
 *
 * @brief      The file with source for [DRIVER_NAME] LLD functions
 *
 * @file       [OUTPUT_FILE_NAME]
 *
 * @author     [NAME] [SURNAME] - (Created on: [DATE] - [TIME])
 *
 * @copyright  Copyright (C) [YEAR] [NAME] [SURNAME] <[EMAIL]>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_[driver_name]_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)                      oC_LSF_IsRamAddress(Address)
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsChannelCorrect(Channel)           oC_Channel_IsCorrect([DRIVER_NAME],Channel)
#define IsChannelPoweredOn(Channel)         (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_[DRIVER_NAME]_LLD_IsChannelCorrect( oC_[DRIVER_NAME]_Channel_t Channel )
{
    return oC_Channel_IsCorrect([DRIVER_NAME],Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_[DRIVER_NAME]_LLD_IsChannelIndexCorrect( oC_[DRIVER_NAME]_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements([DRIVER_NAME]);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_[DRIVER_NAME]_LLD_ChannelIndex_t oC_[DRIVER_NAME]_LLD_ChannelToChannelIndex( oC_[DRIVER_NAME]_Channel_t Channel )
{
    return oC_Channel_ToIndex([DRIVER_NAME],Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_[DRIVER_NAME]_Channel_t oC_[DRIVER_NAME]_LLD_ChannelIndexToChannel( oC_[DRIVER_NAME]_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex([DRIVER_NAME],Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_[DRIVER_NAME]_Channel_t oC_[DRIVER_NAME]_LLD_GetChannelOfModulePin( oC_[DRIVER_NAME]_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_[DRIVER_NAME]_LLD))
    {
        oC_Module_TurnOn(oC_Module_[DRIVER_NAME]_LLD);
        errorCode           = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]_LLD))
    {
        oC_Module_TurnOff(oC_Module_[DRIVER_NAME]_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_SetPower( oC_[DRIVER_NAME]_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                               , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off           , oC_ErrorCode_PowerStateNotCorrect) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,Power)       , oC_ErrorCode_CannotEnableChannel)  &&
            ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt) , oC_ErrorCode_CannotEnableInterrupt)
            )
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex([DRIVER_NAME],Channel);

            oC_ASSERT(channelIndex < oC_ModuleChannel_NumberOfElements([DRIVER_NAME]));


            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_ReadPower( oC_[DRIVER_NAME]_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outPower = oC_Machine_GetPowerStateForChannel(Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

