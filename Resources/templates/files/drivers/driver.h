/** ****************************************************************************************************************************************
 *
 * @file       oc_[driver_name].h
 * 
 * File based on [TEMPLATE_FILE_NAME] Ver 1.1.1
 *
 * @brief      The file with interface for [DRIVER_NAME] driver
 *
 * @author     [NAME] [SURNAME] - (Created on: [DATE] - [TIME])
 *
 * @copyright  Copyright (C) [YEAR] [NAME] [SURNAME] <[EMAIL]>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef _OC_[DRIVER_NAME]_H
#define _OC_[DRIVER_NAME]_H
#define DRIVER_HEADER
#define DRIVER_NAME                 [DRIVER_NAME]

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
<IF> ( [LLD_ACTIVE] == true )
#include <oc_[driver_name]_lld.h>
<END_IF>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief [DRIVER_NAME] driver configuration structure
 *
 * This is the configuration structure for the [DRIVER_NAME] driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_[DRIVER_NAME]_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{

} oC_[DRIVER_NAME]_Config_t;

//==========================================================================================================================================
/**
 * @brief The [DRIVER_NAME] context structure.
 *
 * This is the structure with dynamic allocated data for the [DRIVER_NAME]. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_[DRIVER_NAME]_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_[DRIVER_NAME]_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_[DRIVER_NAME]_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_[DRIVER_NAME]_TurnOn            ( void );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_TurnOff           ( void );
extern bool           oC_[DRIVER_NAME]_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_Configure         ( const oC_[DRIVER_NAME]_Config_t * Config , oC_[DRIVER_NAME]_Context_t * outContext );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_Unconfigure       ( const oC_[DRIVER_NAME]_Config_t * Config , oC_[DRIVER_NAME]_Context_t * outContext );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_Read              ( oC_[DRIVER_NAME]_Context_t Context , char * outBuffer , uint32_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_Write             ( oC_[DRIVER_NAME]_Context_t Context , const char * Buffer , uint32_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_[DRIVER_NAME]_Ioctl             ( oC_[DRIVER_NAME]_Context_t Context , oC_Ioctl_Command_t Command , void * Data );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* _OC_[DRIVER_NAME]_H */
