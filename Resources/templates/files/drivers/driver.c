/** ****************************************************************************************************************************************
 *
 * @file       [OUTPUT_FILE_NAME]
 * 
 * File based on [TEMPLATE_FILE_NAME] Ver 1.1.1
 *
 * @brief      The file with source for [DRIVER_NAME] driver interface
 *
 * @author     [NAME] [SURNAME] - (Created on: [DATE] - [TIME])
 *
 * @copyright  Copyright (C) [YEAR] [NAME] [SURNAME] <[EMAIL]>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_[driver_name].h>
<IF> ( [LLD_ACTIVE] == true )
#include <oc_[driver_name]_lld.h>
<END_IF>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         [DRIVER_NAME]
#define DRIVER_FILE_NAME    "[driver_name]"
#define DRIVER_DEBUGLOG     false
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_[DRIVER_NAME]_Configure
#define DRIVER_UNCONFIGURE  oC_[DRIVER_NAME]_Unconfigure
#define DRIVER_TURN_ON      oC_[DRIVER_NAME]_TurnOn
#define DRIVER_TURN_OFF     oC_[DRIVER_NAME]_TurnOff
#define IS_TURNED_ON        oC_[DRIVER_NAME]_IsTurnedOn
#define HANDLE_IOCTL        oC_[DRIVER_NAME]_Ioctl
#define READ_FROM_DRIVER    oC_[DRIVER_NAME]_Read
#define WRITE_TO_DRIVER     oC_[DRIVER_NAME]_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores [DRIVER_NAME] context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all [DRIVER_NAME] driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "[driver_name] module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect      ( oC_[DRIVER_NAME]_Context_t Context );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the [DRIVER_NAME] module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
<IF> ( [LLD_ACTIVE] == true )
 * It also turns on the LLD layer.
<END_IF>
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_[DRIVER_NAME]))
    {
<IF> ( [LLD_ACTIVE] == true )
        errorCode = oC_[DRIVER_NAME]_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            /* This must be always at the end of the function */
            oC_Module_TurnOn(oC_Module_[DRIVER_NAME]);
            errorCode = oC_ErrorCode_None;
        }
<ELSE>
        /* This must be always at the end of the function */
        oC_Module_TurnOn(oC_Module_[DRIVER_NAME]);
        errorCode = oC_ErrorCode_None;
<END_IF>
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the [DRIVER_NAME] driver
 *
 * The function for turning off the [DRIVER_NAME] driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_[DRIVER_NAME]);

<IF> ( [LLD_ACTIVE] == true )
        errorCode = oC_[DRIVER_NAME]_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }
<ELSE>
        errorCode = oC_ErrorCode_None;
<END_IF>
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_[DRIVER_NAME]_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_[DRIVER_NAME]);
}

//==========================================================================================================================================
/**
 * @brief configures [DRIVER_NAME] pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_[DRIVER_NAME]_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_Configure( const oC_[DRIVER_NAME]_Config_t * Config , oC_[DRIVER_NAME]_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]))
    {
        if(
           oC_AssignErrorCodeIfFalse(&errorCode , IsConfigCorrect(Config)   ,          oC_ErrorCode_WrongConfigAddress )  &&
           oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext) ,                  oC_ErrorCode_OutputAddressNotInRAM )
            )
        {

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_[DRIVER_NAME]_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_Unconfigure( const oC_[DRIVER_NAME]_Config_t * Config , oC_[DRIVER_NAME]_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect )     
            )
        {

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_Ioctl( oC_[DRIVER_NAME]_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * The function is for reading buffer by using [DRIVER_NAME] driver. It is called when someone will read the driver file. 
 *
 * @param Context       Context of the driver
 * @param outBuffer     Buffer for data
 * @param Size          Size of the buffer on input, on output number of read bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_Read( oC_[DRIVER_NAME]_Context_t Context , char * outBuffer , uint32_t * Size , oC_Time_t Timeout  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]) 
     && ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * The function is for writing buffer by using [DRIVER_NAME] driver. It is called when someone will write the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_[DRIVER_NAME]_Write( oC_[DRIVER_NAME]_Context_t Context , const char * Buffer , uint32_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_[DRIVER_NAME]) 
     && ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isaddresscorrect(Buffer)             , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {

    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the [DRIVER_NAME] driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_[DRIVER_NAME]_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_[DRIVER_NAME]Context,Context->ObjectControl);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
