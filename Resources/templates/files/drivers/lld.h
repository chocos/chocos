/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the [DRIVER_NAME] driver
 *
 * @file       [OUTPUT_FILE_NAME]
 *
 * @author     [NAME] [SURNAME] - (Created on: [DATE] - [TIME])
 *
 * @copyright  Copyright (C) [YEAR] [NAME] [SURNAME] <[EMAIL]>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup [DRIVER_NAME]-LLD [DRIVER_NAME]-LLD [DRIVER_NAME] Low Level Driver
 *
 * @brief Handles [DRIVER_NAME] transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_[DRIVER_NAME]_LLD_H
#define _OC_[DRIVER_NAME]_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>

#if (oC_Channel_IsModuleDefined([DRIVER_NAME]) && oC_ModulePinFunctions_IsModuleDefined([DRIVER_NAME]) && oC_ModulePin_IsModuleDefined([DRIVER_NAME])) || defined(DOXYGEN)
#define oC_[DRIVER_NAME]_LLD_AVAILABLE

#if oC_Channel_IsModuleDefined([DRIVER_NAME]) == false && !defined(DOXYGEN)
#error [DRIVER_NAME] module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined([DRIVER_NAME]) == false && !defined(DOXYGEN)
#error  [DRIVER_NAME] module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined([DRIVER_NAME]) == false && !defined(DOXYGEN)
#error  [DRIVER_NAME] module pins are not defined
#else

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup [DRIVER_NAME]-LLD
//! @{
#define MODULE_NAME [DRIVER_NAME]


//==========================================================================================================================================
/**
 * @brief channel of the [DRIVER_NAME]
 *
 * The type stores channel of the [DRIVER_NAME]
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_[DRIVER_NAME]_Channel_t;
#endif
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_[DRIVER_NAME]_LLD_PinFunction_t;
#endif
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_[DRIVER_NAME]_Pin_t
 * @brief pin of the [DRIVER_NAME] peripheral
 *
 * The type stores peripheral pin, where the [DRIVER_NAME] channel is connected.
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_[DRIVER_NAME]_Pin_t;
#endif
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the [DRIVER_NAME]
 *
 * The type stores index of the channel in the [DRIVER_NAME] array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_[DRIVER_NAME]_LLD_ChannelIndex_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each [DRIVER_NAME] channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each [DRIVER_NAME] channel defined in the machine
 * oC_[DRIVER_NAME]_LLD_ForEachChannel(Channel , ChannelIndex)
 * {
 *      // this will be executed for each channel
 *      oC_[DRIVER_NAME]_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_[DRIVER_NAME]_LLD_ForEachChannel( Channel , ChannelIndex )           oC_Channel_Foreach( [DRIVER_NAME] , Channel )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup [DRIVER_NAME]-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief checks if the [DRIVER_NAME] channel is correct
 *
 * Checks if the [DRIVER_NAME] channel is correct.
 *
 * @param Channel       Channel of the [DRIVER_NAME] to check
 *
 * @return true if channel is correct
 */
//==========================================================================================================================================
extern bool oC_[DRIVER_NAME]_LLD_IsChannelCorrect( oC_[DRIVER_NAME]_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief check if the channel index is correct
 *
 * Checks if the [DRIVER_NAME] channel index is correct.
 *
 * @param ChannelIndex  Index of the channel in module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_[DRIVER_NAME]_LLD_IsChannelIndexCorrect( oC_[DRIVER_NAME]_LLD_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief converts channel to channel index
 *
 * The function converts channel to index of the channel in the [DRIVER_NAME] module
 *
 * @param Channel       Channel of the [DRIVER_NAME]
 *
 * @return index of the channel in the [DRIVER_NAME]
 */
//==========================================================================================================================================
extern oC_[DRIVER_NAME]_LLD_ChannelIndex_t oC_[DRIVER_NAME]_LLD_ChannelToChannelIndex( oC_[DRIVER_NAME]_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief converts channel index to channel
 *
 * The function converts index of the channel to channel in the [DRIVER_NAME] module
 *
 * @param Channel       Channel of the [DRIVER_NAME]
 *
 * @return index of the channel in the [DRIVER_NAME]
 */
//==========================================================================================================================================
extern oC_[DRIVER_NAME]_Channel_t oC_[DRIVER_NAME]_LLD_ChannelIndexToChannel( oC_[DRIVER_NAME]_LLD_ChannelIndex_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel of peripheral pin
 *
 * The function reads channel of peripheral pin. It not checks if the channel is correct, use #oC_[DRIVER_NAME]_LLD_IsChannelCorrect function to check
 * if returned channel is correct.
 *
 * @param ModulePin         special pin that is connected to [DRIVER_NAME] channel
 *
 * @return channel of [DRIVER_NAME] (it can be not correct)
 */
//==========================================================================================================================================
extern oC_[DRIVER_NAME]_Channel_t oC_[DRIVER_NAME]_LLD_GetChannelOfModulePin( oC_[DRIVER_NAME]_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleIsTurnedOn       | The given module has been already started
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | The given module has not started yet
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief allow to turn on/off register map
 *
 * The function is for manage power state on the [DRIVER_NAME] channel. It allow to turn on/off register map. Note, that if register map is turned off
 * then none of operations can be performed on the channel. It is required to turn on the power before any configurations on the channel.
 *
 * @param Channel   one of available channels in the machine
 * @param Power     power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel           | The given `Channel` is not correct `[DRIVER_NAME]` channel
 *  #oC_ErrorCode_PowerStateNotCorrect   | The given `Power` is not #oC_Power_On or #oC_Power_Off
 *  #oC_ErrorCode_CannotEnableChannel    | The given channel cannot be enabled (reason is unknown)
 *  #oC_ErrorCode_CannotEnableInterrupt  | Cannot enable channel interrupt
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_SetPower( oC_[DRIVER_NAME]_Channel_t Channel , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief read current power state
 *
 * The function is for reading power state of the register map on the selected channel. Channel should be powered on before configuration
 * and any other operations on it. Powering off the channel allow to save power. Each channel should be powered off when the driver is powered
 * off.
 *
 * @param Channel   one of available channels in the machine
 * @param outPower  destination for the power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                      | Description
 * -------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_NotImplemented        | Function is already not implemented
 *  #oC_ErrorCode_ImplementError        | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet   | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel          | The given `Channel` is not correct `[DRIVER_NAME]` channel
 *  #oC_ErrorCode_OutputAddressNotInRAM | `outPower` does not point to the RAM section
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_ReadPower( oC_[DRIVER_NAME]_Channel_t Channel , oC_Power_t * outPower );

//==========================================================================================================================================
/**
 * @brief restore default state on channel
 *
 * The function is for restoring default state on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_[DRIVER_NAME]_LLD_RestoreDefaultStateOnChannel( oC_[DRIVER_NAME]_Channel_t Channel );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_[DRIVER_NAME]_LLD_H */
#endif
