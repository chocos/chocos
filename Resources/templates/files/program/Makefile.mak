############################################################################################################################################
##
##  Author:         [NAME] [SURNAME]
##  Date:           [DATE] - [TIME]
##  Description:    makefile for [PROGRAM_NAME] program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
PROGRAM_NAME            = [PROGRAM_NAME]
SPACE                   = [SPACE]_SPACE
OPTIMALIZE              = O0
WARNING_FLAGS           = -Wall
CSTD                    = c99
DEFINITIONS             =
SOURCE_FILES            = main.c
INCLUDES_DIRS           = 
STANDARD_INPUT          = uart_stdio
STANDARD_OUTPUT         = uart_stdio
STANDARD_ERROR          = uart_stdio
HEAP_MAP_SIZE			= 0
PROCESS_STACK_SIZE      = [STACK_SIZE]
ALLOCATION_LIMIT        = 0
TRACK_ALLOCATION        = FALSE
FILE_TYPE               = [FILE_TYPE]

##============================================================================================================================
##                                          
##              INCLUDE MAIN MAKEFILE               
##                                          
##============================================================================================================================
include $(PROGRAM_MK_FILE_PATH)
