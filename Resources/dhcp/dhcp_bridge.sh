#!/bin/bash

# Nazwa interfejsu bridge
BRIDGE_NAME="br0"

# Interfejsy sieciowe
IFACE1="enp114s0"  # interfejs dla płytki embedded
IFACE2="enp11s0"   # interfejs do routera

# Sprawdź, czy interfejsy istnieją
if ! ip link show "$IFACE1" > /dev/null 2>&1; then
    echo "Błąd: Interfejs $IFACE1 nie istnieje."
    exit 1
fi

if ! ip link show "$IFACE2" > /dev/null 2>&1; then
    echo "Błąd: Interfejs $IFACE2 nie istnieje."
    exit 1
fi

# Usuń istniejący bridge, jeśli istnieje
if ip link show "$BRIDGE_NAME" > /dev/null 2>&1; then
    echo "Usuwanie istniejącego bridge'a $BRIDGE_NAME..."
    sudo ip link set "$BRIDGE_NAME" down
    sudo ip link delete "$BRIDGE_NAME" type bridge
fi

# Tworzenie nowego bridge
echo "Tworzenie bridge $BRIDGE_NAME..."
sudo ip link add name "$BRIDGE_NAME" type bridge

# Dodawanie interfejsów do bridge
echo "Dodawanie interfejsów $IFACE1 i $IFACE2 do bridge $BRIDGE_NAME..."
sudo ip link set "$IFACE1" master "$BRIDGE_NAME"
sudo ip link set "$IFACE2" master "$BRIDGE_NAME"

# Włączenie interfejsów i bridge
echo "Aktywacja interfejsów i bridge..."
sudo ip link set "$IFACE1" up
sudo ip link set "$IFACE2" up
sudo ip link set "$BRIDGE_NAME" up

# Uzyskiwanie adresu IP dla bridge (przez DHCP)
echo "Pobieranie adresu IP dla bridge $BRIDGE_NAME..."
sudo dhclient "$BRIDGE_NAME"

echo "Bridge został skonfigurowany pomyślnie!"
