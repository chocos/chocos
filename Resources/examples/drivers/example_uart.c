/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the example_uart program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-28 - 14:32:56) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_uart.h>

//! [read_pin]
//=============================================================================
//                    == READ PIN ==
//
//          This function reads the pin by its name.
//
//=============================================================================
static bool ReadPin( const char* PinName , oC_Pin_t* Pin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if( ErrorCode( oC_GPIO_FindPinByName(PinName, Pin) ) )
    {
        return true;
    }
    else
    {
        printf("Cannot find the pin '%s': %R\n", PinName, errorCode);
        return false;
    }
}
//! [read_pin]

//! [read_arguments]
//=============================================================================
//                    == READ PINS ==
//
//          This function reads the pins by their names.
//
//=============================================================================
static bool ReadPins( int Argc , char** Argv , oC_Pin_t* TxPin , oC_Pin_t* RxPin )
{
    const char* txPinName = oC_GetArgumentAfter(Argc, Argv, "--tx-pin");
    const char* rxPinName = oC_GetArgumentAfter(Argc, Argv, "--rx-pin");

    if(txPinName == NULL)
    {
        printf("The --tx-pin argument is required\n");
        return false;
    }

    if(rxPinName == NULL)
    {
        printf("The --rx-pin argument is required\n");
        return false;
    }

    return ReadPin(txPinName, TxPin) && ReadPin(rxPinName, RxPin);
}
//! [read_arguments]

//! [configure_uart]
//=============================================================================
//                   == CONFIGURE UART ==
//
//          This function configures the UART.
//
//=============================================================================
static bool ConfigureUart( oC_UART_Config_t* Config, oC_UART_Context_t* Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    printf("Configuring the UART...\n");

    // Call the configuration function
    if( ErrorCode( oC_UART_Configure(Config, Context) ) )
    {
        return true;
    }
    else
    {
        printf("Configuration failed: %R\n", errorCode);
        return false;
    }
}
//! [configure_uart]


//! [unconfigure_uart]
//=============================================================================
//                   == UNCONFIGURE ==
//
//          This function unconfigures the UART.
//
//=============================================================================
static bool Unconfigure( oC_UART_Context_t* Context , oC_UART_Config_t* Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    printf("Unconfiguration of the UART driver\n");
    errorCode = oC_UART_Unconfigure(Config, Context);
    if(errorCode != oC_ErrorCode_None)
    {
        printf("Error while unconfiguring the UART: %R\n", errorCode);
        return false;
    }

    return true;
}
//! [unconfigure_uart]

//! [transmit]
//=============================================================================
//                   == TRANSMIT ==
//
//          This function transmits the data.
//
//=============================================================================
static bool Transmit( oC_UART_Context_t Context , const char* Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_MemorySize_t size = strlen(Data) + 1;

    printf("Sending data: %s\n", Data);
    errorCode = oC_UART_Write(Context, Data, &size, oC_s(1));
    if(errorCode != oC_ErrorCode_None)
    {
        printf("Error while sending data: %R\n", errorCode);
        return false;
    }

    return true;
}
//! [transmit]

//! [receive]
//=============================================================================
//                   == RECEIVE ==
//
//          This function receives the data.
//
//=============================================================================
static bool Receive( oC_UART_Context_t Context , char* Buffer, oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    errorCode = oC_UART_Receive(Context, Buffer, Size, oC_min(5));
    if( errorCode != oC_ErrorCode_None && errorCode != oC_ErrorCode_NoAllBytesRead )
    {
        printf("Error while receiving data: %R\n", errorCode);
        return false;
    }
    printf("Received data: %s\n", Buffer);

    return true;
}
//! [receive]

//=============================================================================
//                   == MAIN FUNCTION ==
//
//          This is the main function of the example_uart program - 
//          the program shows how to use the UART driver.
//
//=============================================================================
int main( int Argc , char ** Argv )
{
    oC_UART_Context_t context = NULL;
    oC_Pin_t txPin = oC_Pin_NotUsed;
    oC_Pin_t rxPin = oC_Pin_NotUsed;
    bool loopback = oC_ArgumentOccur(Argc, Argv, "--loopback");

    if( !ReadPins(Argc, Argv, &txPin, &rxPin) )
    {
        return -1;
    }

    //! [uart_configuration]
    oC_UART_Config_t config = {
        .Rx         = rxPin,                            // The RX pin
        .Tx         = txPin,                            // The TX pin
        .WordLength = oC_UART_WordLength_8Bits,         // 8 bits per word
        .BitRate    = 9600,                             // 9600 bits per second
        .Parity     = oC_UART_Parity_None,              // No parity
        .StopBit    = oC_UART_StopBit_1Bit,             // 1 stop bit
        .BitOrder   = oC_UART_BitOrder_LSBFirst,        // Least significant bit first
        .Invert     = oC_UART_Invert_NotInverted,       // Not inverted signals
        .Dma        = oC_UART_Dma_DontUse,              // Don't use DMA
        .Loopback   = loopback    // Loopback mode for testing
    };
    //! [uart_configuration]

    if( !ConfigureUart(&config, &context) )
    {
        return -1;
    }

    const char testMessage[] = "Hello, World!";
    if( !Transmit(context, testMessage) )
    {
        Unconfigure(&context, &config);
        return -1;
    }

    char buffer[sizeof(testMessage)];
    memset(buffer, 0, sizeof(buffer));

    if( !Receive(context, buffer, sizeof(buffer)) )
    {
        Unconfigure(&context, &config);
        return -1;
    }

    if(strcmp(buffer, testMessage) != 0)
    {
        printf("Received data is incorrect: %s\n", buffer);
        Unconfigure(&context, &config);
        return -1;
    }
    printf("Received data is correct\n");

    if( !Unconfigure(&context, &config) )
    {
        return -1;
    }

    return 0;
}
