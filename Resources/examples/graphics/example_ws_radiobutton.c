/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the ws_radiobutton program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-27 - 17:22:28) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @example     example_ws_radiobutton.c
 * 
 ******************************************************************************************************************************************/

//!< Thanks to this definition we can use short names of types inside the widget screen module
#define WIDGET_SCREEN_DEFINE_SHORT_TYPES
#include <oc_stdio.h>
#include <oc_array.h>
#include <oc_widgetscreen.h>

//! [palette]
//=============================================================================
/*
 *                              === PALETTE ===
 *
 *   The variable stores information about colors used by the program.
 *   Thanks to that you don't have to define colors for each widget,
 *   but you can keep set of colors in one place and replace all occurrences
 *   in easy way if you need. Of course you are not obligated to use only
 *   one palette - you can define as many palettes as you want to.
 */
//=============================================================================
static const oC_WidgetScreen_Palette_t  Palette = {
    //
    //  Palette to use when the widget is ready to use
    //
    [WidgetState_Ready] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Activated] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Pressed] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x1A090D ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Hovered] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0x6B6570 ,
    } ,
    [WidgetState_Disabled] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0x6B6570 ,
    } ,
};
//! [palette]

//! [drawstyle]
//=============================================================================
/*
 * Stores style for drawing widgets
 */
//=============================================================================
static const oC_WidgetScreen_DrawStyle_t DrawStyle = {
    [WidgetState_Ready] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Dashed ,
           .BorderWidth = 1 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Pressed] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Activated] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Hovered] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Disabled] = {
           .BorderStyle = BorderStyle_None ,
           .BorderWidth = 0 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
};
//! [drawstyle]

//! [optionpressed]
//=============================================================================
/*
 * Handler called when option has been pressed
 */
//=============================================================================
static oC_WidgetScreen_ScreenId_t HandleWidget_OptionPressed( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );
    if(option != NULL && option->Label != NULL && strcmp(option->Label, "Exit") == 0)
    {
        return 0xFF; // Exit from the program
    }
    else
    {
        return ScreenID;
    }
}
//! [optionpressed]

//! [widgets]
//=============================================================================
/*
 * List of widgets on the screen
 */
//=============================================================================
static const oC_WidgetScreen_WidgetDefinition_t ExampleWidgets[] = {
        {
             .Type                          = oC_WidgetScreen_WidgetType_RadioButton,
             .Position                      = { .X = 20, .Y = 20 },
             .Width                         = 440 ,
             .Height                        = 230 ,
             .ZPosition                     = 1,
             .Palette                       = &Palette,
             .DrawStyle                     = &DrawStyle,
             .Handlers                      = {
                 [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_OptionPressed ,
             },
             .TypeSpecific                  = {
                   .RadioButton = {
                                   .OptionWidth             = 215,
                                   .OptionHeight            = 110,
                                   .HorizontalCellSpacing   = 5,
                                   .VerticalCellSpacing     = 5,
                   },
             },
        },
};
//! [widgets]

//! [screens]
//=============================================================================
/*
 * Called to prepare the example screen
 */
//=============================================================================
static oC_WidgetScreen_ScreenId_t PrepareScreen( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_ScreenId_t ScreenID )
{
    for(int i = 0; i < 4; i++)
    {
        oC_WidgetScreen_Option_t option;
        oC_DefaultString_t label;

        memset(&option, 0, sizeof(option));
        memset(label, 0, sizeof(label));

        if(i < 3)
        {
            sprintf(label, "Option number %d", i + 1);
            option.Label = label;
        }
        else
        {
            option.Label = "Exit";
        }
        option.Font  = oC_Font_(CooperBlack_20pt);
        option.VerticalTextAlign = VerticalTextAlign_Center;
        option.TextAlign         = TextAlign_Center;

        oC_WidgetScreen_AddOption(Screen, 0, &option);
    }
    return ScreenID;
}

//=============================================================================
/*
 * List of widget screens
 */
//=============================================================================
static const oC_WidgetScreen_ScreenDefinition_t ExampleScreens[] = {
        {
             .Position                      = { .X = 0, .Y = 0 },
             .Width                         = 480 ,
             .Height                        = 272 ,
             .BackgroundColor               = 0x4A314D,
             .ColorFormat                   = oC_ColorFormat_RGB888,
             .PrepareHandler                = PrepareScreen,
             .Handler                       = NULL,
             .WidgetsDefinitions            = ExampleWidgets,
             .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(ExampleWidgets)
        },
};
//! [screens]

//==========================================================================================================================================
/*
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t errorCode = oC_WidgetScreen_HandleScreens(NULL, ExampleScreens, oC_ARRAY_SIZE(ExampleScreens), NULL);
    printf("Handling screens finished with: %R\n", errorCode);

    return 0;
}
