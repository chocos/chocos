/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the ws_buttons.c program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-27 - 20:31:24) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @example     example_ws_buttons.c
 * 
 ******************************************************************************************************************************************/

#define WIDGET_SCREEN_DEFINE_SHORT_TYPES
#include <oc_stdio.h>
#include <oc_widgetscreen.h>


//! [palette]
//=============================================================================
/*
 *                              === PALETTE ===
 *
 *   The variable stores information about colors used by the program.
 *   Thanks to that you don't have to define colors for each widget,
 *   but you can keep set of colors in one place and replace all occurrences
 *   in easy way if you need. Of course you are not obligated to use only
 *   one palette - you can define as many palettes as you want to.
 */
//=============================================================================
static const oC_WidgetScreen_Palette_t  Palette = {
    //
    //  Palette to use when the widget is ready to use
    //
    [WidgetState_Ready] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Activated] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Pressed] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x1A090D ,
           .BorderColor = 0xA8BA9A ,
    } ,
    [WidgetState_Hovered] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0x6B6570 ,
    } ,
    [WidgetState_Disabled] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xACE894 ,
           .FillColor   = 0x6B6570 ,
           .BorderColor = 0x6B6570 ,
    } ,
};

//=============================================================================
/*
 *                           === DRAW STYLE ===
 *
 *          Array with a style for drawing of the widget screen. 
 *          It is defined for every state of the widget, so you can
 *          define different styles for different states.
 */
//=============================================================================
static const oC_WidgetScreen_DrawStyle_t DrawStyle = {
    [WidgetState_Ready] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Dashed ,
           .BorderWidth = 1 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Pressed] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Activated] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Hovered] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Disabled] = {
           .BorderStyle = BorderStyle_None ,
           .BorderWidth = 0 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
};

//! [push_button_handler]
//=============================================================================
/*
 *                          === PRINT BUTTON HANDLER ===
 * 
 *         The handler for the print button. It will print a message
 *         when the button is pressed.
 * 
 */
//=============================================================================
static oC_WidgetScreen_ScreenId_t PrintButtonHandler( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen )
{
    printf("Button pressed\n");
    return ScreenID;
}
//! [push_button_handler]

//! [exit_button_handler]
//=============================================================================
/*
 *                          === EXIT BUTTON HANDLER ===
 * 
 *         The handler for the exit button. It will exit from the program
 *         when the button is pressed.
 * 
 */
//=============================================================================
static oC_WidgetScreen_ScreenId_t ExitButtonHandler( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen )
{
    return 0xFF; // Return invalid screen ID to exit from the program
}
//!< [exit_button_handler]

//! [main_menu_widgets]
//=============================================================================
/*
 *                           === MAIN MENU WIDGETS ===
 *
 *          The definition of the widgets that should be displayed on the main menu screen.
 *          You can specify the position, size, palette, draw style, and handlers for the widgets.
 * 
 *          Please note, that this is the array so you can define
 *          as many widgets as you need to.
 */
//=============================================================================
static const oC_WidgetScreen_WidgetDefinition_t MainMenuWidgets[] = {
        {
             .Type                          = oC_WidgetScreen_WidgetType_PushButton,
             .Position                      = { .X = (140), .Y = 50 },
             .Width                         = 200,
             .Height                        = 80,
             .ZPosition                     = 1,
             .Palette                       = &Palette,
             .DrawStyle                     = &DrawStyle,
             .Handlers                      = {
                     [WidgetState_Pressed] = PrintButtonHandler,
             },
             .String                       = {
                 .DefaultString           = "Print",
                 .Font                    = oC_Font_(Consolas),
                 .TextAlign               = TextAlign_Center ,
                 .VerticalTextAlign       = VerticalTextAlign_Center ,
                 .UpdateStringHandler     = NULL,
             },
             .TypeSpecific                  = {
                   .PushButton = {
                       .Type = PushButtonType_Standard,
                   },
             },
        },
        {
             .Type                          = oC_WidgetScreen_WidgetType_PushButton,
             .Position                      = { .X = (140), .Y = 140 },
             .Width                         = 200,
             .Height                        = 80,
             .ZPosition                     = 1,
             .Palette                       = &Palette,
             .DrawStyle                     = &DrawStyle,
             .Handlers                      = {
                     [WidgetState_Pressed] = ExitButtonHandler,
             },
             .String                       = {
                 .DefaultString           = "Exit",
                 .Font                    = oC_Font_(Consolas),
                 .TextAlign               = TextAlign_Center ,
                 .VerticalTextAlign       = VerticalTextAlign_Center ,
                 .UpdateStringHandler     = NULL,
             },
             .TypeSpecific                  = {
                   .PushButton = {
                       .Type = PushButtonType_Standard,
                   },
             },
        },
};
//! [main_menu_widgets]

//=============================================================================
/*
 *                           === MENU SCREENS ===
 *
 *          The definition of the screen with the main menu.
 *          You can specify the position, size, background color,
 *          and the widgets that should be displayed on the screen.
 * 
 *          Please note, that this is the array so you can define
 *          as many screens as you need to.
 */
//=============================================================================
static const oC_WidgetScreen_ScreenDefinition_t MenuScreens[] = {
        {
             .Position                      = { .X = 0, .Y = 0 },
             .Width                         = 480 ,
             .Height                        = 272 ,
             .BackgroundColor               = 0x4A314D,
             .ColorFormat                   = oC_ColorFormat_RGB888,
             .PrepareHandler                = NULL,
             .Handler                       = NULL,
             .WidgetsDefinitions            = MainMenuWidgets,
             .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(MainMenuWidgets)
        },
};

//==========================================================================================================================================
/*
 * Main entry of the program. Parameters are not required
 */
//==========================================================================================================================================
int main()
{
    oC_WidgetScreen_HandleScreens(NULL, MenuScreens, oC_ARRAY_SIZE(MenuScreens), NULL);
    return 0;
}
