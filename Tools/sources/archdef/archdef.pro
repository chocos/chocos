#-------------------------------------------------
#
# Project created by QtCreator 2015-11-02T19:44:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = archdef
TEMPLATE = app


SOURCES += main.cpp\
        architecturedefiner.cpp

HEADERS  += architecturedefiner.h

FORMS    += architecturedefiner.ui
