#include "architecturedefiner.h"
#include "ui_architecturedefiner.h"

ArchitectureDefiner::ArchitectureDefiner(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ArchitectureDefiner)
{
    ui->setupUi(this);
}

ArchitectureDefiner::~ArchitectureDefiner()
{
    delete ui;
}
