#ifndef ARCHITECTUREDEFINER_H
#define ARCHITECTUREDEFINER_H

#include <QMainWindow>

namespace Ui {
class ArchitectureDefiner;
}

class ArchitectureDefiner : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArchitectureDefiner(QWidget *parent = 0);
    ~ArchitectureDefiner();

private:
    Ui::ArchitectureDefiner *ui;
};

#endif // ARCHITECTUREDEFINER_H
