#include "architecturedefiner.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ArchitectureDefiner w;
    w.show();

    return a.exec();
}
