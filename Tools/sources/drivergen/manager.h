#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QStringList>
#include <QSettings>

#define TEMPLATES_PATH                  "templates_path"
#define DRIVERS_INC_PATH                "drivers_inc_path"
#define DRIVERS_SRC_PATH                "drivers_src_path"
#define LLD_INC_PATH                    "lld_inc_path"
#define LLD_SRC_PATH                    "lld_src_path"
#define CONFIG_PATH                     "config_path"

#define DRIVER_FILES_ARRAY_NAME         "driver_files"
#define LLD_FILES_ARRAY_NAME            "lld_files"

#define FILE_TEMPLATE_TAG               "template"
#define FILE_OUTPUT_TAG                 "output"

#define DRIVER_NAME_REPLACE_TAG         "<dname>"
#define PRODUCENT_NAME_REPLACE_TAG      "<producent>"
#define FAMILY_NAME_REPLACE_TAG         "<family>"
#define MACHINE_NAME_REPLACE_TAG        "<machine>"

#define DRIVER_NAME_REG_EXP             "\\w+"

#ifdef __linux__
#define TMPLPARS_PROGRAM_NAME           "./tmplpars"
#else
#define TMPLPARS_PROGRAM_NAME           "tmplpars.exe"
#endif
class Manager
{

    //==============================================================================================
    //
    //  PRIVATE VARIABLES SECTION
    //
    //==============================================================================================
    private:

        QString                 cv_DriverName;          /**< Name of the driver to create */
        bool                    cv_WithLLD;             /**< Flag if should create LLD layer also */
        QString                 cv_ProducentName;       /**< Name of the producent if LLD is active */
        QString                 cv_FamilyName;          /**< Name of the family if LLD is active */
        QString                 cv_MachineName;         /**< Name of the machine if LLD is active */
        QStringList             cv_Variables;           /**< Additional variables put into the template */

        QString                 cv_TemplatesPath;       /**< Path to the templates */
        QString                 cv_DriversIncPath;      /**< Path to the driver include destination */
        QString                 cv_DriversSrcPath;      /**< Path to the driver source destination */
        QString                 cv_LLDIncPath;          /**< Path to the lld layer include destination */
        QString                 cv_LLDSrcPath;          /**< Path to the lld layer source destination */
        QString                 cv_ConfigPath;          /**< Path to the configuration directory */

        QMap<QString,QString>   cv_DriverIncFiles;      /**< List of the include files to create in driver layer */
        QMap<QString,QString>   cv_DriverSrcFiles;      /**< List of the source files to create in driver layer */
        QMap<QString,QString>   cv_LLDIncFiles;         /**< List of the include files to create in lld layer */
        QMap<QString,QString>   cv_LLDSrcFiles;         /**< List of the source files to create in lld layer */

    //==============================================================================================
    //
    //  PUBLIC METHODS SECTION
    //
    //==============================================================================================
    public:

        Manager();
        bool                    ParseArguments          ( QStringList Arguments );
        bool                    LoadSettings            ( QString FileName );
        bool                    CreatePaths             ();
        bool                    CheckProgramFields      ();
        bool                    CreateDriverFiles       ();
        bool                    CreateLLDFiles          ();

    //==============================================================================================
    //
    //  PRIVATE METHODS SECTION
    //
    //==============================================================================================
    private:

        bool                    PrintHelp               ();
        bool                    CheckPathExists         ( QString Path , QString Msg );
        bool                    CheckDriverName         ( QString DriverName );
        bool                    CheckFilesList          ( QMap<QString,QString> & Files , QString Msg );
        bool                    CheckFileExists         ( QString FilePath );
        QString                 ReadValueFromSettings   ( QSettings & Settings , QString key );
        QMap<QString,QString>   ReadFilesFromSettings   ( QSettings & Settings , QString ArrayName );
        bool                    CreatePath              ( QString Path , QString Msg );
        bool                    RunProgram              ( QString Name , QStringList Arguments );
        bool                    CreateFileFromTemplate  ( QString TemplateFilePath , QString OutputFilePath );
        bool                    CreateFileFromTemplate  ( QString TemplateFileName , QString OutputFileName , QString OutputPath );
        bool                    CreateFiles             ( QMap<QString,QString> Files , QString OutputPath );
        bool                    SortFiles               ( QMap<QString,QString> & Files ,  QMap<QString,QString> & Headers ,  QMap<QString,QString> & Sources );
        QString                 ReplaceDriverNameTag    (QString String );
        QString                 ReplaceProducentNameTag (QString String );
        QString                 ReplaceFamilyNameTag    (QString String );
        QString                 ReplaceMachineNameTag   (QString String );
};

#endif // MANAGER_H
