#include <QCoreApplication>
#include <manager.h>
#include <console.h>

#define SETTINGS_FILE_NAME      "drivergen.ini"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Manager Man;

    if (
            Man.ParseArguments ( a.arguments ())
        &&  Man.LoadSettings ( SETTINGS_FILE_NAME )
        &&  Man.CreatePaths ()
        &&  Man.CheckProgramFields ()
        &&  Man.CreateDriverFiles()
        &&  Man.CreateLLDFiles()
        )
    {
        PrintGreen ( "Driver successfully generated!" );
    }
    else
    {
        PrintError ( "Cannot create the driver!" );
    }

    return 0;
}
