#-------------------------------------------------
#
# Project created by QtCreator 2014-10-17T15:24:12
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = drivergen
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    manager.cpp

HEADERS += \
    console.h \
    manager.h

RESOURCES += \
    resources.qrc
