#include "manager.h"
#include <console.h>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QDir>
#include <QRegExp>
#include <QProcess>

//==================================================================================================
/**
 *  The constructor of the object
 */
//==================================================================================================
Manager::Manager(  )
{
    cv_WithLLD  = false;
}

//==================================================================================================
/**
 * The function prints help file
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::PrintHelp ()
{
    QFile file( ":/cmd/help" );

    if ( file.open ( QFile::ReadOnly ) )
    {
        QTextStream txt(&file);

        PrintDefault ( txt.readAll ().toStdString () );

        return true;
    }
    else
    {
        return false;
    }
}

//==================================================================================================
/**
 *
 * The function checks if the Path directory is correct and exists
 *
 * @param Path      a path to check if exists
 * @param Msg       a message to print if path not exists
 *
 * @return true if exists
 */
//==================================================================================================
bool Manager::CheckPathExists( QString Path , QString Msg )
{
    QDir dir( Path );

    if ( !dir.exists ( "." ) )
    {
        PrintError ( QString(Msg).arg (Path).toStdString ());
        return false;
    }
    else
    {
        return true;
    }
}

//==================================================================================================
/**
 *
 * The function checks if driver name is correctly set
 *
 * @return true if driver name is correct
 */
//==================================================================================================
bool Manager::CheckDriverName ( QString DriverName )
{
    bool result;

    QRegExp RegExp( DRIVER_NAME_REG_EXP );

    if ( !RegExp.exactMatch ( DriverName ) )
    {
        PrintError ( QString("The name of the driver %1 is not correct! The name must matches a word characters").arg (DriverName).toStdString ());
        result  = false;
    }
    else
    {
        result  = true;
    }

    return result;
}

//==================================================================================================
/**
 *
 * The function checks if the list of the files is correct
 *
 * @param Files         The list of the files in format template_file_name=>output_file_name
 * @param Msg           A message to print when the list is not correct
 *
 * @return true if list is correct
 */
//==================================================================================================
bool Manager::CheckFilesList( QMap<QString,QString> & Files , QString Msg )
{
    bool result;

    if ( Files.isEmpty () )
    {
        PrintError ( Msg.toStdString ());
        result = false;
    }
    else
    {
        result  = true;
    }

    return result;
}

//==================================================================================================
/**
 * The function checks if the file exists
 *
 * @param FilePath      The path to the file to check if exists
 *
 * @return true if exists
 */
//==================================================================================================
bool Manager::CheckFileExists(QString FilePath)
{
    QFile file(FilePath);

    return file.exists();
}

//==================================================================================================
/**
 *
 * The function reads a value of the key in a settings file
 *
 * @param Settings  Reference to the settings object
 * @param key       key to read
 *
 * @return string of the value from settings
 */
//==================================================================================================
QString Manager::ReadValueFromSettings( QSettings & Settings , QString key )
{
    QString value = ReplaceDriverNameTag( Settings.value ( key ).toString() );

    value = ReplaceProducentNameTag(value);
    value = ReplaceFamilyNameTag(value);
    value = ReplaceMachineNameTag(value);

    return  value;
}

//==================================================================================================
/**
 *
 * The function reads files from the settings
 *
 * @param Settings          Reference to the openned settings object
 * @param ArrayName         Name of the array in ini file
 *
 * @return Map of the files in format template=>output
 */
//==================================================================================================
QMap<QString,QString> Manager::ReadFilesFromSettings( QSettings & Settings , QString ArrayName )
{
    QMap<QString , QString> Map;

    int size = Settings.beginReadArray ( ArrayName );

    for( int i = 0 ; i < size ; i++ )
    {
        Settings.setArrayIndex (i);
        Map[ ReplaceDriverNameTag(Settings.value ( FILE_TEMPLATE_TAG ).toString ()) ] = ReplaceDriverNameTag(Settings.value ( FILE_OUTPUT_TAG ).toString () );
    }

    Settings.endArray();

    return Map;
}

//==================================================================================================
/**
 *
 * The function creates a the path directory and print error message when path cannot be created
 *
 * @param Path      A path to create a directory
 * @param Msg       A message to print if cannot create a path
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::CreatePath (QString Path , QString Msg)
{
    bool result;

    QDir dir( Path );

    if ( dir.exists () )
    {
        result = true;
    }
    else if ( !dir.mkpath ( "." ) )
    {
        PrintError ( Msg.arg (Path).toStdString () );
        result = false;
    }
    else
    {
        result = true;
    }

    return result;
}

//==================================================================================================
/**
 * The function run an external program located in the program directory
 *
 * @param Name              A name of the program to run
 * @param Arguments         Arguments for a program
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::RunProgram(QString Name, QStringList Arguments)
{

    QString command = Name;

    foreach(QString arg , Arguments)
    {
        command.push_back( QString(" ") + arg );
    }

    system(command.toStdString().c_str());

    return true;
}

//==================================================================================================
/**
 * The function creates a new file according to the template
 *
 * @param TemplateFilePath  The path to a template file
 * @param OutputFilePath    The destination path for the output file
 *
 * @return true if success, false if already exists
 */
//==================================================================================================
bool Manager::CreateFileFromTemplate(QString TemplateFilePath, QString OutputFilePath)
{
    bool result = false;
    QStringList arguments;

    arguments << TemplateFilePath;
    arguments << OutputFilePath;
    arguments << cv_Variables;

    if ( !CheckFileExists( TemplateFilePath ) )
    {
        PrintError( QString("Error! The template file '%1' not exists!!").arg(TemplateFilePath).toStdString() );
        result = false;
    }
    else if ( CheckFileExists( OutputFilePath ) )
    {
        PrintError( QString("Error! The output file '%1' already exists!!").arg(OutputFilePath).toStdString() );
        result = false;
    }
    else
    {
        result = RunProgram( TMPLPARS_PROGRAM_NAME , arguments );
    }

    return result;
}

//==================================================================================================
/**
 * The function creates a new file according to the template file. It also creates a paths for the
 * files.
 *
 * @param TemplateFileName      The name of a template file
 * @param OutputFileName        The name of the output file
 * @param OutputPath            a destination path for the output file
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::CreateFileFromTemplate(QString TemplateFileName, QString OutputFileName, QString OutputPath)
{
    QString templateFilePath = cv_TemplatesPath + TemplateFileName;
    QString outputFilePath   = OutputPath + OutputFileName;

    return CreateFileFromTemplate( templateFilePath , outputFilePath );
}


//==================================================================================================
/**
 * The function creates files for the given map
 *
 * @param Files                 The map of the files to create, where the key is name of a template
 *                              file and value is name of a output file
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::CreateFiles(QMap<QString, QString> Files, QString OutputPath )
{
    bool result = true;

    QMapIterator<QString , QString> i(Files);

    while( i.hasNext() && result )
    {
        i.next();
        result = result && CreateFileFromTemplate( i.key() , i.value() , OutputPath );
    }

    return result;
}

//==================================================================================================
/**
  * The function sorts files to headers and sources according to the output file name (if .h then it is header)
  *
  * @param FileSystemType       The map of the files, where the key is name of a template file,
  *                             and the value is name of an output file
  * @param Headers              Output map, where will be moved header files from the Files map
  * @param Sources              Output map, where will be moved source files from the Files map
  *
  * @return true if success
  */
//==================================================================================================
bool Manager::SortFiles( QMap<QString,QString> & Files ,  QMap<QString,QString> & Headers ,  QMap<QString,QString> & Sources )
{
    bool result = true;

    QMapIterator<QString , QString> i(Files);

    while( i.hasNext() && result )
    {
        i.next();

        QString templateFileName = i.key();
        QString outputFileName   = i.value();

        if(outputFileName.contains(".h"))
        {
            Headers[templateFileName] = outputFileName;
        }
        else if(outputFileName.contains(".c") || outputFileName.contains(".cpp"))
        {
            Sources[templateFileName] = outputFileName;
        }
        else
        {
            PrintError( QString("Unrecognized file type '%1'").arg(outputFileName).toStdString() );
            result = false;
        }
    }

    return result;
}

//==================================================================================================
/**
 * The function replace a driver name tag to real driver name
 *
 * @param String                The string where driver name tag should be replaced
 *
 * @return reference to the replaced string
 */
//==================================================================================================
QString Manager::ReplaceDriverNameTag(QString String)
{
    return String.replace (DRIVER_NAME_REPLACE_TAG , cv_DriverName.toLower ());
}

//==================================================================================================
/**
 * The function replace a producent name tag to real producent name
 *
 * @param String                The string where tag should be replaced
 *
 * @return reference to the replaced string
 */
//==================================================================================================
QString Manager::ReplaceProducentNameTag(QString String)
{
    return (cv_WithLLD) ? String.replace (PRODUCENT_NAME_REPLACE_TAG , cv_ProducentName.toLower()) : String;
}

//==================================================================================================
/**
 * The function replace a family name tag to real producent name
 *
 * @param String                The string where tag should be replaced
 *
 * @return reference to the replaced string
 */
//==================================================================================================
QString Manager::ReplaceFamilyNameTag(QString String)
{
    return (cv_WithLLD) ? String.replace (FAMILY_NAME_REPLACE_TAG , cv_FamilyName.toLower()) : String;
}

//==================================================================================================
/**
 * The function replace a machine name tag to real producent name
 *
 * @param String                The string where tag should be replaced
 *
 * @return reference to the replaced string
 */
//==================================================================================================
QString Manager::ReplaceMachineNameTag(QString String)
{
    return (cv_WithLLD) ? String.replace (MACHINE_NAME_REPLACE_TAG , cv_MachineName.toLower()) : String;
}

//==================================================================================================
/**
 * @brief
 *
 *  The function for parsing arguments of the program
 *
 * @param Arguments         List of the arguments with the name of the program at 0 index
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::ParseArguments( QStringList Arguments )
{
    bool result = false;
    int  argCnt = Arguments.count ();

    if( argCnt >= 3 )
    {
        cv_DriverName   = Arguments[1];
        cv_WithLLD      = (Arguments[2].toLower () == "true") ? true : false;

        if((cv_WithLLD && argCnt >= 6) || cv_WithLLD == false)
        {
            int argumentIndex = 3;

            if(cv_WithLLD)
            {
                cv_ProducentName = Arguments[argumentIndex++];
                cv_FamilyName    = Arguments[argumentIndex++];
                cv_MachineName   = Arguments[argumentIndex++];

                cv_Variables.push_back( QString("PRODUCENT=%1").arg(cv_ProducentName) );
                cv_Variables.push_back( QString("FAMILY=%1").arg(cv_FamilyName) );
                cv_Variables.push_back( QString("MACHINE=%1").arg(cv_MachineName) );
            }

            cv_Variables.push_back( (cv_WithLLD) ? "LLD_ACTIVE=true" : "LLD_ACTIVE=false" );
            cv_Variables.push_back( QString("DRIVER_NAME=%1").arg(cv_DriverName.toUpper()) );
            cv_Variables.push_back( QString("driver_name=%1").arg(cv_DriverName.toLower()) );

            for( argumentIndex = 3 ; (argumentIndex < argCnt) ; argumentIndex++ )
            {
                cv_Variables.push_back ( Arguments[argumentIndex] );
            }

            result = true;
        }
    }

    if ( !result && !PrintHelp () )
    {
        PrintError ("Cannot print help file!");
    }

    return result;
}

//==================================================================================================
/**
 * The function loads settings from a ini file
 *
 * @param FileName      Name of the ini file to read
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::LoadSettings( QString FileName )
{
    QMap<QString,QString>   driverFiles;
    QMap<QString,QString>   lldFiles;
    bool result = true;
    QSettings Settings( FileName , QSettings::IniFormat );

    cv_TemplatesPath        = ReadValueFromSettings( Settings , TEMPLATES_PATH );
    cv_ConfigPath           = ReadValueFromSettings( Settings , CONFIG_PATH );
    cv_DriversIncPath       = ReadValueFromSettings( Settings , DRIVERS_INC_PATH );
    cv_DriversSrcPath       = ReadValueFromSettings( Settings , DRIVERS_SRC_PATH );
    cv_LLDIncPath           = ReadValueFromSettings( Settings , LLD_INC_PATH );
    cv_LLDSrcPath           = ReadValueFromSettings( Settings , LLD_SRC_PATH );

    driverFiles             = ReadFilesFromSettings( Settings , DRIVER_FILES_ARRAY_NAME );
    lldFiles                = ReadFilesFromSettings( Settings , LLD_FILES_ARRAY_NAME );

    result = SortFiles(driverFiles,cv_DriverIncFiles,cv_DriverSrcFiles) &&
             SortFiles(lldFiles,cv_LLDIncFiles,cv_LLDSrcFiles);

    return result;
}

//==================================================================================================
/**
 * The function creates paths for the driver
 *
 * @return true if success
 */
bool Manager::CreatePaths ()
{
    bool result = true;

    result  = result && CreatePath( cv_DriversIncPath   , "Error! The driver include path '%1' cannot be created!" );

    if(cv_WithLLD)
    {
        result  = result && CreatePath( cv_LLDIncPath , "Error! The lld include path '%1' cannot be created!" );
        result  = result && CreatePath( cv_LLDSrcPath , "Error! The lld source path '%1' cannot be created!" );
    }

    result  = result && CreatePath( cv_DriversSrcPath   , "Error! The driver source path '%1' cannot be created!" );
    result  = result && CreatePath( cv_ConfigPath       , "Error! The config path '%1' cannot be created!" );

    return result;
}

//==================================================================================================
/**
 * The function checks if any field of the program is correctly given
 *
 * @return true if all fields are correct
 */
//==================================================================================================
bool Manager::CheckProgramFields ()
{
    bool result = true;

    result = result && CheckDriverName( cv_DriverName );

    result = result && CheckPathExists( cv_TemplatesPath , "Error! The templates path '%1' not exists!" );
    result = result && CheckPathExists( cv_ConfigPath    , "Error! The config path '%1' not exists!" );
    result = result && CheckPathExists( cv_DriversIncPath, "Error! The drivers include path '%1' not exists!" );
    result = result && CheckPathExists( cv_DriversSrcPath, "Error! The drivers source path '%1' not exists!" );

    if(cv_WithLLD)
    {
        result = result && CheckPathExists( cv_LLDIncPath    , "Error! The LLD include path '%1' not exists!" );
        result = result && CheckPathExists( cv_LLDSrcPath    , "Error! The LLD source path '%1' not exists!" );
    }

    return result;
}

//==================================================================================================
/**
 * The function creates a files for a driver layer acording to the templates
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::CreateDriverFiles()
{
    return CreateFiles( cv_DriverIncFiles , cv_DriversIncPath ) && CreateFiles( cv_DriverSrcFiles , cv_DriversSrcPath );
}

//==================================================================================================
/**
 * The function creates a files for a LLD layer acording to the templates
 *
 * @return true if success
 */
//==================================================================================================
bool Manager::CreateLLDFiles()
{
    return !cv_WithLLD || (CreateFiles( cv_LLDIncFiles , cv_LLDIncPath ) && CreateFiles( cv_LLDSrcFiles , cv_LLDSrcPath ) );
}
