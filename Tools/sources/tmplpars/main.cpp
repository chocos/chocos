#include <QCoreApplication>
#include <templateparser.h>
#include <console.h>
#include <QTextStream>
#include <QFileInfo>

struct Properties_t
{
        QString ProgramName;
        QString TemplateFileName;
        QString OutputFileName;
        QMap<QString , QString> Variables;
        bool    Properties;
};

void ShowUsage()
{
    QFile HelpFile( ":/cmd/usage" );

    if ( HelpFile.open( QFile::ReadOnly ) )
    {
        QTextStream HelpStream(&HelpFile);

        PrintDefault( HelpStream.readAll().toStdString() );
    }
    else
    {
        PrintError("File with usage not found!");
    }
}

void ShowHelp()
{
    QFile HelpFile( ":/cmd/help" );

    if ( HelpFile.open( QFile::ReadOnly ) )
    {
        QTextStream HelpStream(&HelpFile);

        PrintDefault( HelpStream.readAll().toStdString() );
    }
    else
    {
        PrintError("File with help not found!");
    }
}

bool CheckProperties( Properties_t & Properties )
{
    bool result;
    if ( Properties.TemplateFileName.isEmpty() )
    {
        PrintError("The template path is not given! (type --help for more)");
        result = false;
    }
    else if ( Properties.OutputFileName.isEmpty() )
    {
        PrintError("The output path is not given! (type --help for more)");
        result = false;
    }
    else
    {
        result = true;
    }

    return result;
}


Properties_t ReadProperties( QStringList arguments )
{
    Properties_t Properties;

    if ( arguments.contains( "--help" ) )
    {
        ShowHelp();
    }
    else
    {
        Properties.ProgramName      = arguments.value(0);
        Properties.TemplateFileName = arguments.value(1);
        Properties.OutputFileName   = arguments.value(2);

        int cnt = arguments.count();

        for(int i = 3 ; i < cnt ; i++)
        {
            QStringList splitted    = arguments.value(i).split("=");

            if ( splitted.count() == 2 )
            {
                QString value = splitted[1];

                if(value.endsWith(']') && value.startsWith('['))
                {
                    QString fileName = value;

                    fileName = fileName.remove('[');
                    fileName = fileName.remove(']');

                    QFile file(fileName);
                    QFileInfo fileInfo(file);
                    QTextStream textStream(&file);

                    if(file.exists())
                    {
                        if(file.open(QFile::ReadOnly))
                        {
                            QByteArray bytes = file.readAll();

                            file.reset();

                            Properties.Variables[ splitted[0] ]                         = textStream.readAll();
                            Properties.Variables[ QString("%1:SIZE").arg(splitted[0]) ] = QString("%1").arg(file.size());
                            Properties.Variables[ QString("%1:NAME").arg(splitted[0]) ] = fileInfo.fileName();

                            for(qint64 byteIndex = 0; byteIndex < file.size(); byteIndex++)
                            {
                                quint8 byte = bytes.at(byteIndex);
                                Properties.Variables[ QString("%1:%2").arg(splitted[0]).arg(byteIndex) ]           = bytes.at(byteIndex);
                                Properties.Variables[ QString("%1:HEX%2").arg(splitted[0]).arg(byteIndex) ]        = QString("0x%1").arg(byte,2,16,QLatin1Char('0'));
                                Properties.Variables[ QString("%1:0x%2").arg(splitted[0]).arg(byteIndex,0,16) ]    = bytes.at(byteIndex);
                                Properties.Variables[ QString("%1:HEX0x%2").arg(splitted[0]).arg(byteIndex,0,16) ] = QString("0x%1").arg(byte,2,16,QLatin1Char('0'));
                            }
                        }
                        else
                        {
                            PrintError(QString("Cannot open file %1!").arg(fileName).toStdString());
                        }
                    }
                    else
                    {
                        PrintWarning(QString("File %1 does not exist!").arg(fileName).toStdString());
                    }
                }
                else
                {
                    Properties.Variables[ splitted[0] ] = splitted[1];
                }
            }
        }
    }

    return Properties;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Properties_t Properties = ReadProperties( a.arguments() );


    if ( CheckProperties( Properties ) )
    {
        TemplateParser * T = new TemplateParser( Properties.TemplateFileName , Properties.OutputFileName , Properties.Variables);
    }
    else
    {
        ShowUsage();
    }

    return 0;
}
