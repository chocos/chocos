#ifndef CONSOLE_H
#define CONSOLE_H

#ifdef __linux__
#include <iostream>

/**
 * @brief
 *
 * The type represents color of the text in the console
 */
enum ConsoleTextColor_t
{
    ConsoleTextColor_DARK_BLUE      = 94 ,
    ConsoleTextColor_DARK_GREEN     = 32 ,
    ConsoleTextColor_AQUA           = 35 ,
    ConsoleTextColor_DARK_RED       = 31 ,
    ConsoleTextColor_DARK_PINK      = 95 ,
    ConsoleTextColor_GOLD           = 33 ,
    ConsoleTextColor_DEFAULT        = 39 ,
    ConsoleTextColor_GRAY           = 90 ,
    ConsoleTextColor_LIGHT_BLUE     = 34 ,
    ConsoleTextColor_LIGHT_GREEN    = 32 ,
    ConsoleTextColor_CYAN           = 36 ,
    ConsoleTextColor_LIGHT_RED      = 31 ,
    ConsoleTextColor_LIGHT_PINK     = 31 ,
    ConsoleTextColor_YELLOW         = 33 ,
    ConsoleTextColor_WHITE          = 37 ,
    ConsoleTextColor_RED            = 31 ,
    ConsoleTextColor_GREEN          = 32 ,
    ConsoleTextColor_BLUE           = 94
};

/**
 * @brief
 *
 * The function to set new text color in the console
 *
 * @param color
 */
static inline void SetConsoleTextColor( ConsoleTextColor_t color )
{
    std::cout << "\033[" << color << "m";
}

/**
 * @brief
 *
 * The function print red color msg
 *
 * @param msg
 */
static inline void PrintError( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_RED );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}

/**
 * @brief
 *
 * The function print message in a yellow color
 *
 * @param msg
 */
static inline void PrintWarning( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_YELLOW );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}

/**
 * @brief
 *
 * The function print message in a green color
 *
 * @param msg
 */
static inline void PrintGreen( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_GREEN );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}


/**
 * @brief
 *
 * The function print message in a default color
 *
 * @param msg
 */
static inline void PrintDefault( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}


/**
 * @brief
 *
 * The function print message in a selected color
 *
 * @param msg           message to print
 * @param color         color of the message to print
 */
static inline void PrintInColor( std::string msg , ConsoleTextColor_t color)
{
    SetConsoleTextColor( color );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}
#else
#include <windows.h>
#include <iostream>

/**
 * @brief
 *
 * The type represents color of the text in the console
 */
enum ConsoleTextColor_t
{
    ConsoleTextColor_DARK_BLUE      = 1 ,
    ConsoleTextColor_DARK_GREEN     = 2 ,
    ConsoleTextColor_AQUA           = 3 ,
    ConsoleTextColor_DARK_RED       = 4 ,
    ConsoleTextColor_DARK_PINK      = 5 ,
    ConsoleTextColor_GOLD           = 6 ,
    ConsoleTextColor_DEFAULT        = 7 ,
    ConsoleTextColor_GRAY           = 8 ,
    ConsoleTextColor_LIGHT_BLUE     = 9 ,
    ConsoleTextColor_LIGHT_GREEN    = 10 ,
    ConsoleTextColor_CYAN           = 11 ,
    ConsoleTextColor_LIGHT_RED      = 12 ,
    ConsoleTextColor_LIGHT_PINK     = 13 ,
    ConsoleTextColor_YELLOW         = 14 ,
    ConsoleTextColor_WHITE          = 15 ,
    ConsoleTextColor_RED        = FOREGROUND_RED ,
    ConsoleTextColor_GREEN      = FOREGROUND_GREEN ,
    ConsoleTextColor_BLUE       = FOREGROUND_BLUE
};

/**
 * @brief
 *
 * The function to set new text color in the console
 *
 * @param color
 */
static inline void SetConsoleTextColor( ConsoleTextColor_t color )
{
    SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ) , (WORD)color );
}

/**
 * @brief
 *
 * The function print red color msg
 *
 * @param msg
 */
static inline void PrintError( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_RED );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}

/**
 * @brief
 *
 * The function print message in a yellow color
 *
 * @param msg
 */
static inline void PrintWarning( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_YELLOW );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}

/**
 * @brief
 *
 * The function print message in a green color
 *
 * @param msg
 */
static inline void PrintGreen( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_GREEN );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}


/**
 * @brief
 *
 * The function print message in a default color
 *
 * @param msg
 */
static inline void PrintDefault( std::string msg )
{
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}


/**
 * @brief
 *
 * The function print message in a selected color
 *
 * @param msg           message to print
 * @param color         color of the message to print
 */
static inline void PrintInColor( std::string msg , ConsoleTextColor_t color)
{
    SetConsoleTextColor( color );
    std::cout << msg << std::endl;
    SetConsoleTextColor( ConsoleTextColor_DEFAULT );
}

#endif
#endif // CONSOLE_H
