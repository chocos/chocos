=================================================================================================
===                                                                                           ===
===                                                                                           ===
===                     Smart Template Parser                                                 ===
===                                                                                           ===
===                     Patryk Kubiak    2016                                                 ===
===                                                                                           ===
===                     Ver. 1.1.0                                                            ===
===                                                                                           ===
=================================================================================================
===                                                                                           ===
===     Usage:                                                                                ===
===                                                                                           ===
===             %1 [template_path] [output_path] [variables]                                  ===
===                                                                                           ===
===                [template_path] - path to the template file                                ===
===                [output_path]   - path to the output file                                  ===
===                [variables]     - list of the variables in format [variable]=[value]       ===
===                                  or [array:index]=[value]                                 ===
===                                                                                           ===
===    Type --help for more information                                                       ===
===                                                                                           ===
=================================================================================================
