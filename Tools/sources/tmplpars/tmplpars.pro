#-------------------------------------------------
#
# Project created by QtCreator 2014-10-17T16:03:22
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = tmplpars
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += static

TEMPLATE = app


SOURCES += main.cpp \
    templateparser.cpp

HEADERS += \
    templateparser.h \
    console.h

RESOURCES += \
    resources.qrc
