#!/bin/bash
#
#       Builds the project inside the docker container
#

#
#   Path to the directory with this script
#
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PROJECT_DIR=$THIS_DIR
THIS_SCRIPT=$PROJECT_DIR/build.sh


#
#   Path to the configuration file
#
CONFIGURATION_FILE_PATH=~/.choco-scripts.cfg
SCRIPT_DESCRIPTION=""

#
#   Installs choco-scripts
#
function installChocoScripts()
{
    echo "Installation of the choco-scripts"

    # This line installs wget tool - you don't need to use it if you already have it
    apt-get update && apt-get install -y wget

    # This downloads an installation script and run it 
    wget -O - https://release.choco-technologies.com/scripts/install-choco-scripts.sh | bash
}

#
#   Verification of the choco scripts installation
#
if [ -f "$CONFIGURATION_FILE_PATH" ]
then 
    source $CONFIGURATION_FILE_PATH
else 
    printf "\033[31;1mChoco-Scripts are not installed for this user\033[0m\n\n"
    printf "      \033[37;1mYou can find the installation instruction here: \033[0m\n"
    printf "            \033[34;1mhttps://bitbucket.org/chocotechnologies/scripts/src/master/\033[0m\n\n"

    while true
    do
        read -p "Do you want to try to auto-install it? [Y/n]: " answer
        case $answer in 
            [Yy]* ) installChocoScripts; break;;
            [Nn]* ) echo "Skipping installation"; exit 1;;
            * ) echo "Please answer Y or n";;
        esac
    done
    exit 1
fi

#
#   Information message
#
echo "Using choco-scripts from path $CHOCO_SCRIPTS_PATH in version $CHOCO_SCRIPTS_VERSION"

#
#   Importing of the framework main script
#
source $(getChocoScriptsPath)

#
#   Builds the project inside the docker container 
#
function build()
{
    source "$SET_PATHS_FILE_PATH" skip-checking-arch
    if isStringEqual "$DONT_USE_DOCKER" "FALSE"
    then 
        "$RUN_CONTAINER_FILE_PATH" -w="$SOURCE_DIR" --machine-name="$MACHINE_NAME" --prepare-for-device=$FLASH --interactive-container=false --command-file="$THIS_SCRIPT" --command-args="--dont-use-docker --machine-name=$MACHINE_NAME"
    else 
        if ! isStringEmpty "$MACHINE_NAME"
        then 
            source "$SEARCH_ARCH_FILE_PATH" "$MACHINE_NAME"
        fi
        "$BUILD_FILE_PATH"
    fi
}

#
#   Connects to the device
#
function connectToDevice()
{
    printInfo "Connecting to the device \n"
    "$THIS_DIR/$FLASH_FILE_NAME" --dont-use-docker="$DONT_USE_DOCKER" --connect=$CONNECT --machine-name="$MACHINE_NAME" --dont-disconnect
}

#
#   Flashes device
#
function flash()
{
    printInfo "Flashing device \n"
    "$THIS_DIR/$FLASH_FILE_NAME" --dont-use-docker="$DONT_USE_DOCKER" --connect=$CONNECT --machine-name="$MACHINE_NAME" --flash --gdb-mode=$GDB_MODE
}

#
#   The function prepares a framework script to work
#
function prepareScript()
{
    defineScript "$0" "Builds the project inside the docker container"
    
    addCommandLineOptionalArgument 'FLASH' '--flash' bool 'The script will flash the binary after the building' 'FALSE' ''
    if isStringEmpty "$MACHINE_NAME"
    then 
        addCommandLineRequiredArgument 'MACHINE_NAME' '--machine-name' options 'Name of the target to be used for building' 'stm32f746ngh6 lm4f120h5qr'
    fi
    addCommandLineOptionalArgument 'SET_PATHS_FILE_PATH' '--paths-file' existing_file 'Name of a script with project paths' "$THIS_DIR/set_paths.sh" ''
    addCommandLineOptionalArgument 'DONT_USE_DOCKER' '--dont-use-docker' bool 'If the option is set, the script does not use docker for building, but it runs the build in the native system' 'FALSE' '' 
    addCommandLineOptionalArgument 'CONNECT' '--connect' bool 'If true, the script will also connect to the target device before flashing' 'FALSE' ''
    addCommandLineOptionalArgument 'GDB_MODE' '--gdb-mode' bool 'If true, the script will open GDB after flashing' 'FALSE' ''
    
    disableConfigurationPrinting
    parseCommandLineArguments "$@"
}

#######################################################################################
#
#   MAIN
#
prepareScript "$@"
build
if isStringEqual "$FLASH" "TRUE"
then 
    flash
elif isStringEqual "$CONNECT" "TRUE"
then
    connectToDevice
fi
