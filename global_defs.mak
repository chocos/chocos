############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile with file names
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = .
endif

##============================================================================================================================
##                                          
##              GENERAL DEFINITIONS                 
##                                          
##============================================================================================================================
BUILD_MAJOR_VERSION             = 1 
BUILD_YEAR                      = 0
BUILD_MONTH                     = 0
BUILD_PATCH_VERSION             = 0
BUILD_VERSION_NAME              = "Feniks"
BUILD_VERSION_DEFINITIONS       = oC_BUILD_MAJOR=$(BUILD_MAJOR_VERSION) oC_BUILD_YEAR=$(BUILD_YEAR) oC_BUILD_MONTH=$(BUILD_MONTH) oC_BUILD_PATCH=$(BUILD_PATCH_VERSION) oC_BUILD_NAME=$(BUILD_VERSION_NAME)
ARCHITECTURE_DEFINITIONS        = MACHINE_WORD_SIZE=$(MACHINE_WORD_SIZE) MACHINE_ARCHITECTURE=$(MACHINE_ARCHITECTURE) MACHINE_CORE_ARCHITECTURE_FAMILY=$(MACHINE_CORE_ARCHITECTURE_FAMILY) MACHINE_PRODUCENT=$(MACHINE_PRODUCENT) MACHINE_FAMILY=$(MACHINE_FAMILY) MACHINE_NAME=$(MACHINE_NAME) $(MACHINE_NAME) $(MACHINE_ENDIANNESS)_ENDIAN
ADDITIONAL_CFLAGS               = -fno-builtin-vprintf \
                                  -fno-builtin-printf \
                                  -fno-builtin-puts \
                                  -fno-builtin-putc \
                                  -fno-builtin-sprintf_s \
                                  -fno-builtin-snprintf \
                                  -fno-builtin-scanf \
                                  -fno-builtin-sscanf \
                                  -fno-builtin-fopen \
                                  -fno-builtin-fclose \
                                  -fno-builtin-fread \
                                  -fno-builtin-fwrite \
                                  -fno-builtin-feof \
                                  -fno-builtin-putchar \
                                  -fno-builtin-vfprintf \
                                  -fno-builtin-fprintf \
                                  -fno-builtin-fputc \
                                  -Wno-unused-const-variable \
                                  -Wno-unused-value \
                                  -Wno-address-of-packed-member \
                                  -fcommon

##============================================================================================================================
##                                          
##              TOOLS CONFIGURATIONS                        
##                                          
##============================================================================================================================
SHELL                           = sh
MKDEP                           = $(TOOLS_DIR)/makedepend
MKDIR                           = mkdir -p
RM                              = rm
RMDIR                           = rmdir
EXPORT                          = $(SOURCES_DIR)/export_var.bat
SIZEOF                          = stat -c %s
MV                              = mv

##============================================================================================================================
##                                          
##          INCLUDE DEFINITIONS MAKEFILE                
##                                          
##============================================================================================================================
include $(PORTABLE_DEFS_MK_FILE_PATH)
