############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/05/15
##  Description:    makefile for HelloWorld program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = .
endif

ifeq ($(PROGRAM_NAME),)
    error Program name is not defined! Define PROGRAM_NAME in a .Makefile
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
SOURCE_DIRS                 = .

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)
include $(USER_SPACE_MK_FILE_PATH)

##============================================================================================================================
##                                          
##              EXTENSIONS CONFIGURATION
##                                          
##============================================================================================================================
C_EXT                       = c
OBJ_EXT                     = o

##============================================================================================================================
##                                          
##              PREPARATION OF DIRECTORIES LIST             
##                                          
##============================================================================================================================
ifeq ($(SPACE),CORE_SPACE)
    INCLUDES_DIRS           += $(SYSTEM_CORE_INCLUDES_DIRS)
    SPACE_LIBRARY_FILE_PATH = $(CORE_SPACE_LIB_FILE_PATH)
else
    INCLUDES_DIRS           += $(SYSTEM_LIBRARIES_INCLUDES_DIRS) $(SYSTEM_CORE_INCLUDES_POSIX_DIR)
    SPACE_LIBRARY_FILE_PATH = $(USER_SPACE_LIB_FILE_PATH)
endif

INCLUDES_DIRS               += $(SYSTEM_PROGRAM_DIR)

##============================================================================================================================
##                                          
##              PREPARATION OF SOURCES LIST IF NOT SET
##                                          
##============================================================================================================================
ifeq ($(SOURCE_FILES),)
    SOURCE_FILES            += $(foreach src,$(foreach subdir, $(SOURCE_DIRS), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))
endif

## Add a additional program files
SOURCE_FILES            += $(foreach src,$(foreach subdir, $(SYSTEM_PROGRAMS_DIR), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))

## Get object files names
OBJECT_FILES            += $(foreach obj,$(patsubst %.c,%.o,$(SOURCE_FILES)),$(notdir $(obj)))

## Renaming each file 
RENAMED_OBJECT_FILES    += $(patsubst %.o,$(PROGRAM_NAME)_%.o,$(OBJECT_FILES))

##============================================================================================================================
##                                          
##              PREPARATION OF DEFINITIONS
##                                          
##============================================================================================================================
ifeq ($(SPACE),CORE_SPACE)
    DEFINITIONS             += oC_CORE_SPACE 
    SPACE_CFLAGS             = -c
else ifeq ($(SPACE),USER_SPACE)
    DEFINITIONS             += oC_USER_SPACE
    #SPACE_CFLAGS             = -fPIE -mno-pic-data-is-text-relative -L$(MACHINE_OUTPUT_DIR) -nostartfiles -luserspace -o $(PROGRAM_ELF_FILE_PATH)
    SPACE_CFLAGS             = -fPIE -mpic-data-is-text-relative -L$(MACHINE_OUTPUT_DIR) -nostartfiles -o $(PROGRAM_ELF_FILE_PATH)
    #SPACE_CFLAGS             = -fPIC -L$(MACHINE_OUTPUT_DIR) -nostartfiles -luserspace -o $(PROGRAM_ELF_FILE_PATH)
    ifeq ($(FILE_TYPE),CBIN)
		DEFINITIONS			+= oC_FILE_TYPE_CBIN
		SPACE_CFLAGS		+= -T$(PROGRAM_LD_FILE_PATH) -e$(PROGRAM_NAME)Main
    else ifeq ($(FILE_TYPE),ELF)
		DEFINITIONS			+= oC_FILE_TYPE_ELF
		SPACE_CFLAGS		+= -T$(PROGRAM_ELF_LD_FILE_PATH) -eMainFunction
	else
        $(error FILE_TYPE for $(PROGRAM_NAME) is not defined)
	endif
else 
	$(error SPACE is not defined for $(PROGRAM_NAME))
endif



ifeq ($(ALLOCATION_LIMIT),)
    ALLOCATION_LIMIT         = 0
endif

ifeq ($(TRACK_ALLOCATION),TRUE)
    DEFINITIONS             += TRACK_ALLOCATION
endif

## Add common definitions 
DEFINITIONS                 += $(BUILD_VERSION_DEFINITIONS) $(ARCHITECTURE_DEFINITIONS) ALLOCATION_LIMIT=$(ALLOCATION_LIMIT)

DEFINITIONS                 += main=$(PROGRAM_NAME)Main ProgramRegistration=$(PROGRAM_NAME)Registration oC_PROGRAM_NAME=$(PROGRAM_NAME) oC_STDIN=$(STANDARD_INPUT) oC_STDOUT=$(STANDARD_OUTPUT) oC_STDERR=$(STANDARD_ERROR) MACHINE_WORD_SIZE=$(MACHINE_WORD_SIZE) HEAP_MAP_SIZE=$(HEAP_MAP_SIZE) PROCESS_STACK_SIZE=$(PROCESS_STACK_SIZE)
     
##============================================================================================================================
##                                          
##              PREPARATION OF COMPILER ARGUMENTS   
##                                          
##============================================================================================================================
INCLUDES                        = $(foreach dir,$(INCLUDES_DIRS),-I$(dir))
DEFINITIONS_FLAGS               = $(foreach def,$(DEFINITIONS),-D$(def))
CFLAGS                          = -g -std=$(CSTD) -z max-page-size=0x08 $(WARNING_FLAGS) -$(OPTIMALIZE) $(CPUCONFIG_CFLAGS) $(SPACE_CFLAGS) -fdata-sections -ffunction-sections -fstack-usage -Wno-format -Werror $(ADDITIONAL_CFLAGS)

##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
program_info:
	@echo "=================================================================================================================="
	@echo "                                           Building Program $(PROGRAM_NAME)"
	@echo "=================================================================================================================="
	@$(MKDIR) $(MACHINE_OUTPUT_DIR)
	
build_program_dependences:
	@echo "Creating dependeces..."
	@$(MKDEP) -f $(PROGRAM_MK_FILE_NAME) $(INCLUDES_DIRS) -- $(CFLAGS) -- $(CORE_SOURCES) -o .$(OBJ_EXT)
	
build_program_objects:
	@echo "Building program..."
	@$(CC) $(SOURCE_FILES) $(INCLUDES) $(CFLAGS) $(DEFINITIONS_FLAGS)
	
ifeq ($(SPACE),CORE_SPACE)
rename_sources:
	@$(foreach obj,$(OBJECT_FILES),mv $(obj) $(PROGRAM_NAME)_$(obj);) 
else 
rename_sources:
	@echo "Skipping renaming sources - the program is in user space " 
endif    
    
add_program_to_CORE_SPACE:
	@echo "Add program to $(SPACE) space..."
	@$(AR) r $(SPACE_LIBRARY_FILE_PATH) $(RENAMED_OBJECT_FILES)
	@$(RM) *.o
	
add_program_to_USER_SPACE: 
	@echo "Preparing bin file for program $(PROGRAM_DIR_NAME)"
	@$(OBJCOPY) $(PROGRAM_ELF_FILE_PATH) -O binary $(PROGRAM_BIN_FILE_PATH) 
	@echo 'Creating extended listing....'
	@$(OBJDUMP) -S $(PROGRAM_ELF_FILE_PATH) > $(PROGRAM_LST_FILE_PATH)

archieve_program: add_program_to_$(SPACE)

program_move_stack_analyze_result:
	@$(MV) *.su $(MACHINE_OUTPUT_DIR)
    
program_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     $(PROGRAM_NAME) building success"
	@echo ""
	
build_program: program_info build_program_objects rename_sources archieve_program program_move_stack_analyze_result program_status
    
clean_program:
	@echo "=================================================================================================================="
	@echo "                                           Cleaning Program $(PROGRAM_NAME)"
	@echo "=================================================================================================================="
	@echo " "
	$(RM) -f *.o
	$(RM) -f *.a
	$(RM) -f $(PROGRAM_LIB_FILE_PATH)
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Core Space cleaning success"
	@echo ""
    
