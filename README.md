#V.0.16.9.0#

The release is planned for 02.2016.

#Issues Queries#
- [Patryk Kubiak Open Issues](https://bitbucket.org/chocos/chocos/issues?responsible=kubiakp&status=on+hold&status=new&status=open)
- [Kamil Drobienko Open Issues](https://bitbucket.org/chocos/chocos/issues?responsible=drobny&status=on+hold&status=new&status=open)
- [Mikolaj Filar Open Issues](https://bitbucket.org/chocos/chocos/issues?responsible=miki234389&status=on+hold&status=new&status=open)

### Release notes ###